﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Dice_Bot.Event;
using Dice_Bot.Model;
using Dice_Bot.Model.Response.EtherCrash;
using Utility;
using Newtonsoft.Json;
using SuperSocket.ClientEngine;
using WebSocket4Net;

namespace Dice_Bot.EtherCrash
{
    public class EtherCrash : DiceBot
    {
        private CookieContainer cookies = new CookieContainer();
        private WebSocket gameSocket;
        private WebSocket chatSocket;

        private const string url = "https://www.ethercrash.io";
        private const string urlGS = "http://gs.ethercrash.io";
        private const string urlOTT = "https://www.ethercrash.io/ott";
        private const string urlPlay = "https://www.ethercrash.io/play";
        private const string urlSocketTransport = "https://www.ethercrash.io/socket.io/?EIO=3&transport=polling&t=";
        private const string urlSocketTransportGS = "https://gs.ethercrash.io/socket.io/?EIO=3&transport=polling&t=";
        private const string urlSocketSID = "https://www.ethercrash.io/socket.io/?EIO=3&sid=";
        private const string urlSocketSIDGS = "https://gs.ethercrash.io/socket.io/?EIO=3&sid=";

        private const string urlSocket = "wss://www.ethercrash.io/socket.io/?EIO=3&transport=websocket&sid=";
        private const string urlSocketGS = "wss://gs.ethercrash.io/socket.io/?EIO=3&transport=websocket&sid=";

        private const string userAgent =
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36";

        private string guid;
        private string ott;
        private string cookieIO;
        private string cookieIOChat;
        private string cookieCFUID;

        private bool betsOpening;
        private bool gameCrashed;
        private int requestID = 1;

        public EtherCrash(DiceBotInfo dbInfo, WebsiteInfo wbInfo, Bet bet)
            : base(dbInfo, wbInfo, bet)
        {
            if (!this.OpenDriver(url)) return;
            this.SignIn();
        }

        public sealed override async void SignIn()
        {
            var clientHandler = new HttpClientHandler
            {
                UseCookies = true,
                CookieContainer = this.cookies,
                AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip
            };

            var client = new HttpClient(clientHandler)
            {
                BaseAddress = new Uri(url)
            };

            client.DefaultRequestHeaders.Add("User-Agent", userAgent);
            client.DefaultRequestHeaders.Add("referer", urlPlay);
            client.DefaultRequestHeaders.Add("origin", url);

            this.cookies.Add(new Cookie("id", this.bet.basic.account, "/", "ethercrash.io"));
            var response = client.GetStringAsync(urlPlay).Result;

            response = client.GetStringAsync(urlSocketTransport + UtilsValue.Timestamp()).Result;
            response = client.GetStringAsync(urlSocketTransportGS + UtilsValue.Timestamp()).Result;
            foreach (Cookie c in this.cookies.GetCookies(new Uri(url)))
            {
                switch (c.Name)
                {
                    case "io":
                        cookieIOChat = c.Value;
                        break;
                    case "__cfduid":
                        cookieCFUID = c.Value;
                        break;
                }
            }

            response = client.GetStringAsync(urlSocketSID + cookieIOChat
                + "&transport=polling&t=" + UtilsValue.Timestamp()).Result;
            foreach (Cookie c in this.cookies.GetCookies(new Uri(urlGS)))
            {
                switch (c.Name)
                {
                    case "io":
                        cookieIO = c.Value;
                        break;
                    case "__cfduid":
                        cookieCFUID = c.Value;
                        break;
                }
            }

            ott = await GetOTTContent(client);
            if (ott == null) return;

            var body = "420[\"join\",{\"ott\":\"" + ott + "\",\"api_version\":1}]";
            body = body.Length + ":" + body;

            var strContent = new StringContent(body, Encoding.UTF8, "text/plain");
            response = client.PostAsync(urlSocketSIDGS + cookieIO
                + "&transport=polling&t=" + UtilsValue.Timestamp(), strContent)
                .Result.Content.ReadAsStringAsync().Result;

            body = "420[\"join\",[\"english\"]]";
            body = body.Length + ":" + body;

            strContent = new StringContent(body, Encoding.UTF8, "text/plain");
            response = client.PostAsync(urlSocketSID + cookieIOChat
                + "&transport=polling&t=" + UtilsValue.Timestamp(), strContent)
                .Result.Content.ReadAsStringAsync().Result;
            response = client.GetStringAsync(urlSocketSID + cookieIOChat
                + "&transport=polling&t=" + UtilsValue.Timestamp()).Result;

            var cookies = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("__cfduid", cookieCFUID),
                new KeyValuePair<string, string>("io", cookieIOChat),
                new KeyValuePair<string, string>("id", this.bet.basic.account)
            };

            chatSocket = new WebSocket(urlSocket + cookieIOChat, null, cookies);
            chatSocket.Opened += socket_opened;
            chatSocket.Error += socket_error;
            chatSocket.MessageReceived += socket_message_received;
            chatSocket.Closed += socket_closed;
            ConnectChatSocket();

            cookies[1] = new KeyValuePair<string, string>("io", cookieIO);
            gameSocket = new WebSocket(urlSocketGS + cookieIO);
            gameSocket.Opened += socket_opened;
            gameSocket.Error += socket_error;
            gameSocket.MessageReceived += socket_message_received;
            gameSocket.Closed += socket_closed;
            if (!await ConnectGameSocket()) return;

            gameSocket.Send("2");
            chatSocket.Send("2");

            this.UpdateHistory("Đăng nhập thành công", Color.Green);
            this.PerformBet();
        }

        private async Task<string> GetOTTContent(HttpClient client)
        {
            while (true)
            {
                if (this.dbInfo.stop == Stop.Stop) return null;
                var ottContent = new StringContent(string.Empty);
                var responseMessage = client.PostAsync(urlOTT, ottContent).Result;
                var response = responseMessage.Content.ReadAsStringAsync().Result;
                if (responseMessage.IsSuccessStatusCode) return response;

                this.UpdateHistory("Đăng nhập thất bại, đang thử lại sau 2s", Color.Red);
                await Task.Delay(2000);
            }
        }

        private async void ConnectChatSocket()
        {
            while (true)
            {
                if (this.dbInfo.stop == Stop.Stop) return;
                chatSocket.Open();

                while (chatSocket.State == WebSocketState.Connecting) Thread.Sleep(300);
                if (chatSocket.State == WebSocketState.Open) return;

                this.UpdateHistory("Khởi tạo chat socket thất bại, " +
                    "đang thử lại sau 2s !!!", Color.Red);
                await Task.Delay(2000);
            }
        }

        private async Task<bool> ConnectGameSocket()
        {
            while (true)
            {
                if (this.dbInfo.stop == Stop.Stop) return false;
                gameSocket.Open();

                while (gameSocket.State == WebSocketState.Connecting) Thread.Sleep(300);
                if (gameSocket.State == WebSocketState.Open) return true;

                this.UpdateHistory("Khởi tạo game socket thất bại, " +
                    "đang thử lại sau 2s !!!", Color.Red);
                await Task.Delay(2000);
            }
        }

        public override async void PerformBet()
        {
            while (true)
            {
                if (this.dbInfo.stop == Stop.Stop) return;
                if (this.bet.bet >= this.bet.balance)
                {
                    this.OnLostAll();
                    return;
                }

                if (gameSocket.State == WebSocketState.Open)
                {
                    var cashout = this.bet.cashout * 100;
                    gameSocket.Send("42" + requestID++ + "[\"place_bet\"," +
                        (this.bet.bet * 100000000).ToString("0") + "," + cashout.ToString("0") + "]");
                    guid = Guid.NewGuid().ToString();
                }
                else
                {
                    this.UpdateHistory($"Socket hiện tại {gameSocket.State}, " +
                        $"đang thử kết nối lại sau 2s", Color.Black);

                    await Task.Delay(2000);
                    if (!await ConnectGameSocket()) return;

                    gameSocket.Send("2");
                    chatSocket.Send("2");

                    this.UpdateHistory("Kết nối lại thành công", Color.Green);
                    continue;
                }

                break;
            }
        }

        private void socket_opened(object sender, EventArgs e)
        {
            ((WebSocket)sender).Send("2probe");
        }

        private void socket_closed(object sender, EventArgs e)
        {
        }

        private void socket_error(object sender, ErrorEventArgs e)
        {
            this.UpdateHistory("Websocket đã lỗi, liên hệ dev !!!", Color.Red);
        }

        private void socket_message_received(object sender, MessageReceivedEventArgs e)
        {
            if (e.Message == "3probe")
            {
                ((WebSocket)sender).Send("5");
            }
            else
            {
                if (e.Message.StartsWith("42[\"game_starting\","))
                {
                    var id = e.Message.Substring(e.Message.IndexOf("\"game_id\":") + "\"game_id\":".Length);
                    id = id.Substring(0, id.IndexOf(","));

                    guid = string.Empty;
                    gameCrashed = false;
                    betsOpening = true;

                    this.UpdateHistory("Game chuẩn bị bắt đầu, đang đặt cược", Color.Black);
                    PerformBet();
                }
                else if (e.Message.StartsWith("42[\"game_started\","))
                {
                    // Close bet and wait result
                    betsOpening = false;
                }
                else if (e.Message.StartsWith("42[\"game_crash\",") && !string.IsNullOrEmpty(guid))
                {
                    var roll = UtilsValue.ExtractNumberFromString(e.Message.Replace("42", ""));
                    this.guid = string.Empty;

                    Console.WriteLine("thua va roll la:" + roll);
                    this.FinishBet(this.requestID, $"{decimal.Parse(roll)}x",false, 99);
                    this.OnWinLose();
                }
                else if (e.Message.StartsWith("42[\"cashed_out\"") && !string.IsNullOrEmpty(guid))
                {
                    if (!e.Message.Contains("\"" + this.bet.basic.password + "\"")) return;
                    var roll = UtilsValue.ExtractNumberFromString(e.Message.Replace("42", ""));
                    this.gameCrashed = true;
                    this.guid = string.Empty;

                    this.FinishBet(this.requestID, $"{decimal.Parse(roll)}x", true, 99);
                    this.OnWinLose();
                }
                else if (e.Message.StartsWith("430[null,{\"state"))
                {
                    var content = e.Message.Substring(e.Message.IndexOf("{"));
                    content = content.Substring(0, content.LastIndexOf("}"));

                    var loginResponse = JsonConvert.DeserializeObject<EtherCrashLogin>(content);
                    this.bet.SetBalance(loginResponse.balance_satoshis / 100000000m);
                }
                else if (e.Message.StartsWith("42[\"game_tick\""))
                {
                    if (string.IsNullOrEmpty(guid) && !gameCrashed) return;
                    var x = e.Message.Substring(e.Message.IndexOf(",") + 1);
                    x = x.Substring(0, x.LastIndexOf("]"));

                    if (!decimal.TryParse(x, out var tickval) || tickval % 1.25m != 0) return;
                    var crash = Math.Floor(100 * Math.Pow(Math.E, 0.00006 * (double)tickval));
                    this.UpdateHistory($"Game running - {crash / 100}x", Color.Black);
                }
            }
        }

        public override void ResetSeed()
        {
            base.ResetSeed();
        }

        public override void UpdateHistory(string newContent, object color)
        {
            base.UpdateHistory(newContent, color);
        }

        public override void StopProgram()
        {
            base.StopProgram();
        }
    }
}