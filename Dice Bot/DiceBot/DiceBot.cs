﻿using Dice_Bot.Event;
using Dice_Bot.Interface;
using Dice_Bot.Model;
using Utility;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using SharpLua;

namespace Dice_Bot
{
    public class DiceBot : BaseDiceBot
    {
        #region Init Event
        public event EventEnableAllControl OnEnableAllControl;
        public event EventUpdateTotalBet OnUpdateTotalBet;
        public event EventUpdateHistory OnUpdateHistory;
        public event EventAddDataToDGV OnAddDataToDGV;
        #endregion

        private LuaInterface Lua = LuaRuntime.GetLua();
        protected DiceBotInfo dbInfo;
        protected IWebDriver driver;
        protected Bet bet;

        protected string pathCoin;
        protected bool isProgrammer;
        private long chartBets;
        private int gamePause;
        private int gameStop;

        protected DiceBot(DiceBotInfo dbInfo, WebsiteInfo wbInfo, Bet bet)
        {
            this.dbInfo = dbInfo;
            this.bet = bet;

            var coinInfo = wbInfo.coinInfos.Single(x => x.coin == bet.basic.coin);
            this.isProgrammer = this.dbInfo.isProgrammer && !string.IsNullOrEmpty(this.dbInfo.programmer);
            this.pathCoin = coinInfo.path;
            this.dbInfo.form.dbs.Add(this);

            #region Init Event; Lua Register, Init Programmer
            this.OnEnableAllControl += dbInfo.form.EnableAllControl;
            this.OnUpdateHistory += dbInfo.form.UpdateHistory;
            this.OnUpdateTotalBet += dbInfo.form.UpdateTotalBet;
            this.OnAddDataToDGV += dbInfo.form.AddDataToDGV;

            if (!this.isProgrammer) return;
            this.Lua.RegisterFunction("print", this, this.GetType().GetMethod("UpdateHistory"));
            this.Lua.RegisterFunction("withdraw", this, this.GetType().GetMethod("Withdraw"));
            this.Lua.RegisterFunction("resetseed", this, this.GetType().GetMethod("ResetSeed"));
            this.Lua.RegisterFunction("stopprogram", this, this.GetType().GetMethod("StopProgram"));

            LuaRuntime.SetLua(this.Lua);
            this.SetLuaVars();
            LuaRuntime.Run(this.dbInfo.programmer);
            this.GetLuaVars();
            #endregion
        }

        #region General Function
        protected bool ConditionProgrammer()
        {
            if (this.bet.bet <= 0)
            {
                this.UpdateHistory("Please set starting with nextbet = x.xxxxxxx", Color.Red);
                return false;
            }

            if (this.bet.cashout <= 0)
            {
                this.UpdateHistory("Please set starting with cashout = x.xxxxxxx", Color.Red);
                return false;
            }

            if (this.bet.winChance > 0) return true;
            this.UpdateHistory("Please set starting with winchance = x.xxxxxxx", Color.Red);
            return false;
        }

        private void GetLuaVars()
        {
            this.bet.bet = (decimal)(double)Lua["nextbet"];
            this.bet.cashout = (decimal)(double)Lua["cashout"];
            this.bet.winChance = (decimal)(double)Lua["winchance"];
            this.bet.high = (bool)Lua["bethigh"];
        }

        private void SetLuaVars()
        {
            Lua["balance"] = this.bet.balance;
            Lua["rootbalance"] = this.bet.rootBalance;
            Lua["roll"] = this.bet.roll;
            Lua["winchance"] = this.bet.winChance;
            Lua["cashout"] = this.bet.cashout;
            Lua["nextbet"] = this.bet.bet;
            Lua["bethigh"] = this.bet.high;
            Lua["wins"] = this.bet.win;
            Lua["loses"] = this.bet.lose;
            Lua["totalprofit"] = this.bet.totalProfit;
            Lua["profitWin"] = this.bet.profitWin;
            Lua["profitLose"] = this.bet.profitLose;
        }

        protected void OnLostAll()
        {
            this.dbInfo.SetStop(Stop.Stop);
            this.UpdateHistory($"Bạn không đủ tiền để tiếp tục đặt cược " +
                $"coin: {this.bet.basic.coin}, bet: {this.bet.bet}, " +
                $"balance: {this.bet.balance}", Color.Red);
        }

        protected void FinishBet(long id, string roll, bool result, decimal winChance = 0, decimal cashout = 0)
        {
            var profitWin = result ? this.bet.bet * (cashout == 0 ? this.bet.cashout : cashout - 1) : 0;
            var profitLose = result ? 0 : -this.bet.bet;
            
            this.bet.betID = id.ToString();
            this.bet.roll = roll;
            this.bet.winChance = winChance > 0 ? winChance / this.bet.cashout : this.bet.cashout;
            this.bet.profitWin += profitWin;
            this.bet.profitLose -= profitLose;
            this.bet.totalProfit += profitWin + profitLose;
            this.bet.balance = this.bet.rootBalance + this.bet.totalProfit;

            this.bet.basic.result = result;
            if (result) this.bet.win++;
            else this.bet.lose++;
        }

        private void NewestResult(Bet bet)
        {
            this.UpdateHistory("--- Kết quả mới nhất --- \n" +
                $"ID: {bet.betID}\nCrash: {bet.roll}\n" +
                $"Cashout: {bet.cashout}\n" +
                $"Result: {bet.basic.result}\n{(bet.basic.result ? "Bạn THẮNG" : "Bạn THUA")}",
                bet.basic.result ? Color.Green : Color.Red);
            this.OnUpdateTotalBet?.Invoke(bet.basic.result);
        }

        protected bool OpenDriver(string url, bool refreshHomePage = false)
        {
            try
            {
                switch (this.dbInfo.browserIndex)
                {
                    case 0: // Chrome
                        var chromeService = ChromeDriverService.CreateDefaultService();
                        var chromeOptions = new ChromeOptions();

                        chromeService.HideCommandPromptWindow = true;
                        this.driver = new ChromeDriver(chromeService, chromeOptions) { Url = url };
                        this.dbInfo.form.chromes.Add((ChromeDriver)this.driver);
                        break;
                    default: // Firefox
                        var firefoxService = FirefoxDriverService.CreateDefaultService();
                        var firefoxOptions = new FirefoxOptions();
                        var profile = new FirefoxProfile();

                        if (!string.IsNullOrEmpty(this.dbInfo.proxy))
                        {
                            var proxy = this.dbInfo.proxy.Split(':');
                            profile.SetPreference("network.proxy.type", 1);
                            profile.SetPreference("network.proxy.http", proxy[0]);
                            profile.SetPreference("network.proxy.http_port", proxy[1]);
                            profile.SetPreference("network.proxy.ssl", proxy[0]);
                            profile.SetPreference("network.proxy.ssl_port", proxy[1]);
                            firefoxOptions.Profile = profile;
                        }

                        firefoxService.HideCommandPromptWindow = true;
                        this.driver = new FirefoxDriver(firefoxService, firefoxOptions) { Url = url };
                        this.dbInfo.form.firefoxs.Add((FirefoxDriver)this.driver);
                        break;
                }

                this.driver.Navigate();
                if (refreshHomePage) this.driver.Navigate().Refresh();
                return true;
            }
            catch
            {
                UtilsDialog.DialogError("Trình duyệt chưa được cài đặt " +
                    "hoặc không phải version mới nhất !");
                return false;
            }
        }

        protected void EnableAllControl(bool enable)
        {
            this.OnEnableAllControl?.Invoke(enable, true);
        }

        protected void OnWinLose(Action action = null)
        {
            this.OnAddDataToDGV?.Invoke(this.dbInfo.dgv, this.bet);
            if (gamePause < 1)
            {
                this.chartBets = this.dbInfo.form.AddDataToChart(this.dbInfo.chart, this.bet, this.chartBets);
                this.NewestResult(this.bet);

                if (this.isProgrammer)
                {
                    try
                    {
                        Lua["result"] = this.bet.basic.result;
                        this.SetLuaVars();
                        LuaRuntime.SetLua(Lua);
                        LuaRuntime.Run("dobet()");
                        this.GetLuaVars();
                        action?.Invoke();
                    }
                    catch
                    {
                        this.UpdateHistory("Code của bạn bị lỗi, kiểm tra lại", Color.Red);
                        return;
                    }
                }
                else
                {
                    switch (this.OnStop())
                    {
                        case null:
                            // Dừng thread hiện tại
                            this.dbInfo.SetStop(Stop.Stop);
                            break;
                        case false when this.bet.basic.game is Game.CrashClassic ||
                                        this.bet.basic.game is Game.CrashTrenballRed ||
                                        this.bet.basic.game is Game.CrashTrenballGreen ||
                                        this.bet.basic.game is Game.CrashTrenballMoon:
                            // Tạm dừng đặt cược
                            this.dbInfo.SetStop(Stop.Pause);
                            break;
                        default:
                            // Tiếp tục đặt cược
                            if (this.gamePause >= 1) break;
                            if (this.bet.bet >= this.bet.balance)
                            {
                                this.OnLostAll();
                                break;
                            }

                            this.UpdateHistory($"{this.bet.basic.game}, " +
                                $"{this.bet.basic.coin}\nĐã đặt cược thành công" +
                                $" với bet: {this.bet.bet}, cashout: {this.bet.cashout}",
                                Color.BlueViolet);

                            action?.Invoke();
                            break;
                    }
                }
            }
            else
            {
                gamePause++;
                if (gamePause >= gameStop)
                {
                    gamePause = 0;
                    gameStop = -1;
                }
                else
                {
                    this.UpdateHistory($"Đã tạm dừng {gamePause} game, còn lại " +
                        $"{gameStop - gamePause} để tiếp tục đặt cược trở lại", Color.Black);
                }
            }
        }

        private bool? OnStop()
        {
            var result = Settings.OnStop(this.bet, this.bet.basic.GetBet(), this.bet.basic.GetCashout());
            switch (result)
            {
                case string newContent:
                    this.UpdateHistory(newContent, newContent.Contains("thua") ? Color.Green : Color.Red);
                    return null;
                case KeyValuePair<int, Bet> newBet:
                    gamePause = 1;
                    gameStop = newBet.Key;

                    this.bet = newBet.Value;
                    this.UpdateHistory($"Bắt đầu tạm dừng {gameStop} game !!!", Color.Black);
                    return false;
                default:
                    this.bet = (Bet)result;
                    return true;
            }
        }

        public virtual void SignIn() { }
        public virtual void WithdrawClick(object sender, EventArgs e) { }
        public virtual void PerformBet() { }
        public virtual void ResetSeed() { }
        
        public virtual bool Withdraw(decimal amount, string address)
        {
            throw new NotImplementedException();
        }

        public virtual void UpdateHistory(string newContent, object color)
        {
            if (color is string s) color = Color.FromName(s);
            this.OnUpdateHistory?.Invoke(newContent, (Color)(color ?? Color.Black), this.dbInfo.threadIndex);
        }

        public virtual void StopProgram()
        {
            this.dbInfo.SetStop(Stop.Stop);
        }
        #endregion
    }
}
