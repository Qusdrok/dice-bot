﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Dice_Bot.Event;
using Dice_Bot.Model;
using Dice_Bot.Model.Response.KingDice;
using Newtonsoft.Json;

namespace Dice_Bot.KingDice
{
    public class KingDice : DiceBot
    {
        private const string url = "https://kingdice.com/api/";
        private const string urlReferer = "https://kingdice.com";
        private const string urlOrigin = "https://kingdice.com";
        private const string urlHost = "kingdice.com";
        private const string userAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0";

        private string accessToken;

        private Random rnd = new Random();
        private HttpClient client;

        protected KingDice(DiceBotInfo dbInfo, WebsiteInfo wbInfo, Bet bet)
            : base(dbInfo, wbInfo, bet)
        {
            if (!this.OpenDriver(url)) return;
            this.SignIn();
        }

        public override async void PerformBet()
        {
            try
            {
                if (this.dbInfo.stop == Stop.Stop) return;
                var clientSeed = rnd.Next(0, 100).ToString();
                var chance = this.bet.high ? 99m - 99 / this.bet.cashout : 99 / this.bet.cashout;
                var pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("rollAmount", (this.bet.bet * 100000000m).ToString("0")),
                    new KeyValuePair<string, string>("rollUnder", chance.ToString("0")),
                    new KeyValuePair<string, string>("mode", this.bet.high ? "2" : "1"),
                    new KeyValuePair<string, string>("rollClient", clientSeed),
                    new KeyValuePair<string, string>("token", this.accessToken)
                };

                var content = new FormUrlEncodedContent(pairs);
                var emitResponse = this.client.PostAsync("play.php", content).Result.Content.ReadAsStringAsync().Result;
                var betResponse = JsonConvert.DeserializeObject<KingDiceBet>(emitResponse);

                this.FinishBet((long) betResponse.roll_id, $"{betResponse.roll_number}",
                    betResponse.roll_result == "win", 99);
                this.OnWinLose(this.PerformBet);
            }
            catch
            {
                this.UpdateHistory("Đặt cược thất bại, đang thử lại sau 2s", Color.Red);
                await Task.Delay(2000);
                this.PerformBet();
            }
        }

        public sealed override async void SignIn()
        {
            try
            {
                var clientHandler = new HttpClientHandler
                {
                    UseCookies = true,
                    AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip
                };

                this.client = new HttpClient(clientHandler)
                {
                    BaseAddress = new Uri(url)
                };

                this.client.DefaultRequestHeaders.AcceptEncoding.Add(
                    new System.Net.Http.Headers.StringWithQualityHeaderValue("gzip"));
                this.client.DefaultRequestHeaders.AcceptEncoding.Add(
                    new System.Net.Http.Headers.StringWithQualityHeaderValue("deflate"));
                this.client.DefaultRequestHeaders.Add("User-Agent", userAgent);
                this.client.DefaultRequestHeaders.Add("Host", urlHost);
                this.client.DefaultRequestHeaders.Add("Origin", urlOrigin);
                this.client.DefaultRequestHeaders.Add("Referer", urlReferer);

                var pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("username", this.bet.basic.account),
                    new KeyValuePair<string, string>("password", this.bet.basic.password),
                    new KeyValuePair<string, string>("sdb", "8043d46408307f3ac9d14931ba27c9015349bf21b7b7"),
                    new KeyValuePair<string, string>("2facode", "")
                };
                
                var content = new FormUrlEncodedContent(pairs);
                var emitResponse = this.client.PostAsync("login.php", content).
                    Result.Content.ReadAsStringAsync().Result;
                var baseResponse = JsonConvert.DeserializeObject<KingDiceBase>(emitResponse);
                
                if (baseResponse.code != "SUCCESS") return;
                this.accessToken = baseResponse.token;
                pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("token", this.accessToken)
                };
                    
                content = new FormUrlEncodedContent(pairs);
                emitResponse = this.client.PostAsync("logged.php", content).
                    Result.Content.ReadAsStringAsync().Result;
                var loginResponse = JsonConvert.DeserializeObject<KingDiceLogin>(emitResponse);
                
                if (loginResponse.code != "SUCCESS") return;
                this.bet.SetBalance(loginResponse.balance / 100000000m);
                this.UpdateHistory("Đăng nhập thành công", Color.Green);
            }
            catch
            {
                this.UpdateHistory("Đăng nhập thất bại, đang thử lại sau 2s", Color.Red);
                await Task.Delay(2000);
                this.SignIn();
            }
        }

        public override bool Withdraw(decimal amount, string address)
        {
            try
            {
                var pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("amount",(amount * 100000000).ToString()),
                    new KeyValuePair<string, string>("address", address),
                    new KeyValuePair<string, string>("token", this.accessToken)
                };

                var content = new FormUrlEncodedContent(pairs);
                var emitResponse = this.client.PostAsync("withdraw.php", content).
                    Result.Content.ReadAsStringAsync().Result;
                return emitResponse.Contains("SUCCESS");
            }
            catch
            {
                this.UpdateHistory("Withdraw thất bại, đang thử lại", Color.Red);
                return this.Withdraw(amount, address);
            }
        }

        public override void ResetSeed()
        {
            base.ResetSeed();
        }

        public override void UpdateHistory(string newContent, object color)
        {
            base.UpdateHistory(newContent, color);
        }

        public override void StopProgram()
        {
            base.StopProgram();
        }
    }
}