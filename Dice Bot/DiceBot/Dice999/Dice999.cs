﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Dice_Bot.Model;
using Dice_Bot.Model.Dice999_Response;
using Newtonsoft.Json;

namespace Dice_Bot.Dice999
{
    public class Dice999 : DiceBot
    {
        private const string url = "https://www.999dice.com/?349461231";
        private const string urlAPI = "https://www.999dice.com/api/web.aspx";

        private string sessionCookie = string.Empty;
        private string response = string.Empty;
        private string hash = string.Empty;
        private int requestID;

        private Random rnd = new Random();
        private HttpClient client;

        private List<KeyValuePair<string, string>> pairs;
        private FormUrlEncodedContent content;

        public Dice999(DiceBotInfo dbInfo, WebsiteInfo wbInfo, Bet bet)
            : base(dbInfo, wbInfo, bet)
        {
            if (!this.OpenDriver(url)) return;
            this.SignIn();
        }

        public sealed override async void SignIn()
        {
            while (true)
            {
                if (this.dbInfo.stop == Event.Stop.Stop) return;
                var clientHandler = new HttpClientHandler
                {
                    UseCookies = true,
                    AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip
                };

                client = new HttpClient(clientHandler)
                {
                    BaseAddress = new Uri(urlAPI)
                };

                client.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("gzip"));
                client.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("deflate"));

                pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("a", "Login"),
                    new KeyValuePair<string, string>("key", "d74da202b7614e0482476930826e3348"),
                    new KeyValuePair<string, string>("Username", this.bet.basic.account),
                    new KeyValuePair<string, string>("Password", this.bet.basic.password),
                };

                content = new FormUrlEncodedContent(pairs);
                response = client.PostAsync("", content).Result.Content.ReadAsStringAsync().Result;

                var loginResponse = JsonConvert.DeserializeObject<Dice999Login>(response);
                if (!string.IsNullOrEmpty(loginResponse.SessionCookie) && loginResponse.SessionCookie != null)
                {
                    sessionCookie = loginResponse.SessionCookie;
                    this.UpdateHistory("Đăng nhập thành công", Color.Green);
                    this.GetBalance();
                    this.PerformBet();
                }
                else
                {
                    this.UpdateHistory("Đăng nhập thất bại, đang thử lại sau 2s", Color.Red);
                    await Task.Delay(2000);
                    continue;
                }

                break;
            }
        }

        public override async void PerformBet()
        {
            try
            {
                if (this.dbInfo.stop == Event.Stop.Stop) return;
                var chance = 999999.0m * (99 / bet.cashout / 100.0m);
                if (string.IsNullOrEmpty(hash) && hash != null)
                {
                    try
                    {
                        pairs = new List<KeyValuePair<string, string>>
                        {
                            new KeyValuePair<string, string>("a", "GetServerSeedHash"),
                            new KeyValuePair<string, string>("s", sessionCookie)
                        };

                        content = new FormUrlEncodedContent(pairs);
                        response = client.PostAsync("", content).Result.Content.ReadAsStringAsync().Result;

                        if (response.Contains("error"))
                        {
                            PerformBet();
                            return;
                        }

                        hash = JsonConvert.DeserializeObject<Dice999Hash>(response).Hash;
                    }
                    catch (AggregateException ex)
                    {
                        Debug.Assert(ex.InnerException != null, "ex.InnerException != null");
                        if (ex.InnerException.Message.Contains("ssl"))
                        {
                            PerformBet();
                            return;
                        }
                    }
                }

                var clientSeed = rnd.Next(0, int.MaxValue).ToString();
                pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("a", "PlaceBet"),
                    new KeyValuePair<string, string>("s", sessionCookie),
                    new KeyValuePair<string, string>("PayIn",
                        (this.bet.bet * 100000000).ToString("0", NumberFormatInfo.InvariantInfo)),
                    new KeyValuePair<string, string>("Low",
                        (999999 - (int) chance).ToString(NumberFormatInfo.InvariantInfo)),
                    new KeyValuePair<string, string>("High", 999999.ToString(NumberFormatInfo.InvariantInfo)),
                    new KeyValuePair<string, string>("ClientSeed", clientSeed),
                    new KeyValuePair<string, string>("Currency", this.bet.basic.coin.ToString().ToLower()),
                    new KeyValuePair<string, string>("ProtocolVersion", "2")
                };

                content = new FormUrlEncodedContent(pairs);
                response = client.PostAsync("", content).Result.Content.ReadAsStringAsync().Result;

                var betResponse = JsonConvert.DeserializeObject<Dice999Bet>(response);
                var newChance = chance * 100m / 999999m;
                var roll = betResponse.Secret / 10000.0m;
                var result = roll > 99.99m - newChance;

                this.FinishBet(this.requestID++, $"{roll}", result, newChance + 9);
                this.OnWinLose(this.PerformBet);
            }
            catch
            {
                this.UpdateHistory("Đặt cược thất bại, đang thử lại sau 2s", Color.Red);
                await Task.Delay(2000);
                this.PerformBet();
            }
        }

        private async void GetBalance()
        {
            try
            {
                if (this.dbInfo.stop == Event.Stop.Stop) return;
                pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("a", "GetBalance"),
                    new KeyValuePair<string, string>("s", sessionCookie),
                    new KeyValuePair<string, string>("Currency", this.bet.basic.coin.ToString().ToLower()),
                    new KeyValuePair<string, string>("Stats", "1")
                };

                content = new FormUrlEncodedContent(pairs);
                response = client.PostAsync("", content).Result.Content.ReadAsStringAsync().Result;

                var loginResponse = JsonConvert.DeserializeObject<Dice999Login>(response);
                this.bet.SetBalance(loginResponse.Balance / 100000000m);
            }
            catch
            {
                this.UpdateHistory("Nhận dữ liệu tài khoản thất bại, đang thử lại sau 2s", Color.Red);
                await Task.Delay(2000);
                this.GetBalance();
            }
        }

        public override bool Withdraw(decimal amount, string address)
        {
            try
            {
                if (this.dbInfo.stop == Event.Stop.Stop) return false;
                this.pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("a", "Withdraw"),
                    new KeyValuePair<string, string>("s", this.sessionCookie),
                    new KeyValuePair<string, string>("Currency", this.bet.basic.coin.ToString().ToLower()),
                    new KeyValuePair<string, string>("Amount", (amount * 100000000m).ToString("0")),
                    new KeyValuePair<string, string>("Address", address)
                };

                this.content = new FormUrlEncodedContent(pairs);
                var response = this.client.PostAsync("", content);
                while (!response.IsCompleted) Thread.Sleep(300);
                this.response = response.Result.Content.ReadAsStringAsync().Result;
                return true;
            }
            catch (AggregateException e)
            {
                if (!e.InnerException.Message.Contains("ssl")) return false;
                this.UpdateHistory("Withdraw thất bại, đang thử lại", Color.Red);
                return this.Withdraw(amount, address);
            }
        }

        public override void ResetSeed()
        {
            base.ResetSeed();
        }

        public override void UpdateHistory(string newContent, object color)
        {
            base.UpdateHistory(newContent, color);
        }

        public override void StopProgram()
        {
            base.StopProgram();
        }
    }
}