﻿using System;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Dice_Bot.Model;
using Dice_Bot.Model.Response.WolfBet;
using Newtonsoft.Json;

namespace Dice_Bot.WolfBet
{
    public class WolfBet : DiceBot
    {
        private const string url = "https://wolf.bet";
        private const string accept = "text/html,application/xhtml+xml,application/xml;" +
            "q=0.9,image/webp,image/apng,*/*;" +
            "q=0.8,application/signed-exchange;v=b3;q=0.9";
        private const string userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) " +
            "AppleWebKit/537.36 (KHTML, like Gecko) " +
            "Chrome/79.0.3945.117 Safari/537.36";

        private int requestID;

        private HttpClient client;

        public WolfBet(DiceBotInfo dbInfo, WebsiteInfo wbInfo, Bet bet)
            : base(dbInfo, wbInfo, bet)
        {
            if (!this.OpenDriver(url)) return;
            this.SignIn();
        }

        public sealed override async void SignIn()
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                       | SecurityProtocolType.Tls11
                                                       | SecurityProtocolType.Tls12
                                                       | SecurityProtocolType.Ssl3;
                var clientHandler = new HttpClientHandler
                {
                    UseCookies = true,
                    AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip
                };

                this.client = new HttpClient(clientHandler)
                {
                    BaseAddress = new Uri(url + "/api/v1/")
                };

                this.client.DefaultRequestHeaders.AcceptEncoding.Add(
                    new System.Net.Http.Headers.StringWithQualityHeaderValue("gzip"));
                this.client.DefaultRequestHeaders.AcceptEncoding.Add(
                    new System.Net.Http.Headers.StringWithQualityHeaderValue("deflate"));
                this.client.DefaultRequestHeaders.Add("UserAgent", userAgent);
                this.client.DefaultRequestHeaders.Add("Origin", url);
                this.client.DefaultRequestHeaders.Add("Accept", accept);
                this.client.DefaultRequestHeaders.Add("authorization", "Bearer " + this.bet.basic.password);

                var emitResponse = this.client.GetStringAsync("user/balances").Result;
                var profileResponse = JsonConvert.DeserializeObject<WolfBetProfile>(emitResponse);

                if (profileResponse.balances == null) return;
                foreach (var x in profileResponse.balances.Where(x =>
                    string.Equals(x.currency, this.bet.basic.coin.ToString(),
                        StringComparison.CurrentCultureIgnoreCase)))
                {
                    this.bet.SetBalance(decimal.Parse(x.amount));
                }

                this.UpdateHistory("Đăng nhập thành công", Color.Green);
                this.PerformBet();
            }
            catch
            {
                this.UpdateHistory("Đăng nhập thất bại, đang thử lại sau 2s", Color.Red);
                await Task.Delay(2000);
                this.SignIn();
            }
        }

        public override bool Withdraw(decimal amount, string address)
        {
            try
            {
                var x = $"{{\"amount\":\"{amount.ToString()}\"," +
                        $"\"currency\":\"{this.bet.basic.coin.ToString().ToLower()}\",\"address\":\"{address}\"}}";
                var content = new StringContent(x);
                content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                return this.client.PostAsync("bet/place", content).Result.IsSuccessStatusCode;
            }
            catch
            {
                this.UpdateHistory("Withdraw thất bại, đang thử lại", Color.Red);
                return this.Withdraw(amount, address);
            }
        }

        public override async void PerformBet()
        {
            try
            {
                var chance = Math.Round(99 / this.bet.cashout, 2);
                var betResponse = new WolfBetRequestBet
                {
                    amount = this.bet.bet.ToString("0.00000000"),
                    currency = this.bet.basic.coin.ToString().ToLower(),
                    rule = this.bet.high ? "over" : "under",
                    bet_value = (this.bet.high ? 99.99m - chance : chance).ToString("0.##"),
                    game = "dice"
                };

                var loginResponse = JsonConvert.SerializeObject(betResponse);
                var content = new StringContent(loginResponse);

                content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = this.client.PostAsync("bet/place", content).Result;
                var emitResponse = response.Content.ReadAsStringAsync().Result;
                var resultResponse = JsonConvert.DeserializeObject<WolfBetResult>(emitResponse);

                if (resultResponse.bet == null) return;
                var roll = decimal.Parse(resultResponse.bet.result_value);
                var result = this.bet.high ? roll > 99.99m - 99 / this.bet.cashout : roll < 99 / this.bet.cashout;

                this.FinishBet(this.requestID++, $"{roll}", result, 99);
                this.OnWinLose(this.PerformBet);
            }
            catch
            {
                this.UpdateHistory("Đặt cược thất bại, đang thử lại sau 2s", Color.Red);
                await Task.Delay(2000);
                this.PerformBet();
            }
        }

        public override async void ResetSeed()
        {
            try
            {
                var response = this.client.GetAsync("game/seed/refresh").Result.Content.ReadAsStringAsync().Result;
                var content = new StringContent("{\"game\":\"dice\"}");

                content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                response = this.client.PostAsync("game/seed/refresh", content).Result.Content.ReadAsStringAsync()
                    .Result;
            }
            catch
            {
                this.UpdateHistory("Reset seed thất bại, đang thử lại sau 2s", Color.Red);
                await Task.Delay(2000);
                this.ResetSeed();
            }
        }

        public override void UpdateHistory(string newContent, object color)
        {
            base.UpdateHistory(newContent, color);
        }

        public override void StopProgram()
        {
            base.StopProgram();
        }
    }
}