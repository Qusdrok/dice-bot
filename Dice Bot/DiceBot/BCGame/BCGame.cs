﻿using System.Drawing;
using System.Threading.Tasks;
using Dice_Bot.Model;
using Utility;

namespace Dice_Bot.BCGame
{
    public class BCGame : DiceBot
    {
        private const string url = "https://bc.game/home";
        private const string pathSignIn = "//*[@id='header']/div[1]/div[2]/button";
        private const string pathUsername = "//*[@id='login2']/form/div[1]/input";
        private const string pathPassword = "//*[@id='login2']/form/div[2]/input";
        private const string pathSignInPopUp = "//*[@id='login2']/form/div[3]/button";
        protected const string pathAllCoin = "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[1]";

        protected const string pathGameCrashInList = "//*[@id='home']/div[1]/div/div[1]/div[1]/div/a[1]";
        protected const string pathGameCrashTrenball = "//*[@id='crash-game']/div[1]/div[1]/div[2]";

        protected const string pathResultCrashId = "//*[@id='crash-results']/a[6]/div/div[1]";
        protected const string pathResultCrashNumber = "//*[@id='crash-results']/a[6]/div/div[2]";

        protected const string pathBalance = "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[1]/div/div/span[1]";
        protected long oldID = 0;

        protected BCGame(DiceBotInfo dbInfo, WebsiteInfo wbInfo, Bet bet)
            : base(dbInfo, wbInfo, bet)
        {
            if (!this.OpenDriver(url, true)) return;
            this.SignIn();
        }

        public sealed override async void SignIn()
        {
            try
            {
                UtilsValue.SendClick(this.driver, pathSignIn);
                UtilsValue.SendText(this.driver, pathUsername, this.bet.basic.account);
                UtilsValue.SendText(this.driver, pathPassword, this.bet.basic.password);
                UtilsValue.SendClick(this.driver, pathSignInPopUp);
            }
            catch
            {
                this.UpdateHistory("Đăng nhập thất bại, đang thử lại sau 2s", Color.Red);
                this.driver.Navigate().Refresh();

                await Task.Delay(2000);
                this.SignIn();
            }
        }
    }
}
