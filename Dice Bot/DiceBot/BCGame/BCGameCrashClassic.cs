﻿using Dice_Bot.Event;
using Dice_Bot.Model;
using Utility;
using System.Drawing;
using System.Threading.Tasks;

namespace Dice_Bot.BCGame
{
    public class BCGameCrashClassic : BCGame
    {
        private const string pathInputBetClassic = "//*[@id='crash-controls']/form/div[1]/div/div/input";
        private const string pathInputCashOutClassic = "//*[@id='crash-controls']/form/div[2]/div/div/input";
        private const string pathButtonBetClassic = "//*[@id='crash-bet']";

        public BCGameCrashClassic(DiceBotInfo dbInfo, WebsiteInfo wbInfo, Bet bet)
            : base(dbInfo, wbInfo, bet)
        {
            if (this.isProgrammer && !this.ConditionProgrammer()) return;
            this.InitPlay();
        }

        private async void PerformPlay()
        {
            while (true)
            {
                if (this.dbInfo.stop != Stop.Stop)
                {
                    var newID = long.Parse(UtilsValue.ExtractNumberFromElement(this.driver, pathResultCrashId));
                    if (oldID < newID)
                    {
                        var crash = decimal.Parse(UtilsValue.ExtractNumberFromElement(this.driver, pathResultCrashNumber));
                        var result = crash >= this.bet.cashout;

                        this.oldID = newID;
                        this.FinishBet(newID, $"{crash}x", result, 99);
                        this.OnWinLose(this.PerformBet);
                    }

                    await Task.Delay(1230);
                    continue;
                }

                this.UpdateHistory("Đã dừng thread hiện tại !!!", Color.Black);
                break;
            }
        }

        public override async void PerformBet()
        {
            try
            {
                if (this.dbInfo.stop == Event.Stop.Stop) return;
                UtilsValue.SendText(this.driver, pathInputBetClassic, this.bet.bet.ToString(), KeyControl.A);
                UtilsValue.SendText(this.driver, pathInputCashOutClassic, this.bet.cashout.ToString(), KeyControl.A);
                UtilsValue.SendClick(this.driver, pathButtonBetClassic);
            }
            catch
            {
                this.UpdateHistory("Đặt cược lỗi, đang thử lại sau 2s", Color.Red);
                await Task.Delay(2000);
                this.PerformBet();
            }
        }

        private async void InitPlay()
        {
            try
            {
                if (this.dbInfo.stop == Event.Stop.Stop) return;
                UtilsValue.SendClick(this.driver, pathAllCoin);
                UtilsValue.SendClick(this.driver, pathCoin, true);
                UtilsValue.SendClick(this.driver, pathGameCrashInList);

                this.UpdateHistory("Đăng nhập thành công", Color.Green);
                this.EnableAllControl(false);

                this.bet.SetBalance(decimal.Parse(UtilsValue.ExtractNumberFromElement(this.driver, pathBalance)));
                this.PerformPlay();
            }
            catch
            {
                this.UpdateHistory("Có captcha, hãy tự đăng nhập và chờ 2s !!!", Color.Red);
                await Task.Delay(2000);
                this.InitPlay();
            }
        }

        public override void ResetSeed()
        {
            base.ResetSeed();
        }

        public override void UpdateHistory(string newContent, object color)
        {
            base.UpdateHistory(newContent, color);
        }

        public override void StopProgram()
        {
            base.StopProgram();
        }
    }
}
