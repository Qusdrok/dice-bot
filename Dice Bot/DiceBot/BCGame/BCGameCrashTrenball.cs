﻿using Dice_Bot.Event;
using Dice_Bot.Model;
using Utility;
using System.Drawing;
using System.Threading.Tasks;

namespace Dice_Bot.BCGame
{
    public class BCGameCrashTrenball : BCGame
    {
        private const string pathInputBetTrenball = "//*[@id='crash-war-manual']/div[1]/div/div/div/input";
        private const string pathButtonRedTrenball = "//*[@id='crash-war-manual']/div[2]/div[2]/div[1]/button";
        private const string pathButtonGreenTrenball = "//*[@id='crash-war-manual']/div[2]/div[2]/div[2]/button";
        private const string pathButtonMoonTrenball = "//*[@id='crash-war-manual']/div[2]/div[2]/div[3]/button";

        private decimal newCashout;
        private string mainPathButton;
        private string type;

        public BCGameCrashTrenball(DiceBotInfo dbInfo, WebsiteInfo wbInfo, Bet bet, int type)
            : base(dbInfo, wbInfo, bet)
        {
            switch (type)
            {
                case 1:
                    this.mainPathButton = pathButtonRedTrenball;
                    this.newCashout = 1.96m;
                    this.type = "smaller";
                    break;
                case 2:
                    this.mainPathButton = pathButtonGreenTrenball;
                    this.newCashout = 2m;
                    this.type = "bigger";
                    break;
                case 3:
                    this.mainPathButton = pathButtonMoonTrenball;
                    this.newCashout = 10m;
                    this.type = "bigger";
                    break;
            }
            this.InitPlay();
        }

        private async void PerformPlay()
        {
            while (true)
            {
                if (this.dbInfo.stop != Stop.Stop)
                {
                    var newID = int.Parse(UtilsValue.ExtractNumberFromElement(this.driver, pathResultCrashId));
                    if (oldID < newID)
                    {
                        var crash = decimal.Parse(UtilsValue.ExtractNumberFromElement(this.driver, pathResultCrashNumber));
                        var result = this.type == "smaller" ? crash <= this.newCashout : crash >= this.newCashout;

                        this.oldID = newID;
                        this.FinishBet(newID, $"{crash}x", result, 99, newCashout);
                        this.OnWinLose(this.PerformBet);
                    }

                    await Task.Delay(1230);
                    continue;
                }

                this.UpdateHistory("Đã dừng thread hiện tại !!!", Color.Black);
                break;
            }
        }

        public override async void PerformBet()
        {
            try
            {
                if (this.dbInfo.stop == Event.Stop.Stop) return;
                UtilsValue.SendText(this.driver, pathInputBetTrenball, this.bet.bet.ToString(), KeyControl.A);
                UtilsValue.SendClick(this.driver, mainPathButton);
            }
            catch
            {
                this.UpdateHistory("Đặt cược lỗi, đang thử lại sau 2s", Color.Red);
                await Task.Delay(2000);
                this.PerformBet();
            }
        }

        private async void InitPlay()
        {
            try
            {
                if (this.dbInfo.stop == Stop.Stop) return;
                UtilsValue.SendClick(this.driver, pathAllCoin);
                UtilsValue.SendClick(this.driver, pathCoin, true);
                UtilsValue.SendClick(this.driver, pathGameCrashInList);
                UtilsValue.SendClick(this.driver, pathGameCrashTrenball);

                this.UpdateHistory("Đăng nhập thành công", Color.Green);
                this.EnableAllControl(false);

                this.bet.SetBalance(decimal.Parse(UtilsValue.ExtractNumberFromElement(this.driver, pathBalance)));
                this.PerformPlay();
            }
            catch
            {
                this.UpdateHistory("Captcha, please tự đăng nhập và chờ 2s !!!", Color.Red);
                await Task.Delay(2000);
                this.InitPlay();
            }
        }

        public override void ResetSeed()
        {
            base.ResetSeed();
        }

        public override void UpdateHistory(string newContent, object color)
        {
            base.UpdateHistory(newContent, color);
        }

        public override void StopProgram()
        {
            base.StopProgram();
        }
    }
}
