﻿using Dice_Bot.Model;

namespace Dice_Bot.BCGame
{
    public class BCGameDice : BCGame
    {
        public BCGameDice(DiceBotInfo dbInfo, WebsiteInfo wbInfo, Bet bet)
            : base(dbInfo, wbInfo, bet)
        {
        }

        public override void ResetSeed()
        {
            base.ResetSeed();
        }

        public override void UpdateHistory(string newContent, object color)
        {
            base.UpdateHistory(newContent, color);
        }

        public override void StopProgram()
        {
            base.StopProgram();
        }
    }
}
