﻿using Dice_Bot.Model;
using System;
using System.Drawing;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Dice_Bot.Model.Response.WinDice;
using Newtonsoft.Json;

namespace Dice_Bot.WinDice
{
    public class WinDice : DiceBot
    {
        private const string url = "https://windice.io";
        private string seed;

        private decimal high;
        private decimal low;
        private int requestID;

        private Random rnd = new Random();
        private HttpClient client;

        public WinDice(DiceBotInfo dbInfo, WebsiteInfo wbInfo, Bet bet)
            : base(dbInfo, wbInfo, bet)
        {
            if (!this.OpenDriver(url)) return;
            this.SignIn();
        }

        public override async void PerformBet()
        {
            try
            {
                var chance = 99 / this.bet.cashout;
                if (this.bet.high)
                {
                    high = 99.99m * 100;
                    low = (99.99m - chance) * 100 + 1;
                }
                else
                {
                    high = chance * 100 - 1;
                    low = 0;
                }

                var requestBetResponse = JsonConvert.SerializeObject(new WinDiceRequestBet
                {
                    curr = this.bet.basic.coin.ToString().ToLower(),
                    bet = this.bet.bet,
                    game = "in",
                    high = (int) high,
                    low = (int) low
                });

                var content = new StringContent(requestBetResponse);
                content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

                var response = this.client.PostAsync("roll", content).Result;
                var betResponse =
                    JsonConvert.DeserializeObject<WinDiceBet>(response.Content.ReadAsStringAsync().Result);

                if (betResponse.status != "success") return;
                var roll = betResponse.data.result / 100m;
                var result = this.bet.high ? roll > 99.99m - chance : roll < chance;

                this.FinishBet(this.requestID++,$"{roll}", result, 99);
                this.OnWinLose(this.PerformBet);
            }
            catch
            {
                this.UpdateHistory("Đặt cược thất bại, đang thử lại sau 2s", Color.Red);
                await Task.Delay(2000);
                this.PerformBet();
            }
        }

        public sealed override async void SignIn()
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                       | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12
                                                       | SecurityProtocolType.Ssl3;

                var clientHandler = new HttpClientHandler
                {
                    UseCookies = true,
                    AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip
                };

                this.client = new HttpClient(clientHandler)
                {
                    BaseAddress = new Uri(url + "/api/v1/api/")
                };

                this.client.DefaultRequestHeaders.AcceptEncoding.Add(
                    new System.Net.Http.Headers.StringWithQualityHeaderValue("gzip"));
                this.client.DefaultRequestHeaders.AcceptEncoding.Add(
                    new System.Net.Http.Headers.StringWithQualityHeaderValue("deflate"));
                this.client.DefaultRequestHeaders.Add("UserAgent", "DiceBot");
                this.client.DefaultRequestHeaders.Add("Authorization", this.bet.basic.password);

                var response = this.client.GetStringAsync("user").Result;
                var balanceResponse = JsonConvert.DeserializeObject<WinDiceUser>(response);
                
                this.UpdateHistory("Đăng nhập thành công", Color.Green);
                this.bet.SetBalance((decimal) typeof(WinDiceBalance)
                    .GetProperty(this.bet.basic.coin.ToString().ToLower())
                    ?.GetValue(balanceResponse.data.balance));
                this.PerformBet();
            }
            catch
            {
                this.UpdateHistory("Đăng nhập thất bại, đang thử lại sau 2s", Color.Red);
                await Task.Delay(2000);
                this.SignIn();
            }
        }

        public override async void ResetSeed()
        {
            try
            {
                while (seed.Length < 12) seed += rnd.Next(0, int.MaxValue).ToString();
                var seedResponse = JsonConvert.SerializeObject(new WinDiceSeed {value = seed});
                var content = new StringContent(seedResponse);

                content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = this.client.PostAsync("seed", content).Result;
            }
            catch
            {
                this.UpdateHistory("Reset seed thất bại, đang thử lại sau 2s", Color.Red);
                await Task.Delay(2000);
                this.ResetSeed();
            }
        }

        public override void UpdateHistory(string newContent, object color)
        {
            base.UpdateHistory(newContent, color);
        }

        public override void StopProgram()
        {
            base.StopProgram();
        }
    }
}