﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Dice_Bot.Event;
using Dice_Bot.Model;
using Dice_Bot.Model.Response.Bitsler;
using Newtonsoft.Json;

namespace Dice_Bot.Bitsler
{
    public class Bitsler : DiceBot
    {
        private const string url = "https://www.bitsler.com";
        protected string accessToken;

        private Random rnd = new Random();
        protected HttpClient client;

        protected Bitsler(DiceBotInfo dbInfo, WebsiteInfo wbInfo, Bet bet)
            : base(dbInfo, wbInfo, bet)
        {
            if (!this.OpenDriver(url)) return;
            this.SignIn();
        }

        public sealed override async void SignIn()
        {
            try
            {
                if (this.dbInfo.stop == Stop.Stop) return;
                var clientHandler = new HttpClientHandler
                {
                    UseCookies = true,
                    AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip
                };

                this.client = new HttpClient(clientHandler)
                {
                    BaseAddress = new Uri(url)
                };

                this.client.DefaultRequestHeaders.AcceptEncoding.Add(
                    new System.Net.Http.Headers.StringWithQualityHeaderValue("gzip"));
                this.client.DefaultRequestHeaders.AcceptEncoding.Add(
                    new System.Net.Http.Headers.StringWithQualityHeaderValue("deflate"));
                this.client.DefaultRequestHeaders.Add("User-Agent", "DiceBot");

                var response = this.client.GetStringAsync(url).Result;
                var pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("username", this.bet.basic.account),
                    new KeyValuePair<string, string>("password", this.bet.basic.password),
                    new KeyValuePair<string, string>("api_key", "0b2edbfe44e98df79665e52896c22987445683e78")
                };

                var content = new FormUrlEncodedContent(pairs);
                var nResponse = this.client.PostAsync("api/login", content).Result;
                var bytes = nResponse.Content.ReadAsStringAsync().Result;
                var emitResponse = bytes;
                var loginResponse =
                    JsonConvert.DeserializeObject<BitslerLogin>(emitResponse.Replace("\"return\":", "\"_return\":"));

                if (loginResponse == null) return;
                if (loginResponse.success != "true") return;

                this.accessToken = loginResponse.access_token;
                pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("access_token", this.accessToken)
                };

                content = new FormUrlEncodedContent(pairs);
                emitResponse = this.client.PostAsync("api/getuserstats", content).Result.Content.ReadAsStringAsync()
                    .Result;
                var statResponse =
                    JsonConvert.DeserializeObject<BitslerStats>(emitResponse.Replace("\"return\":", "\"_return\":"));

                if (statResponse == null) return;
                if (statResponse.success != "true") return;
                
                this.UpdateHistory("Đăng nhập thành công", Color.Green);
                switch (this.bet.basic.coin.ToString().ToLower())
                {
                    case "btc":
                        this.bet.SetBalance(statResponse.btc_balance);
                        break;
                    case "ltc":
                        this.bet.SetBalance(statResponse.ltc_balance);
                        break;
                    case "doge":
                        this.bet.SetBalance(statResponse.doge_balance);
                        break;
                    case "eth":
                        this.bet.SetBalance(statResponse.eth_balance);
                        break;
                    case "burst":
                        this.bet.SetBalance(statResponse.burst_balance);
                        break;
                    case "dash":
                        this.bet.SetBalance(statResponse.dash_balance);
                        break;
                    case "zec":
                        this.bet.SetBalance(statResponse.zec_balance);
                        break;
                    case "bch":
                        this.bet.SetBalance(statResponse.bch_balance);
                        break;
                    case "xmr":
                        this.bet.SetBalance(statResponse.xmr_balance);
                        break;
                    case "etc":
                        this.bet.SetBalance(statResponse.etc_balance);
                        break;
                    case "neo":
                        this.bet.SetBalance(statResponse.neo_balance);
                        break;
                    case "strat":
                        this.bet.SetBalance(statResponse.strat_balance);
                        break;
                    case "kmd":
                        this.bet.SetBalance(statResponse.kmd_balance);
                        break;
                    case "xrp":
                        this.bet.SetBalance(statResponse.xrp_balance);
                        break;
                    case "btg":
                        this.bet.SetBalance(statResponse.btg_balance);
                        break;
                    case "qtum":
                        this.bet.SetBalance(statResponse.qtum_balance);
                        break;
                    case "lsk":
                        this.bet.SetBalance(statResponse.lsk_balance);
                        break;
                    case "dgb":
                        this.bet.SetBalance(statResponse.dgb_balance);
                        break;
                    case "waves":
                        this.bet.SetBalance(statResponse.waves_balance);
                        break;
                    case "btslr":
                        this.bet.SetBalance(statResponse.btslr_balance);
                        break;
                    case "bsv":
                        this.bet.SetBalance(statResponse.bsv_balance);
                        break;
                    case "xlm":
                        this.bet.SetBalance(statResponse.xlm_balance);
                        break;
                    case "usdt":
                        this.bet.SetBalance(statResponse.usdt_balance);
                        break;
                    case "trx":
                        this.bet.SetBalance(statResponse.trx_balance);
                        break;
                    case "eos":
                        this.bet.SetBalance(statResponse.eos_balance);
                        break;
                }
            }
            catch
            {
                this.UpdateHistory("Đăng nhập thất bại, đang thử lại sau 2s", Color.Red);
                await Task.Delay(2000);
                this.SignIn();
            }
        }

        public override async void ResetSeed()
        {
            try
            {
                if (this.dbInfo.stop == Stop.Stop) return;
                var pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("access_token", this.accessToken),
                    new KeyValuePair<string, string>("seed_client", rnd.Next(0, int.MaxValue).ToString())
                };

                var content = new FormUrlEncodedContent(pairs);
                var emitResponse = this.client.PostAsync("api/change-seeds", content).
                    Result.Content.ReadAsStringAsync().Result;
                JsonConvert.DeserializeObject<BitslerSeed>(emitResponse.
                    Replace("\"return\":", "\"_return\":"));
            }
            catch (WebException e)
            {
                if (e.Response != null)
                {
                    var emitResponse = new StreamReader(e.Response.GetResponseStream()).ReadToEnd();
                    if (e.Message.Contains("429"))
                    {
                        await Task.Delay(2000);
                        this.ResetSeed();
                    }
                }
            }
        }
    }
}