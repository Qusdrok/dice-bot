﻿using System.Collections.Generic;
using System.Drawing;
using System.Net.Http;
using System.Threading.Tasks;
using Dice_Bot.Event;
using Dice_Bot.Model;
using Dice_Bot.Model.Response.Bitsler;
using Newtonsoft.Json;

namespace Dice_Bot.Bitsler
{
    public class BitslerDice : Bitsler
    {
        private int requestID;

        protected BitslerDice(DiceBotInfo dbInfo, WebsiteInfo wbInfo, Bet bet)
            : base(dbInfo, wbInfo, bet)
        {
            this.PerformBet();
        }

        public sealed override async void PerformBet()
        {
            try
            {
                if (this.dbInfo.stop == Stop.Stop) return;
                var pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("access_token", this.accessToken),
                    new KeyValuePair<string, string>("amount", this.bet.bet.ToString("0.00000000")),
                    new KeyValuePair<string, string>("over", this.bet.high.ToString().ToLower()),
                    new KeyValuePair<string, string>("target",
                        !this.bet.high
                            ? (99 / this.bet.cashout).ToString("0.00")
                            : (99.99m - 99 / this.bet.cashout).ToString("0.00")),
                    new KeyValuePair<string, string>("currency", this.bet.basic.coin.ToString().ToLower()),
                    new KeyValuePair<string, string>("api_key", "0b2edbfe44e98df79665e52896c22987445683e78")
                };

                var content = new FormUrlEncodedContent(pairs);
                var response = this.client.PostAsync("api/bet-dice", content).Result;
                var emitResponse = response.Content.ReadAsStringAsync().Result;
                var betResponse = JsonConvert.DeserializeObject<BitslerBet>(emitResponse.
                    Replace("\"return\":", "\"_return\":"));

                if (betResponse == null) return;
                if (!betResponse.success) return;
                var result = this.bet.high
                    ? betResponse.result > 99.99m - 99 / this.bet.cashout
                    : betResponse.result < 99 / this.bet.cashout;

                this.FinishBet(this.requestID++, $"{betResponse.result}", result, 99);
                this.OnWinLose(this.PerformBet);
            }
            catch
            {
                this.UpdateHistory("Đặt cược thất bại, đang thử lại sau 2s",Color.Red);
                await Task.Delay(2000);
                this.PerformBet();
            }
        }

        public override void ResetSeed()
        {
            base.ResetSeed();
        }

        public override void UpdateHistory(string newContent, object color)
        {
            base.UpdateHistory(newContent, color);
        }

        public override void StopProgram()
        {
            base.StopProgram();
        }
    }
}