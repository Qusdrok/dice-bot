﻿using System;
using Dice_Bot.Model;

namespace Dice_Bot.LuckyFish
{
    public class LuckyFish : DiceBot
    {
        public LuckyFish(DiceBotInfo dbInfo, WebsiteInfo wbInfo, Bet bet, bool isInit = true)
            : base(dbInfo, wbInfo, bet)
        {
            this.SignIn();
        }

        public sealed override void SignIn()
        {
            base.SignIn();
        }

        public override void ResetSeed()
        {
            base.ResetSeed();
        }
    }
}
