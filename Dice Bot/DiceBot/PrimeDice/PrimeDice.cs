﻿using System;
using System.Drawing;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using Dice_Bot.Model;
using Dice_Bot.Model.Response.PrimeDice;
using GraphQL.Client;
using GraphQL.Common.Request;

namespace Dice_Bot.PrimeDice
{
    public class PrimeDice : DiceBot
    {
        private const string url = "https://primedice.com/?c=Seuntjie";
        private const string urlAPI = "https://api.primedice.com/graphql";

        private const string rollName = "primediceRoll";
        private const string gameName = "CasinoGamePrimedice";
        private const string enumName = "CasinoGamePrimediceConditionEnum";
        private const string statGameName = "primedice";
        private int requestID;

        private Random rnd = new Random();
        private GraphQLClient GQLClient;

        public PrimeDice(DiceBotInfo dbInfo, WebsiteInfo wbInfo, Bet bet)
            : base(dbInfo, wbInfo, bet)
        {
            if (!this.OpenDriver(url)) return;
            this.SignIn();
        }

        public override async void ResetSeed()
        {
            try
            {
                var loginRequest = new GraphQLRequest
                {
                    Query = "mutation DiceBotRotateSeed " +
                        "($seed: String!){rotateServerSeed{ seed seedHash nonce } " +
                        "changeClientSeed(seed: $seed){seed}}",
                    Variables = new
                    {
                        seed = rnd.Next(0, int.MaxValue).ToString()
                    }
                };

                var response = this.GQLClient.PostAsync(loginRequest).Result;
                var seedResponse = response.GetDataFieldAs<PrimeDiceSeed>("rotateServerSeed");
            }
            catch
            {
                this.UpdateHistory("Reset seed lỗi, đang thử lại sau 2s", Color.Red);
                await Task.Delay(2000);
                this.ResetSeed();
            }
        }

        public override async void SignIn()
        {
            while (true)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                GQLClient = new GraphQLClient(urlAPI);
                GQLClient.DefaultRequestHeaders.Add("x-access-token", this.bet.basic.account);

                var loginRequest = new GraphQLRequest
                {
                    Query = "query " +
                            "DiceBotLogin{user {activeServerSeed { seedHash seed nonce} activeClientSeed{seed} " +
                            "id balances{available{currency amount}} " +
                            "statistic {game bets wins losses betAmount profit currency}}}"
                };
                var response = GQLClient.PostAsync(loginRequest).Result;
                var userResponse = response.GetDataFieldAs<PrimeDiceUser>("user");

                if (string.IsNullOrWhiteSpace(userResponse.id))
                {
                    this.UpdateHistory("Đăng nhập thất bại, đang thử lại sau 2s", Color.Red);
                    await Task.Delay(2000);
                    continue;
                }

                foreach (var balance in userResponse.balances)
                {
                    if (!string.Equals(balance.available.currency, this.bet.basic.coin.ToString(),
                        StringComparison.CurrentCultureIgnoreCase)) continue;
                    this.bet.SetBalance((decimal) balance.available.amount);
                    break;
                }

                this.UpdateHistory("Đăng nhập thành công", Color.Green);
                break;
            }
        }

        public override async void PerformBet()
        {
            try
            {
                var chance = 99.99m - 99 / this.bet.cashout;
                var request = new GraphQLRequest
                {
                    Query = @"mutation DiceBotDiceBet($amount: Float! 
                  $target: Float!
                  $condition: " + enumName + @"!
                  $currency: CurrencyEnum!
                  $identifier: String!){ " + rollName +
                            "(amount: $amount, target: $target,condition: $condition,currency: $currency, identifier: $identifier)" +
                            " { id nonce currency amount payout state { ... on " + gameName +
                            " { result target condition } } " +
                            "createdAt serverSeed{seedHash seed nonce} " +
                            "clientSeed{seed} user{balances{available{amount currency}} " +
                            "statistic{game bets wins losses betAmount profit currency}}}}",
                    Variables = new
                    {
                        amount = this.bet.bet,
                        target = chance,
                        condition = "above",
                        currency = this.bet.basic.coin.ToString().ToLower(),
                        identifier = "0123456789abcdef"
                    }
                };

                var response = GQLClient.PostAsync(request).Result;
                if (response.Errors != null && response.Errors.Length > 0)
                    this.UpdateHistory(response.Errors[0].Message, Color.Black);

                if (response.Data == null) return;
                var betResponse = response.GetDataFieldAs<PrimeDiceBet>(rollName);

                var roll = (decimal) betResponse.state.result;
                var result = roll > 99.99m - (99.99m - (decimal) betResponse.state.target);

                this.FinishBet(this.requestID++, $"{roll}", result, 99);
                var idRequest = new GraphQLRequest
                {
                    Query = " query DiceBotGetBetId($betId: String!){bet(betId: $betId){iid}}",
                    Variables = new
                    {
                        betId = this.requestID
                    }
                };

                var nBetReponse = this.GQLClient.PostAsync(idRequest).Result;
                if (nBetReponse.Data == null) return;

                this.bet.betID = nBetReponse.Data.bet.iid.ToString();
                if (this.bet.betID.Contains("house:")) this.bet.betID = this.bet.betID.Substring("house:".Length);
            }
            catch
            {
                this.UpdateHistory("Đặt cược thất bại, đang thử lại sau 2s", Color.Red);
                await Task.Delay(2000);
                this.PerformBet();
            }
        }

        public override void UpdateHistory(string newContent, object color)
        {
            base.UpdateHistory(newContent, color);
        }

        public override void StopProgram()
        {
            base.StopProgram();
        }
    }
}