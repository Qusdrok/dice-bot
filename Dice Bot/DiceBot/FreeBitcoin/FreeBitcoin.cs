﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Dice_Bot.Event;
using Dice_Bot.Model;
using Dice_Bot.Model.Response.FreeBitcoin;
using Newtonsoft.Json;
using Utility;

namespace Dice_Bot.FreeBitcoin
{
    public class FreeBitcoin : DiceBot
    {
        private const string url = "https://freebitco.in/";
        private const string urlAPIStat = "https://freebitco.in/cgi-bin/api.pl?op=get_user_stats";
        private const string urlAPIBet = "https://freebitco.in/cgi-bin/bet.pl?";

        private const string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZqwertyuiopasdfghjklzxcvbnm1234567890";
        private string cookieCsrf;
        private string clientSeed;

        private int requestID;

        private Random rnd = new Random();
        private CookieContainer cookies = new CookieContainer();
        private HttpClient client;

        public FreeBitcoin(DiceBotInfo dbInfo, WebsiteInfo wbInfo, Bet bet)
            : base(dbInfo, wbInfo, bet)
        {
            if (!this.OpenDriver(url)) return;
            this.SignIn();
        }

        public sealed override async void SignIn()
        {
            try
            {
                if (this.dbInfo.stop == Stop.Stop) return;
                var clientHandler = new HttpClientHandler
                {
                    UseCookies = true,
                    CookieContainer = cookies,
                    AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip
                };

                client = new HttpClient(clientHandler)
                {
                    BaseAddress = new Uri(url)
                };

                client.DefaultRequestHeaders.AcceptEncoding.Add(new System.Net.Http.Headers.StringWithQualityHeaderValue("gzip"));
                client.DefaultRequestHeaders.AcceptEncoding.Add(new System.Net.Http.Headers.StringWithQualityHeaderValue("deflate"));

                this.CloudflareBypass(clientHandler);
                foreach (Cookie cookie in this.cookies.GetCookies(new Uri(url)))
                {
                    if (cookie.Name != "csrf_token") continue;
                    this.cookieCsrf = cookie.Value;
                    break;
                }

                var pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("csrf_token", this.cookieCsrf),
                    new KeyValuePair<string, string>("op", "login_new"),
                    new KeyValuePair<string, string>("btc_address", this.bet.basic.account),
                    new KeyValuePair<string, string>("password", this.bet.basic.password),
                    new KeyValuePair<string, string>("tfa_code", ""),
                };

                var content = new FormUrlEncodedContent(pairs);
                var emitResponse = client.PostAsync("", content).Result;

                if (!emitResponse.IsSuccessStatusCode) return;
                var response = emitResponse.Content.ReadAsStringAsync().Result;
                var message = response.Split(':');

                if (message.Length <= 2) return;
                this.cookies.Add(new Cookie("btc_address", message[1], "/", "freebitco.in"));
                this.cookies.Add(new Cookie("password", message[2], "/", "freebitco.in"));
                this.cookies.Add(new Cookie("have_account", "1", "/", "freebitco.in"));

                response = client.GetStringAsync(urlAPIStat).Result;
                var statResponse = JsonConvert.DeserializeObject<FreeBitcoinStat>(response);
                if (statResponse == null) return;
                
                this.UpdateHistory("Đăng nhập thành công", Color.Green);
                this.bet.SetBalance(statResponse.balance / 100000000m);
                this.PerformBet();
            }
            catch
            {
                this.UpdateHistory("Đăng nhập thất bại, đang thử lại sau 2s", Color.Red);
                await Task.Delay(2000);
                this.SignIn();
            }
        }

        public override async void PerformBet()
        {
            try
            {
                if (this.dbInfo.stop == Stop.Stop) return;
                while (this.clientSeed.Length < 16) this.clientSeed += alphabet[rnd.Next(0, alphabet.Length)];
                var _params = string.Format(System.Globalization.NumberFormatInfo.InvariantInfo,
                    "m={0}&client_seed={1}&jackpot=0&stake={2}&multiplier={3}&rand={5}&csrf_token={4}",
                    this.bet.high ? "hi" : "lo", this.clientSeed, this.bet.bet, this.bet.cashout,
                    this.cookieCsrf, rnd.Next(0, 9999999) / 10000000);
                var response = this.client.GetAsync(urlAPIBet + _params).Result;
                if (!response.IsSuccessStatusCode) return;
                var betResponse = response.Content.ReadAsStringAsync().Result;
                var message = betResponse.Split(':');

                if (message.Length <= 2) return;
                //1. Success code (s1)
                //2. Result (w/l)
                //3. Rolled number
                //4. User balance
                //5. Amount won or lost (always positive). If 2. is l, then amount is subtracted from balance else if w it is added.
                //6. Redundant (can ignore)
                //7. Server seed hash for next roll
                //8. Client seed of previous roll
                //9. Nonce for next roll
                //10. Server seed for previous roll
                //11. Server seed hash for previous roll
                //12. Client seed again (can ignore)
                //13. Previous nonce
                //14. Jackpot result (1 if won 0 if not won)
                //15. Redundant (can ignore)
                //16. Jackpot amount won (0 if lost)
                //17. Bonus account balance after bet
                //18. Bonus account wager remaining
                //19. Max. amount of bonus eligible
                //20. Max bet
                //21. Account balance before bet
                //22. Account balance after bet
                //23. Bonus account balance before bet
                //24. Bonus account balance after bet
                var result = message[1] == "w";
                var roll = decimal.Parse(message[2]) / 100m;

                this.FinishBet(this.requestID++, $"{roll}", result, 95);
                this.OnWinLose(this.PerformBet);
            }
            catch
            {
                this.UpdateHistory("Đặt cược lỗi, đang thử lại sau 2s !!!", Color.Red);
                await Task.Delay(2000);
                this.PerformBet();
            }
        }

        private async void CloudflareBypass(HttpClientHandler clientHandler)
        {
            while (true)
            {
                if (this.dbInfo.stop == Stop.Stop) return;
                var response = string.Empty;
                var responseMessage = client.GetAsync("").Result;

                if (responseMessage.IsSuccessStatusCode)
                {
                    response = responseMessage.Content.ReadAsStringAsync().Result;
                }
                else
                {
                    if (responseMessage.StatusCode != HttpStatusCode.ServiceUnavailable) return;
                    response = responseMessage.Content.ReadAsStringAsync().Result;

                    if (UtilsCloudflare.CloudflareBypass(client, clientHandler, response, "freebitco.in", 0)) return;
                    this.UpdateHistory("Đăng nhập thất bại, đang thử lại sau 2s", Color.Red);
                    
                    await Task.Delay(2000);
                    continue;
                }

                break;
            }
        }

        public override void ResetSeed()
        {
            base.ResetSeed();
        }

        public override void UpdateHistory(string newContent, object color)
        {
            base.UpdateHistory(newContent, color);
        }

        public override void StopProgram()
        {
            base.StopProgram();
        }
    }
}
