﻿using System;
using Dice_Bot.Model;

namespace Dice_Bot.GigaBet
{
    public class GigaBet : DiceBot
    {
        public GigaBet(DiceBotInfo dbInfo, WebsiteInfo wbInfo, Bet bet)
            : base(dbInfo, wbInfo, bet)
        {
            this.SignIn();
        }

        public override void PerformBet()
        {
            base.PerformBet();
        }

        public sealed override void SignIn()
        {
            base.SignIn();
        }

        public override void ResetSeed()
        {
            base.ResetSeed();
        }

        public override void UpdateHistory(string newContent, object color)
        {
            base.UpdateHistory(newContent, color);
        }

        public override void StopProgram()
        {
            base.StopProgram();
        }
    }
}
