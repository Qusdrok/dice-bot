﻿using System;

namespace Dice_Bot.Interface
{
    internal interface BaseDiceBot
    {
        void UpdateHistory(string newContent, object color);
        void StopProgram();
        void SignIn();
        void PerformBet();
        void ResetSeed();
        bool Withdraw(decimal amount, string address);
        void WithdrawClick(object sender, EventArgs e);
    }
}
