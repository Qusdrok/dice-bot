﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Dice_Bot.Model;

namespace Dice_Bot.Event
{
    public delegate void EventUpdateHistory(string newContent, Color color, int threadIndex = -1);
    public delegate void EventAddDataToDGV(DataGridView dataGridView, Bet bet);
    public delegate void EventEnableAllControl(bool enable, bool isProgrammer = false);
    public delegate void EventUpdateTotalBet(bool result);
    public delegate long EventAddDataToChart(Chart chart, Bet bet, long chartBets);

    public enum Game
    {
        CrashClassic,
        CrashTrenballRed,
        CrashTrenballGreen,
        CrashTrenballMoon,
        Dice
    }

    public enum Browser
    {
        Chrome,
        FireFox
    }

    public enum Website
    {
        BCGame, 95
        Bitsler,
        Dice999,
        EtherCrash,
        PrimeDice,
        Stake,
        FreeBitcoin,
        GigaBet,
        KingDice,
        LuckyFish,
        WinDice,
        WolfBet
    }

    public enum Stop
    {
        None,
        Pause,
        Stop
    }

    public enum Coin
    {
        AAVE,
        AMPL,
        AVC,
        BAT,
        BCH,
        BSV,
        BTC,
        BTG,
        BTSLR,
        BTT,
        BURST,
        DAI,
        DASH,
        DGB,
        DOGE,
        DOT,
        ENJ,
        EOS,
        ETC,
        ETH,
        EURS,
        JST,
        KMD,
        LINK,
        LSK,
        LTC,
        MANA,
        NBX,
        NEO,
        QTUM,
        SATS,
        SNX,
        STRAT,
        SUN,
        SUSHI,
        TRTL,
        TRX,
        UNI,
        USDC,
        USDT,
        VNDC,
        VSYS,
        WAVES,
        WBTC,
        XLM,
        XMR,
        XRP,
        YFI,
        ZEC
    }

    public static class Event
    {
        public static readonly List<WebsiteInfo> WebsiteInfos = new List<WebsiteInfo>
        {
            #region BCGame
            new WebsiteInfo
            (
                new List<CoinInfo>
                {
                    new CoinInfo(Coin.BTC, 0.0000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[1]"),
                    new CoinInfo(Coin.SATS, 10m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[2]"),
                    new CoinInfo(Coin.ETH, 0.0000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[3]"),
                    new CoinInfo(Coin.XRP, 0.0000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[4]"),
                    new CoinInfo(Coin.DOGE, 0.0000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[5]"),
                    new CoinInfo(Coin.USDT, 0.0001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[6]"),
                    new CoinInfo(Coin.LTC, 0.000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[7]"),
                    new CoinInfo(Coin.XLM, 0.0000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[8]"),
                    new CoinInfo(Coin.TRX, 0.0001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[9]"),
                    new CoinInfo(Coin.BCH, 0.000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[10]"),
                    new CoinInfo(Coin.LINK, 0.0000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[11]"),
                    new CoinInfo(Coin.DOT, 0.00000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[12]"),
                    new CoinInfo(Coin.EOS, 0.0000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[13]"),
                    new CoinInfo(Coin.DAI, 0.0000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[14]"),
                    new CoinInfo(Coin.USDC, 0.0001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[15]"),
                    new CoinInfo(Coin.XMR, 0.0000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[16]"),
                    new CoinInfo(Coin.UNI, 0.0000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[17]"),
                    new CoinInfo(Coin.BSV, 0.000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[18]"),
                    new CoinInfo(Coin.AVC, 0.000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[19]"),
                    new CoinInfo(Coin.VSYS, 0.0000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[20]"),
                    new CoinInfo(Coin.SUSHI, 0.0000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[21]"),
                    new CoinInfo(Coin.AAVE, 0.0000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[22]"),
                    new CoinInfo(Coin.WBTC, 0.0000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[23]"),
                    new CoinInfo(Coin.SNX, 0.0000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[24]"),
                    new CoinInfo(Coin.YFI, 0.00000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[25]"),
                    new CoinInfo(Coin.AMPL, 0.0000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[26]"),
                    new CoinInfo(Coin.MANA, 0.0000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[27]"),
                    new CoinInfo(Coin.BAT, 0.0000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[28]"),
                    new CoinInfo(Coin.ENJ, 0.0000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[29]"),
                    new CoinInfo(Coin.EURS, 0.00001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[30]"),
                    new CoinInfo(Coin.VNDC, 100m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[31]"),
                    new CoinInfo(Coin.NBX, 0.0000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[32]"),
                    new CoinInfo(Coin.BTT, 0.01m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[33]"),
                    new CoinInfo(Coin.TRTL, 0.0000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[34]"),
                    new CoinInfo(Coin.JST, 0.0000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[35]"),
                    new CoinInfo(Coin.SUN, 0.0000001m, true,
                        "//*[@id='header']/div[1]/div[2]/div[1]/div[1]/div[2]/div/div[36]"),
                },
                new List<Game>
                {
                    Game.CrashClassic,
                    Game.CrashTrenballRed,
                    Game.CrashTrenballGreen,
                    Game.CrashTrenballMoon
                },
                Website.BCGame, 99
            ),
            #endregion

            #region EtherCrash
            new WebsiteInfo
            (
                new List<CoinInfo>
                {
                    new CoinInfo(Coin.ETH, 0.000001m)
                },
                new List<Game>
                {
                    Game.CrashClassic
                },
                Website.EtherCrash, 99
            ),
            #endregion

            #region Dice999
            new WebsiteInfo
            (
                new List<CoinInfo>
                {
                    new CoinInfo(Coin.BTC, 0.0000001m),
                    new CoinInfo(Coin.DOGE, 0.0000001m),
                    new CoinInfo(Coin.LTC, 0.0000001m),
                    new CoinInfo(Coin.ETH, 0.0000001m)
                },
                new List<Game>
                {
                    Game.Dice
                },
                Website.Dice999, 99
            ),
            #endregion

            #region PrimeDice
            new WebsiteInfo
            (
                new List<CoinInfo>
                {
                    new CoinInfo(Coin.BTC, 0.0000001m),
                    new CoinInfo(Coin.ETH, 0.0000001m),
                    new CoinInfo(Coin.LTC, 0.0000001m),
                    new CoinInfo(Coin.DOGE, 0.0000001m),
                    new CoinInfo(Coin.BCH, 0.0000001m),
                    new CoinInfo(Coin.XRP, 0.0000001m),
                    new CoinInfo(Coin.TRX, 0.0000001m),
                    new CoinInfo(Coin.EOS, 0.0000001m)
                },
                new List<Game>
                {
                    Game.Dice
                },
                Website.PrimeDice, 99
            ),
            #endregion

            #region FreeBitcoin
            new WebsiteInfo
            (
                new List<CoinInfo>
                {
                    new CoinInfo(Coin.BTC, 0.0000001m)
                },
                new List<Game>
                {
                    Game.Dice
                },
                Website.BCGame
            ),
            #endregion
        };

        public static readonly Dictionary<Coin, string> DepositCoin = new Dictionary<Coin, string>
        {
            {Coin.BTC, "bc1q5w0gt8du285v9d4l54lwu5e7r4ac88ysrah5lg"},
            {Coin.ETH, "0x70a445b2b880e1dfd7f3126bac069b50716249cb"},
            {Coin.XRP, "rKyNc3N5m4RUe17gPp6th69ri8WHaX3HQP"},
            {Coin.DOGE, "DTboJ5v3MnzawnaJ7ZhctP1BLuxi8q3g4m"},
            {Coin.USDT, "0x70a445b2b880e1dfd7f3126bac069b50716249cb"},
            {Coin.LTC, "MC2cbGrWshQ6HuNsKqzV6Kq6tZfiU7SGdo"},
            {Coin.XLM, "GCUXDW3P24XXXRFW5YJJCYHOK7KJHIPXBLZSFGGNZ62L7KHZZSPCMTL7"},
            {Coin.TRX, "TYEzWKkmq6gZuUpRfvbTGFhAWb54GpHg15"},
            {Coin.BCH, "bitcoincash:qzm62jkmqx9wdgpesht7e550d9e8uznm8g2r4urljh"},
            {Coin.LINK, "0x70a445b2b880e1dfd7f3126bac069b50716249cb"},
            {Coin.DOT, "12W96dxkUTUHUH1NgD1B2tPWq78Cb8LDb1CwA8w2rat2RDkq"},
            {Coin.DAI, "0x70a445b2b880e1dfd7f3126bac069b50716249cb"},
            {Coin.USDC, "0x70a445b2b880e1dfd7f3126bac069b50716249cb"},
            {
                Coin.XMR,
                "89YiTX61DULSd11YhBxVA56JhSxzzB8P8hVfdoAB7yDiCGHX5846qR4Gvsq3Qv52TfaU9sGwgKQAJCn8CTCMXazaExLKP7a"
            },
            {Coin.UNI, "0x70a445b2b880e1dfd7f3126bac069b50716249cb"},
            {Coin.BSV, "1J3GcuX2ZKNRpAEgd7npvLEQj6X1X2SN9v"}
        };
    }
}