﻿using Dice_Bot.Event;
using System.Collections.Generic;

namespace Dice_Bot.Model
{
    public class WebsiteInfo
    {
        public List<CoinInfo> coinInfos;
        public List<Game> games;
        public Website website;
        public int divideChance;

        public WebsiteInfo(List<CoinInfo> coinInfos, List<Game> games, Website website, int divideChance)
        {
            this.coinInfos = coinInfos;
            this.games = games;
            this.website = website;
            this.divideChance = divideChance;
        }
    }
}
