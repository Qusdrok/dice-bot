﻿using Dice_Bot.Model;
using System;

namespace Dice_Bot
{
    public class Bet
    {
        public string betID;
        public string roll;
        public bool high;
        public decimal bet;
        public decimal cashout;
        public decimal winChance;
        public decimal profitWin;
        public decimal profitLose;
        public decimal totalProfit;
        public decimal balance;
        public decimal rootBalance;
        public int win;
        public int lose;
        public DateTime time;
        public Basic basic;

        public Bet(bool high, decimal bet, decimal cashout, DateTime time, Basic basic)
        {
            this.high = high;
            this.bet = bet;
            this.cashout = cashout;
            this.time = time;
            this.basic = basic;
        }

        public void SetBalance(decimal balance)
        {
            this.balance = balance;
            this.rootBalance = balance;
        }
    }
}
