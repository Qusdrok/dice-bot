﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.EtherCrash
{
    public class EtherCrashLogin
    {
        [JsonProperty("state")]
        public string state { get; set; }

        [JsonProperty("game_id")]
        public int game_id { get; set; }

        [JsonProperty("last_hash")]
        public string last_hash { get; set; }

        [JsonProperty("max_win")]
        public double max_win { get; set; }

        [JsonProperty("elapsed")]
        public int elapsed { get; set; }

        [JsonProperty("created")]
        public string created { get; set; }

        [JsonProperty("username")]
        public string username { get; set; }

        [JsonProperty("balance_satoshis")]
        public long balance_satoshis { get; set; }
    }
}
