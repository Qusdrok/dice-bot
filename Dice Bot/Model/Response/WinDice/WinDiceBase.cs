﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.WinDice
{
    public class WinDiceBase
    {
        [JsonProperty("status")]
        public string status { get; set; }
        
        [JsonProperty("message")]
        public string message { get; set; }
    }
}