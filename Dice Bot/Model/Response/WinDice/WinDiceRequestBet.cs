﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.WinDice
{
    public class WinDiceRequestBet
    {
        [JsonProperty("curr")]
        public string curr { get; set; }
        
        [JsonProperty("bet")]
        public decimal bet { get; set; }
        
        [JsonProperty("game")]
        public string game { get; set; }
        
        [JsonProperty("low")]
        public int low { get; set; }
        
        [JsonProperty("high")]
        public int high { get; set; }
    }
}