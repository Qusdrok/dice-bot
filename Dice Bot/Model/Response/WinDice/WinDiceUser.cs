﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.WinDice
{
    public class WinDiceUser : WinDiceBase
    {
        [JsonProperty("data")]
        public WinDiceUser data { get; set; }
        
        [JsonProperty("hash")]
        public string hash { get; set; }
        
        [JsonProperty("username")]
        public string username { get; set; }
        
        [JsonProperty("avatar")]
        public string avatar { get; set; }
        
        [JsonProperty("rating")]
        public int rating { get; set; }
        
        [JsonProperty("reg_time")]
        public int reg_time { get; set; }
        
        [JsonProperty("hide_profit")]
        public bool hide_profit { get; set; }
        
        [JsonProperty("hide_bet")]
        public bool hide_bet { get; set; }
        
        [JsonProperty("balance")]
        public WinDiceBalance balance { get; set; }
    }
}