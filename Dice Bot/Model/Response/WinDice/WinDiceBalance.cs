﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.WinDice
{
    public class WinDiceBalance
    {
        [JsonProperty("btc")]
        public decimal btc { get; set; }
        
        [JsonProperty("eth")]
        public decimal eth { get; set; }
        
        [JsonProperty("ltc")]
        public decimal ltc { get; set; }
        
        [JsonProperty("doge")]
        public decimal doge { get; set; }
        
        [JsonProperty("bch")]
        public decimal bch { get; set; }
        
        [JsonProperty("xrp")]
        public decimal xrp { get; set; }
        
        [JsonProperty("win")]
        public decimal win { get; set; }
    }
}