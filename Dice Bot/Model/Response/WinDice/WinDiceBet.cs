﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.WinDice
{
    public class WinDiceBet : WinDiceBase
    {
        [JsonProperty("data")]
        public WinDiceBet data { get; set; }
        
        [JsonProperty("hash")]
        public string hash { get; set; }
        
        [JsonProperty("userHash")]
        public string userHash { get; set; }
        
        [JsonProperty("username")]
        public string username { get; set; }
        
        [JsonProperty("nonce")]
        public int nonce { get; set; }
        
        [JsonProperty("curr")]
        public string curr { get; set; }
        
        [JsonProperty("bet")]
        public decimal bet { get; set; }
        
        [JsonProperty("win")]
        public decimal win { get; set; }
        
        [JsonProperty("jackpot")]
        public decimal jackpot { get; set; }
        
        [JsonProperty("pointLow")]
        public decimal pointLow { get; set; }
        
        [JsonProperty("pointHigh")]
        public decimal pointHigh { get; set; }
        
        [JsonProperty("game")]
        public string game { get; set; }
        
        [JsonProperty("chance")]
        public decimal chance { get; set; }
        
        [JsonProperty("payout")]
        public decimal payout { get; set; }
        
        [JsonProperty("result")]
        public decimal result { get; set; }
        
        [JsonProperty("time")]
        public decimal time { get; set; }
        
        [JsonProperty("isHigh")]
        public bool isHigh { get; set; }
    }
}