﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.WinDice
{
    public class WinDiceSeed
    {
        [JsonProperty("value")]
        public string value { get; set; }
    }
}