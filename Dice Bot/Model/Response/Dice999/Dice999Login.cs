﻿using Newtonsoft.Json;

namespace Dice_Bot.Model
{
    public class Dice999Login
    {
        [JsonProperty("SessionCookie")]
        public string SessionCookie { get; set; }

        [JsonProperty("MaxBetBatchSize")]
        public long MaxBetBatchSize { get; set; }

        [JsonProperty("ClientSeed")]
        public long ClientSeed { get; set; }

        [JsonProperty("ReferredById")]
        public object ReferredById { get; set; }

        [JsonProperty("BetCount")]
        public long BetCount { get; set; }

        [JsonProperty("BetPayIn")]
        public long BetPayIn { get; set; }

        [JsonProperty("BetPayOut")]
        public long BetPayOut { get; set; }

        [JsonProperty("BetWinCount")]
        public long BetWinCount { get; set; }

        [JsonProperty("AccountId")]
        public long AccountId { get; set; }

        [JsonProperty("Balance")]
        public long Balance { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("EmergencyAddress")]
        public object EmergencyAddress { get; set; }

        [JsonProperty("DepositAddress")]
        public string DepositAddress { get; set; }

        [JsonProperty("Doge")]
        public Doge Doge { get; set; }

        [JsonProperty("LTC")]
        public Doge Ltc { get; set; }

        [JsonProperty("ETH")]
        public Doge Eth { get; set; }
    }

    public class Doge
    {
        [JsonProperty("BetCount")]
        public long BetCount { get; set; }

        [JsonProperty("BetPayIn")]
        public long BetPayIn { get; set; }

        [JsonProperty("BetPayOut")]
        public long BetPayOut { get; set; }

        [JsonProperty("BetWinCount")]
        public long BetWinCount { get; set; }

        [JsonProperty("Balance")]
        public long Balance { get; set; }

        [JsonProperty("DepositAddress")]
        public string DepositAddress { get; set; }
    }
}
