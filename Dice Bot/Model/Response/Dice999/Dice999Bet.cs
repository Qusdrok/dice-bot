﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Dice999_Response
{
    public class Dice999Bet
    {
        [JsonProperty("BetId")]
        public long BetId { get; set; }

        [JsonProperty("PayOut")]
        public long PayOut { get; set; }

        [JsonProperty("Secret")]
        public long Secret { get; set; }

        [JsonProperty("StartingBalance")]
        public long StartingBalance { get; set; }

        [JsonProperty("ServerSeed")]
        public string ServerSeed { get; set; }

        [JsonProperty("Next")]
        public string Next { get; set; }

        [JsonProperty("ChanceTooHigh")]
        public int ChanceTooHigh { get; set; }

        [JsonProperty("ChanceTooLow")]
        public int ChanceTooLow { get; set; }

        [JsonProperty("InsufficientFunds")]
        public int InsufficientFunds { get; set; }

        [JsonProperty("NoPossibleProfit")]
        public int NoPossibleProfit { get; set; }

        [JsonProperty("MaxPayoutExceeded")]
        public int MaxPayoutExceeded { get; set; }
    }
}
