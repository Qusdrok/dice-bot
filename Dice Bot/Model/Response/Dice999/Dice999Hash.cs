﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Dice999_Response
{
    public class Dice999Hash
    {
        [JsonProperty("Hash")]
        public string Hash { get; set; }
    }
}
