﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.WolfBet
{
    public class WolfBetNameGame
    {
        [JsonProperty("name")]
        public string name { get; set; }
    }
}