﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.WolfBet
{
    public class WolfBetProfile
    {
        [JsonProperty("user")]
        public WolfBetUser user { get; set; }
        
        [JsonProperty("balances")]
        public List<WolfBetBalance> balances { get; set; }
    }
}