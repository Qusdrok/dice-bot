﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.WolfBet
{
    public class WolfBetUser
    {
        [JsonProperty("login")]
        public string login { get; set; }
        
        [JsonProperty("email")]
        public string email { get; set; }
        
        [JsonProperty("two_factor_authentication")]
        public bool two_factor_authentication { get; set; }
        
        [JsonProperty("has_email_to_verify")]
        public bool has_email_to_verify { get; set; }
        
        [JsonProperty("last_nonce")]
        public string last_nonce { get; set; }
        
        [JsonProperty("seed")]
        public string seed { get; set; }
        
        [JsonProperty("channel")]
        public string channel { get; set; }
        
        [JsonProperty("joined")]
        public string joined { get; set; }
       
        [JsonProperty("balances")]
        public List<WolfBetBalance> balances { get; set; }
        
        [JsonProperty("games")]
        public List<WolfBetGame> games { get; set; }
    }
}