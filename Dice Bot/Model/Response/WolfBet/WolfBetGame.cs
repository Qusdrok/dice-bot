﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.WolfBet
{
    public class WolfBetGame
    {
        [JsonProperty("server_seed_hashed")]
        public string server_seed_hashed { get; set; }
        
        [JsonProperty("game")]
        public WolfBetNameGame game { get; set; }
    }
}