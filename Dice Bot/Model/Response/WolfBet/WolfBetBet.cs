﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.WolfBet
{
    public class WolfBetBet
    {
        [JsonProperty("hash")]
        public string hash { get; set; }
        
        [JsonProperty("nonce")]
        public int nonce { get; set; }
        
        [JsonProperty("user_seed")]
        public string user_seed { get; set; }
        
        [JsonProperty("currency")]
        public string currency { get; set; }
        
        [JsonProperty("amount")]
        public string amount { get; set; }
        
        [JsonProperty("profit")]
        public string profit { get; set; }
        
        [JsonProperty("multiplier")]
        public string multiplier { get; set; }
        
        [JsonProperty("bet_value")]
        public string bet_value { get; set; }
        
        [JsonProperty("result_value")]
        public string result_value { get; set; }
        
        [JsonProperty("state")]
        public string state { get; set; }
        
        [JsonProperty("published_at")]
        public int published_at { get; set; }
        
        [JsonProperty("server_seed_hashed")]
        public string server_seed_hashed { get; set; }
        
        [JsonProperty("user")]
        public WolfBetUser user { get; set; }
        
        [JsonProperty("game")]
        public WolfBetGame game { get; set; }
    }
}