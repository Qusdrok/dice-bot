﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.WolfBet
{
    public class WolfBetResult
    {
        [JsonProperty("bet")]
        public WolfBetBet bet { get; set; }
        
        [JsonProperty("userBalance")]
        public WolfBetBalance userBalance { get; set; }
    }
}