﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.WolfBet
{
    public class WolfBetRequestBet
    {
        [JsonProperty("currency")]
        public string currency { get; set; }
        
        [JsonProperty("game")]
        public string game { get; set; }
        
        [JsonProperty("amount")]
        public string amount { get; set; }
        
        [JsonProperty("rule")]
        public string rule { get; set; }
        
        [JsonProperty("multiplier")]
        public string multiplier { get; set; }
        
        [JsonProperty("bet_value")]
        public string bet_value { get; set; }
    }
}