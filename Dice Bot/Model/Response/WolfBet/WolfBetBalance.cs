﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.WolfBet
{
    public class WolfBetBalance
    {
        [JsonProperty("amount")]
        public string amount { get; set; }
        
        [JsonProperty("currency")]
        public string currency { get; set; }
        
        [JsonProperty("withdraw_fee")]
        public string withdraw_fee { get; set; }
        
        [JsonProperty("withdraw_minimum_amount")]
        public string withdraw_minimum_amount { get; set; }
        
        [JsonProperty("payment_id_required")]
        public bool payment_id_required { get; set; }
    }
}