﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.FreeBitcoin
{
    public class FreeBitcoinStat
    {
        [JsonProperty("wagered")]
        public long wagered { get; set; }

        [JsonProperty("rolls_played")]
        public long rolls_played { get; set; }

        [JsonProperty("lottery_spent")]
        public decimal lottery_spent { get; set; }

        [JsonProperty("status")]
        public string status { get; set; }

        [JsonProperty("jackpot_winnings")]
        public decimal jackpot_winnings { get; set; }

        [JsonProperty("jackpot_spent")]
        public decimal jackpot_spent { get; set; }

        [JsonProperty("reward_points")]
        public decimal reward_points { get; set; }

        [JsonProperty("balance")]
        public decimal balance { get; set; }

        [JsonProperty("total_winnings")]
        public decimal total_winnings { get; set; }

        [JsonProperty("dice_profit")]
        public decimal dice_profit { get; set; }
    }
}
