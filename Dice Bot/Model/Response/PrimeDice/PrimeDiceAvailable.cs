﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.PrimeDice
{
    public class PrimeDiceAvailable
    {
        [JsonProperty("amount")]
        public double amount { get; set; }

        [JsonProperty("currency")]
        public string currency { get; set; }

        [JsonProperty("__typename")]
        public string __typename { get; set; }
    }
}
