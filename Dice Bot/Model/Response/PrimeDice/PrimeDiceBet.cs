﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.PrimeDice
{
    public class PrimeDiceBet
    {
        [JsonProperty("id")] public string id { get; set; }

        [JsonProperty("iid")] public string iid { get; set; }

        [JsonProperty("payoutMultiplier")] public decimal payoutMultiplier { get; set; }

        [JsonProperty("amount")] public double amount { get; set; }

        [JsonProperty("payout")] public double payout { get; set; }

        [JsonProperty("createdAt")] public string createdAt { get; set; }

        [JsonProperty("currency")] public string currency { get; set; }

        [JsonProperty("state")] public PrimeDiceState state { get; set; }

        [JsonProperty("user")] public PrimeDiceUser user { get; set; }

        [JsonProperty("__typename")] public string __typename { get; set; }

        [JsonProperty("serverSeed")] public PrimeDiceSeed serverSeed { get; set; }

        [JsonProperty("clientSeed")] public PrimeDiceSeed clientSeed { get; set; }

        [JsonProperty("nonce")] public int nonce { get; set; }
    }
}