﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.PrimeDice
{
    public class PrimeDiceBalance
    {
        [JsonProperty("available")]
        public PrimeDiceAvailable available { get; set; }

        [JsonProperty("__typename")]
        public string __typename { get; set; }
    }
}
