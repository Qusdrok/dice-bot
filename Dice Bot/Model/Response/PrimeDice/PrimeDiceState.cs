﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.PrimeDice
{
    public class PrimeDiceState
    {
        [JsonProperty("result")]
        public double result { get; set; }
        
        [JsonProperty("target")]
        public double target { get; set; }
        
        [JsonProperty("condition")]
        public string condition { get; set; }
    }
}