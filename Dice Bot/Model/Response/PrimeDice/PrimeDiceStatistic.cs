﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.PrimeDice
{
    public class PrimeDiceStatistic
    {
        [JsonProperty("game")]
        public string game { get; set; }

        [JsonProperty("bets")]
        public decimal bets { get; set; }

        [JsonProperty("wins")]
        public decimal wins { get; set; }

        [JsonProperty("losses")]
        public decimal losses { get; set; }

        [JsonProperty("betAmount")]
        public double betAmount { get; set; }

        [JsonProperty("profit")]
        public double profit { get; set; }

        [JsonProperty("currency")]
        public string currency { get; set; }

        [JsonProperty("__typename")]
        public string __typename { get; set; }
    }
}
