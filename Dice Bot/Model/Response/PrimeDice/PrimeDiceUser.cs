﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Dice_Bot.Model.Response.PrimeDice
{
    public class PrimeDiceUser
    {
        [JsonProperty("id")]
        public string id { get; set; }

        [JsonProperty("name")]
        public string name { get; set; }

        [JsonProperty("roles")]
        public List<object> roles { get; set; }

        [JsonProperty("__typename")]
        public string __typename { get; set; }

        [JsonProperty("balance")]
        public PrimeDiceBalance balance { get; set; }

        [JsonProperty("balances")]
        public PrimeDiceBalance[] balances { get; set; }

        [JsonProperty("statistic")]
        public List<PrimeDiceStatistic> statistic { get; set; }

        [JsonProperty("activeServerSeed")]
        public PrimeDiceSeed activeServerSeed { get; set; }

        [JsonProperty("activeClientSeed")]
        public PrimeDiceSeed activeClientSeed { get; set; }
    }
}
