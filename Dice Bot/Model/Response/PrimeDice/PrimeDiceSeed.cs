﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.PrimeDice
{
    public class PrimeDiceSeed
    {
        [JsonProperty("seedHash")]
        public string seedHash { get; set; }

        [JsonProperty("seed")]
        public string seed { get; set; }

        [JsonProperty("nonce")]
        public int nonce { get; set; }
    }
}
