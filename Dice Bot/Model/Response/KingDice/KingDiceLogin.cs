﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.KingDice
{
    public class KingDiceLogin
    {
        [JsonProperty("balance")]
        public decimal balance { get; set; }
        
        [JsonProperty("unconfirmedbalance")]
        public decimal unconfirmedbalance { get; set; }
        
        [JsonProperty("username")]
        public string username { get; set; }
        
        [JsonProperty("address")]
        public string address { get; set; }
        
        [JsonProperty("cvalue")]
        public string cvalue { get; set; }
        
        [JsonProperty("code")]
        public string code { get; set; }
        
        [JsonProperty("maxProfit")]
        public decimal maxProfit { get; set; }
    }
}