﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.KingDice
{
    public class KingDiceBet
    {
        [JsonProperty("balance")]
        public decimal balance { get; set; }
        
        [JsonProperty("roll_number")]
        public decimal roll_number { get; set; }
        
        [JsonProperty("roll_id")]
        public decimal roll_id { get; set; }
        
        [JsonProperty("roll_under")]
        public decimal roll_under { get; set; }
        
        [JsonProperty("roll_next")]
        public KingDiceBetRoll roll_next { get; set; }
        
        [JsonProperty("roll_time")]
        public string roll_time { get; set; }
        
        [JsonProperty("roll_result")]
        public string roll_result { get; set; }
        
        [JsonProperty("roll_bet")]
        public decimal roll_bet { get; set; }
        
        [JsonProperty("roll_payout")]
        public decimal roll_payout { get; set; }
        
        [JsonProperty("roll_profit")]
        public decimal roll_profit { get; set; }
        
        [JsonProperty("roll_mode")]
        public decimal roll_mode { get; set; }
        
        [JsonProperty("probability")]
        public decimal probability { get; set; }
        
        [JsonProperty("provablef_Hash")]
        public string provablef_Hash { get; set; }
        
        [JsonProperty("provablef_Salt")]
        public string provablef_Salt { get; set; }
        
        [JsonProperty("provablef_clientRoll")]
        public decimal provablef_clientRoll { get; set; }
        
        [JsonProperty("provablef_serverRoll")]
        public decimal provablef_serverRoll { get; set; }
    }
}