﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.KingDice
{
    public class KingDiceBetRoll
    {
        [JsonProperty("hash")]
        public string hash { get; set; }
        
        [JsonProperty("id")]
        public long id { get; set; }
    }
}