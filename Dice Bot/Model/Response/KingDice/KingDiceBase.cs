﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.KingDice
{
    public class KingDiceBase
    {
        [JsonProperty("msg")]
        public string msg { get; set; }
        
        [JsonProperty("code")]
        public string code { get; set; }
        
        [JsonProperty("token")]
        public string token { get; set; }
    }
}