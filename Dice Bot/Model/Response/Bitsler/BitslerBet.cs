﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.Bitsler
{
    public class BitslerBet
    {
        [JsonProperty("success")]
        public bool success { get; set; }
        
        [JsonProperty("username")]
        public string username { get; set; }
        
        [JsonProperty("id")]
        public string id { get; set; }
        
        [JsonProperty("currency")]
        public string currency { get; set; }
        
        [JsonProperty("timestamp")]
        public long timestamp { get; set; }
        
        [JsonProperty("amount")]
        public string amount { get; set; }
        
        [JsonProperty("result")]
        public decimal result { get; set; }
        
        [JsonProperty("over")]
        public bool over { get; set; }
        
        [JsonProperty("target")]
        public decimal target { get; set; }
        
        [JsonProperty("payout")]
        public decimal payout { get; set; }
        
        [JsonProperty("chance")]
        public decimal chance { get; set; }
        
        [JsonProperty("profit")]
        public string profit { get; set; }
        
        [JsonProperty("new_balance")]
        public string new_balance { get; set; }
        
        [JsonProperty("server_seed")]
        public string server_seed { get; set; }
        
        [JsonProperty("client_seed")]
        public string client_seed { get; set; }
        
        [JsonProperty("nonce")]
        public long nonce { get; set; }
        
        [JsonProperty("notifications")]
        public List<object> notifications { get; set; }
        
        [JsonProperty("error")]
        public string error { get; set; }
    }
}