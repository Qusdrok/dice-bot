﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.Bitsler
{
    public class BitslerStats
    {
        [JsonProperty("success")]
        public string success { get; set; }
        
        [JsonProperty("error")]
        public string error { get; set; }
        
        [JsonProperty("btc_balance")]
        public decimal btc_balance { get; set; }
        
        [JsonProperty("btc_wagered")]
        public decimal btc_wagered { get; set; }
        
        [JsonProperty("btc_profit")]
        public decimal btc_profit { get; set; }
        
        [JsonProperty("bets")]
        public string bets { get; set; }
        
        [JsonProperty("ltc_balance")]
        public decimal ltc_balance { get; set; }
        
        [JsonProperty("ltc_wagered")]
        public decimal ltc_wagered { get; set; }
        
        [JsonProperty("ltc_profit")]
        public decimal ltc_profit { get; set; }
        
        [JsonProperty("doge_balance")]
        public decimal doge_balance { get; set; }
        
        [JsonProperty("doge_wagered")]
        public decimal doge_wagered { get; set; }
        
        [JsonProperty("doge_profit")]
        public decimal doge_profit { get; set; }
        
        [JsonProperty("eth_balance")]
        public decimal eth_balance { get; set; }
        
        [JsonProperty("eth_wagered")]
        public decimal eth_wagered { get; set; }
        
        [JsonProperty("eth_profit")]
        public decimal eth_profit { get; set; }
        
        [JsonProperty("burst_balance")]
        public decimal burst_balance { get; set; }
        
        [JsonProperty("burst_wagered")]
        public decimal burst_wagered { get; set; }
        
        [JsonProperty("zec_profit")]
        public decimal zec_profit { get; set; }
        
        [JsonProperty("zec_balance")]
        public decimal zec_balance { get; set; }
        
        [JsonProperty("zec_wagered")]
        public decimal zec_wagered { get; set; }
        
        [JsonProperty("bch_profit")]
        public decimal bch_profit { get; set; }
        
        [JsonProperty("bch_balance")]
        public decimal bch_balance { get; set; }
        
        [JsonProperty("bch_wagered")]
        public decimal bch_wagered { get; set; }
        
        [JsonProperty("dash_profit")]
        public decimal dash_profit { get; set; }
        
        [JsonProperty("dash_balance")]
        public decimal dash_balance { get; set; }
        
        [JsonProperty("dash_wagered")]
        public decimal dash_wagered { get; set; }
        
        [JsonProperty("burst_profit")]
        public decimal burst_profit { get; set; }
        
        [JsonProperty("etc_balance")]
        public decimal etc_balance { get; set; }
        
        [JsonProperty("etc_wagered")]
        public decimal etc_wagered { get; set; }
        
        [JsonProperty("etc_profit")]
        public decimal etc_profit { get; set; }
        
        [JsonProperty("xmr_balance")]
        public decimal xmr_balance { get; set; }
        
        [JsonProperty("xmr_wagered")]
        public decimal xmr_wagered { get; set; }
        
        [JsonProperty("xmr_profit")]
        public decimal xmr_profit { get; set; }
        
        [JsonProperty("neo_balance")]
        public decimal neo_balance { get; set; }
        
        [JsonProperty("neo_wagered")]
        public decimal neo_wagered { get; set; }
        
        [JsonProperty("neo_profit")]
        public decimal neo_profit { get; set; }

        [JsonProperty("strat_balance")]
        public decimal strat_balance { get; set; }
        
        [JsonProperty("strat_wagered")]
        public decimal strat_wagered { get; set; }
        
        [JsonProperty("strat_profit")]
        public decimal strat_profit { get; set; }

        [JsonProperty("kmd_balance")]
        public decimal kmd_balance { get; set; }
        
        [JsonProperty("kmd_wagered")]
        public decimal kmd_wagered { get; set; }
        
        [JsonProperty("kmd_profit")]
        public decimal kmd_profit { get; set; }

        [JsonProperty("xrp_balance")]
        public decimal xrp_balance { get; set; }
        
        [JsonProperty("xrp_wagered")]
        public decimal xrp_wagered { get; set; }
        
        [JsonProperty("xrp_profit")]
        public decimal xrp_profit { get; set; }

        [JsonProperty("btg_balance")]
        public decimal btg_balance { get; set; }
        
        [JsonProperty("btg_wagered")]
        public decimal btg_wagered { get; set; }
        
        [JsonProperty("btg_profit")]
        public decimal btg_profit { get; set; }
        
        [JsonProperty("lsk_balance")]
        public decimal lsk_balance { get; set; }
        
        [JsonProperty("lsk_wagered")]
        public decimal lsk_wagered { get; set; }
        
        [JsonProperty("lsk_profit")]
        public decimal lsk_profit { get; set; }
        
        [JsonProperty("dgb_balance")]
        public decimal dgb_balance { get; set; }

        [JsonProperty("dgb_wagered")]
        public decimal dgb_wagered { get; set; }
        
        [JsonProperty("dgb_profit")]
        public decimal dgb_profit { get; set; }

        [JsonProperty("qtum_balance")]
        public decimal qtum_balance { get; set; }
        
        [JsonProperty("qtum_wagered")]
        public decimal qtum_wagered { get; set; }
        
        [JsonProperty("qtum_profit")]
        public decimal qtum_profit { get; set; }
        
        [JsonProperty("waves_balance")]
        public decimal waves_balance { get; set; }
        
        [JsonProperty("waves_wagered")]
        public decimal waves_wagered { get; set; }
        
        [JsonProperty("waves_profit")]
        public decimal waves_profit { get; set; }
        
        [JsonProperty("btslr_balance")]
        public decimal btslr_balance { get; set; }
        
        [JsonProperty("btslr_wagered")]
        public decimal btslr_wagered { get; set; }
        
        [JsonProperty("btslr_profit")]
        public decimal btslr_profit { get; set; }
        
        [JsonProperty("bsv_balance")]
        public decimal bsv_balance { get; set; }
        
        [JsonProperty("bsv_wagered")]
        public decimal bsv_wagered { get; set; }
        
        [JsonProperty("bsv_profit")]
        public decimal bsv_profit { get; set; }
        
        [JsonProperty("xlm_balance")]
        public decimal xlm_balance { get; set; }
        
        [JsonProperty("xlm_wagered")]
        public decimal xlm_wagered { get; set; }
        
        [JsonProperty("xlm_profit")]
        public decimal xlm_profit { get; set; }
        
        [JsonProperty("usdt_balance")]
        public decimal usdt_balance { get; set; }
        
        [JsonProperty("usdt_wagered")]
        public decimal usdt_wagered { get; set; }
        
        [JsonProperty("usdt_profit")]
        public decimal usdt_profit { get; set; }
        
        [JsonProperty("wins")]
        public string wins { get; set; }
        
        [JsonProperty("losses")]
        public string losses { get; set; }
        
        [JsonProperty("trx_balance")]
        public decimal trx_balance { get; internal set; }
        
        [JsonProperty("trx_profit")]
        public decimal trx_profit { get; internal set; }
        
        [JsonProperty("trx_wagered")]
        public decimal trx_wagered { get; internal set; }
        
        [JsonProperty("eos_balance")]
        public decimal eos_balance { get; internal set; }
        
        [JsonProperty("eos_profit")]
        public decimal eos_profit { get; internal set; }
        
        [JsonProperty("eos_wagered")]
        public decimal eos_wagered { get; internal set; }
    }
}