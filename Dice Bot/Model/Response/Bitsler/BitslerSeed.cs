﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.Bitsler
{
    public class BitslerSeed
    {
        [JsonProperty("previous_hash")]
        public string previous_hash { get; set; }
        
        [JsonProperty("previous_seed")]
        public string previous_seed { get; set; }
        
        [JsonProperty("previous_client")]
        public string previous_client { get; set; }
        
        [JsonProperty("previous_total")]
        public string previous_total { get; set; }
        
        [JsonProperty("current_client")]
        public string current_client { get; set; }
        
        [JsonProperty("current_hash")]
        public string current_hash { get; set; }
        
        [JsonProperty("next_hash")]
        public string next_hash { get; set; }
        
        [JsonProperty("success")]
        public bool success { get; set; }
    }
}