﻿using Newtonsoft.Json;

namespace Dice_Bot.Model.Response.Bitsler
{
    public class BitslerLogin
    {
        [JsonProperty("success")]
        public string success { get; set; }
        
        [JsonProperty("error")]
        public string error { get; set; }
        
        [JsonProperty("access_token")]
        public string access_token { get; set; }
    }
}