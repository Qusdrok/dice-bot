﻿using Dice_Bot.Event;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Dice_Bot.Model
{
    public class DiceBotInfo
    {
        public DiceBotForm form;
        public DataGridView dgv;
        public Chart chart;
        public Stop stop;
        public int threadIndex;
        public int browserIndex;
        public int divideChance;
        public string proxy;
        public string programmer;
        public bool isProgrammer;

        public DiceBotInfo(DiceBotForm form, DataGridView dgv, Chart chart,
            int threadIndex, int browserIndex, int divideChance, string programmer,
            string proxy, Stop stop = Stop.None, bool isProgrammer = false)
        {
            this.form = form;
            this.dgv = dgv;
            this.chart = chart;
            this.stop = stop;
            this.threadIndex = threadIndex;
            this.browserIndex = browserIndex;
            this.proxy = proxy;
            this.programmer = programmer;
            this.isProgrammer = isProgrammer;
        }

        public void SetStop(Stop stop)
        {
            this.stop = stop;
        }
    }
}
