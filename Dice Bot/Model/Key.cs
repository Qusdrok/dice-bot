﻿using System.Collections.Generic;

public class Data
{
    public string id { get; set; }
    public string key_code { get; set; }
    public string exp_day { get; set; }
    public string timestamp { get; set; }
    public string device_id { get; set; }
    public string note { get; set; }
    public string created_time { get; set; }
    public string thread { get; set; }
}

public class Key
{
    public int error { get; set; }
    public int code { get; set; }
    public string msg { get; set; }
    public List<Data> data { get; set; }
}
