﻿using System.Windows.Forms;

namespace Dice_Bot.Model
{
    public class GBGame
    {
        public TextBox betMax;
        public TextBox betMin;
        public TextBox cashoutMax;
        public TextBox cashoutMin;
        public ComboBox coin;
        public ComboBox game;

        public GBGame(TextBox betMax, TextBox betMin, TextBox cashoutMax,
            TextBox cashoutMin, ComboBox coin, ComboBox game)
        {
            this.betMax = betMax;
            this.betMin = betMin;
            this.cashoutMax = cashoutMax;
            this.cashoutMin = cashoutMin;
            this.coin = coin;
            this.game = game;
        }
    }
}
