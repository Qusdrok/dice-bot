﻿using Dice_Bot.Event;

namespace Dice_Bot.Model
{
    public class CoinInfo
    {
        public Coin coin;
        public decimal minimum;
        public bool isSupportSelenium;
        public string path;

        public CoinInfo(Coin coin, decimal minimum, bool isSupportSelenium = false, string path = "")
        {
            this.coin = coin;
            this.minimum = minimum;
            this.isSupportSelenium = isSupportSelenium;
            this.path = path;
        }
    }
}
