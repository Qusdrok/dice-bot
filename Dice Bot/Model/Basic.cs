﻿using Dice_Bot.Event;
using Utility;

namespace Dice_Bot.Model
{
    public class Basic
    {
        public string account;
        public string password;
        public decimal betMax;
        public decimal betMin;
        public decimal cashoutMax;
        public decimal cashoutMin;
        public bool result;
        public Coin coin;
        public Game game;

        public Basic(string account, string password, decimal betMax,
            decimal betMin, decimal cashoutMax, decimal cashoutMin,
            Coin coin, Game game)
        {
            this.account = account;
            this.password = password;
            this.betMax = betMax;
            this.betMin = betMin;
            this.cashoutMax = cashoutMax;
            this.cashoutMin = cashoutMin;
            this.result = false;
            this.coin = coin;
            this.game = game;
        }

        public decimal GetBet()
        {
            return UtilsValue.RandomDecimal(betMin, betMax);
        }

        public decimal GetCashout()
        {
            return UtilsValue.RandomDecimal(cashoutMin, cashoutMax);
        }
    }
}
