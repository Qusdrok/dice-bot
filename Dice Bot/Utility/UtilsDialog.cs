﻿using System.Windows.Forms;

namespace Utility
{
    public static class UtilsDialog
    {
        public static void DialogWarning(string message)
        {
            MessageBox.Show(message, "Cảnh báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static void DialogError(string message)
        {
            MessageBox.Show(message, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void DialogInformation(string message, MessageBoxButtons buttons = MessageBoxButtons.OK)
        {
            MessageBox.Show(message, "Thông báo", buttons, MessageBoxIcon.Information);
        }
    }
}
