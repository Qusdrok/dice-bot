﻿using System;
using System.Threading.Tasks;

namespace Utility
{
    public class UtilsAsync
    {
        public static void RunAsyncVoid(Action newFunction)
        {
            Run(RunFunction(newFunction));
        }

        private static async void Run(Task newTask)
        {
            await newTask;
        }

        private static Task RunFunction(Action newFunction)
        {
            return Task.Factory.StartNew(newFunction);
        }
    }
}
