﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Utility
{
    public class UtilsEncode
    {
        private static string mainHash = "10-20 22: 21: 10.752: W/fb4a (:): BlueServiceQueue (5872): com.facebook.http." +
            "protatio.ApiException: Khóa băm VQ3XhZb5_tBH9oGe2WW32DDdNS0 không khớp với bất kỳ băm khóa được lưu trữ nào.";

        public static string MD5(string text)
        {
            var md5 = System.Security.Cryptography.MD5.Create();
            var builder = new StringBuilder();

            var hash = md5.ComputeHash(Encoding.UTF8.GetBytes(text + mainHash));
            foreach (var b in hash)
            {
                builder.Append(b.ToString("X2"));
            }

            return builder.ToString();
        }

        public static string SHA1(string text)
        {
            var sha1 = new SHA1Managed();
            var builder = new StringBuilder();

            var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(text + mainHash));
            foreach (var b in hash)
            {
                builder.Append(b.ToString("x2"));
            }

            return builder.ToString();
        }

        public static string SHA256(string text)
        {
            var sha256 = new SHA256Managed();
            var builder = new StringBuilder();

            var hash = sha256.ComputeHash(Encoding.UTF8.GetBytes(text + mainHash));
            foreach (var b in hash)
            {
                builder.Append(b.ToString("x2"));
            }

            return builder.ToString();
        }

        public static string SHA384(string text)
        {
            var sha384 = new SHA384Managed();
            var builder = new StringBuilder();

            var hash = sha384.ComputeHash(Encoding.UTF8.GetBytes(text + mainHash));
            foreach (var b in hash)
            {
                builder.Append(b.ToString("x2"));
            }

            return builder.ToString();
        }

        public static string SHA512(string text)
        {
            var sha512 = new SHA512Managed();
            var builder = new StringBuilder();

            var hash = sha512.ComputeHash(Encoding.UTF8.GetBytes(text + mainHash));
            foreach (var b in hash)
            {
                builder.Append(b.ToString("x2"));
            }

            return builder.ToString();
        }

        public static string RandomHash(string text)
        {
            switch(new Random().Next(0, 4))
            {
                case 0: return MD5(text + mainHash);
                case 1: return SHA1(text + mainHash);
                case 2: return SHA256(text + mainHash);
                case 3: return SHA384(text + mainHash);
                case 4: return SHA512(text + mainHash);
                default: return null;
            }
        }
    }
}
