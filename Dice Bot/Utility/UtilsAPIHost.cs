﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Utility
{
    public static class UtilsAPIHost
    {
        private static HttpClient client = new HttpClient();

        private static string urlAPI = "https://tonnypham.000webhostapp.com/";
        private static string urlAPIGetAllData = "get-all-data.php?auth=qusdrokAPI";
        private static string urlAPIGetKeyData = "get-key-data.php?auth=qusdrokAPI";
        private static string urlAPICreateKeyData = "create-key-data.php?auth=qusdrokAPI";
        private static string urlAPIDeleteKeyData = "delete-key-data.php?auth=qusdrokAPI";
        private static string urlAPIUpdateKeyData = "update-key-data.php?auth=qusdrokAPI";

        public static async Task<Key> APIGetAllData()
        {
            var response = await client.GetStringAsync(urlAPI + urlAPIGetAllData);
            var user = JsonConvert.DeserializeObject<Key>(response);
            Console.WriteLine(response);
            return user;
        }

        public static async Task<Key> APIGetKeyData(string keyCode, string deviceID)
        {
            var url = urlAPI + urlAPIGetKeyData + "&key_code=" + keyCode + "&device_id=" + deviceID;
            var response = await client.GetStringAsync(url);

            var user = JsonConvert.DeserializeObject<Key>(response);
            return user;
        }

        public static async Task<Key> APICreateKey(string keyCode, string expDay, string thread)
        {
            var url = urlAPI + urlAPICreateKeyData + "&key_code=" + keyCode + "&exp_day=" + expDay + "&thread=" + thread;
            var response = await client.GetStringAsync(url);

            var user = JsonConvert.DeserializeObject<Key>(response);
            return user;
        }

        public static async Task<Key> APIDeleteKey(string keyCode)
        {
            var url = urlAPI + urlAPIDeleteKeyData + "&key_code=" + keyCode;
            var response = await client.GetStringAsync(url);

            var user = JsonConvert.DeserializeObject<Key>(response);
            return user;
        }

        public static async Task<Key> APIUpdateKey(string keyCode, string deviceID, string expDay, string note, string thread)
        {
            var url = urlAPI + urlAPIUpdateKeyData + "&key_code=" + keyCode + "&device_id="
                      + deviceID + "&exp_day=" + expDay + "&note=" + note + "&thread=" + thread;
            var response = await client.GetStringAsync(url);

            var user = JsonConvert.DeserializeObject<Key>(response);
            return user;
        }
    }
}
