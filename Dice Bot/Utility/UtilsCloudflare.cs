﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;

namespace Utility
{
    public static class UtilsCloudflare
    {
        private static string _response = "cf_chl_jschl_tk__=";
        private static string _rs = "name=\"r\" value=\"";

        public static bool CloudflareBypass(HttpClient client, HttpClientHandler clientHandler,
            string response, string uri, int cfLevel)
        {
            //var JSC = new JavascriptContext();
            var jschl_tk = response.Substring(response.IndexOf(_response) + _response.Length);
            jschl_tk = jschl_tk.Substring(0, jschl_tk.IndexOf("\""));

            var rs = response.Substring(response.IndexOf(_rs) + _rs.Length);
            rs = rs.Substring(0, rs.IndexOf("\""));

            var jschl_vc = response.Substring(response.IndexOf("jschl_vc"));
            jschl_vc = jschl_vc.Substring(jschl_vc.IndexOf("value=\"") + "value=\"".Length);
            jschl_vc = jschl_vc.Substring(0, jschl_vc.IndexOf("\""));

            var pass = response.Substring(response.IndexOf("\"pass\""));
            pass = pass.Substring(pass.IndexOf("value=\"") + "value=\"".Length);
            pass = pass.Substring(0, pass.IndexOf("\""));
            
            //do the CF bypass thing and get the headers
            var script = response.Substring(response.IndexOf("var s,t,o,p,b,r,e,a,k,i,n,g,f,"));
            var _script = script.Substring(0, script.IndexOf(";") + 1);

            script = script.Substring("var s,t,o,p,b,r,e,a,k,i,n,g,f, ".Length);
            var name = script.Substring(0, script.IndexOf("="));
            var _name = script.Substring(script.IndexOf("\"") + 1);
            var __script = script.Substring(script.IndexOf(";") + 1);

            __script = __script.Substring(0, __script.IndexOf("t = document"));
            _script += __script;

            var kkkk = script.Substring(script.IndexOf("k = '") + "k = '".Length);
            kkkk = kkkk.Substring(0, kkkk.IndexOf("';"));

            var kvalue = response.Substring(response.IndexOf($"id=\"{kkkk}\">") + $"id=\"{kkkk}\">".Length);
            kvalue = "var innr = " + kvalue.Substring(0, kvalue.IndexOf("<")) + ";";

            _script += kvalue;
            name += "." + _name.Substring(0, _name.IndexOf("\""));

            _script += script.Substring(script.IndexOf(name));
            _script = _script.Substring(0, _script.IndexOf("f.submit()"));
            _script = _script.Replace("t.length", uri.Length + "");
            _script = _script.Replace("a.value", "var answer");

            if (_script.Contains("f.action += location.hash;") || _script.Contains("f.action+=location.hash;"))
                _script = _script.Replace("f.action += location.hash;", "").Replace("f.action+=location.hash;", "");

            _script = _script.Replace("(function(p){return eval((true+\"\")[0]+\".ch\"+(false+\"\")[1]+(true+\"\")[1]+Function(\"return escape\")()((\"\")[\"italics\"]())[2]+\"o\"+(undefined+\"\")[2]+(true+\"\")[3]+\"A\"+(true+\"\")[0]+\"(\"+p+\")\")}", $"(function(p){{ return eval(\"{uri}\".charCodeAt(p)) }}");
            _script = _script.Replace("function(p){var p = eval(eval(e(\"ZG9jdW1l\")+(undefined+\"\")[1]+(true+\"\")[0]+(+(+!+[]+[+!+[]]+(!![]+[])[!+[]+!+[]+!+[]]+[!+[]+!+[]]+[+[]])+[])[+!+[]]+g(103)+(true+\"\")[3]+(true+\"\")[0]+\"Element\"+g(66)+(NaN+[Infinity])[10]+\"Id(\"+g(107)+\").\"+e(\"aW5uZXJIVE1M\"))); return +(p)}();",
                "function(p){var p = eval(eval(innr));return +(p)}();");

            //JSC.Run(Script1);
            //string answer = JSC.GetParameter("answer").ToString();

            try
            {
                var pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("r", rs),
                    new KeyValuePair<string, string>("jschl_vc", jschl_vc),
                    new KeyValuePair<string, string>("pass", pass),
                    //{ new KeyValuePair<string, string>("jschl_answer", answer) },
                };

                var Content = new FormUrlEncodedContent(pairs);
                var url = $"https://{uri}/?__cf_chl_jschl_tk__={jschl_tk}";

                Content.Headers.Add("Origin", "https://wolf.bet");
                //Content.Headers.Add("Referer", "https://wolf.bet/");
                Content.Headers.Add("sec-fetch-mode", "navigate");
                Content.Headers.Add("sec-fetch-site", "same-origin");
                //Content.Headers.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
                Content.Headers.Remove("content-type");
                Content.Headers.Add("content-type", "application/x-www-form-urlencoded");

                var responseMessage = client.PostAsync(url, Content).Result;
                var found = clientHandler.CookieContainer.GetCookies(new Uri("https://" + uri)).Cast<Cookie>().Any(c => c.Name == "cf_clearance");

                if (!found && cfLevel++ < 5)
                    return CloudflareBypass(client, clientHandler, responseMessage.Content.ReadAsStringAsync().Result, uri, cfLevel);
                return found;
            }
            catch
            {
                return false;
            }
        }
    }
}
