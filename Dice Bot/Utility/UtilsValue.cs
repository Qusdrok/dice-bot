﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Keys = OpenQA.Selenium.Keys;

namespace Utility
{
    public enum KeyControl { None, A }
    public enum ElementType { Exists, Clickable }
    public enum ByElement { XPath, Class, ID, CssSelector }

    public static class UtilsValue
    {
        public static void SendClick(IWebDriver driver, string path, bool isScrollToElement = false)
        {
            var element = WaitUntilElement(driver, path);
            if (isScrollToElement) ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", element);
            element.Click();
        }

        public static void SendText(IWebDriver driver, string path, string newContent, KeyControl control = KeyControl.None)
        {
            var currentElement = WaitUntilElement(driver, path);
            switch (control)
            {
                case KeyControl.A:
                    currentElement.SendKeys(Keys.Control + "a");
                    break;
                case KeyControl.None:
                    break;
            }

            currentElement.SendKeys(newContent);
        }

        public static decimal ConvertDecimal(float value, NumberStyles styles = NumberStyles.Float)
        {
            return decimal.Parse(value.ToString(), styles);
        }

        public static string TextChanged(TextBox textBox, decimal minimum, decimal maximum = decimal.MaxValue)
        {
            try
            {
                if (string.IsNullOrEmpty(textBox.Text)) return minimum.ToString();
                if (decimal.TryParse(textBox.Text, out var value) && (value < minimum || value > maximum)) return minimum.ToString();
                return decimal.Parse(textBox.Text).ToString();
            }
            catch
            {
                return minimum.ToString();
            }
        }

        public static bool KeyPressNumber(KeyPressEventArgs e, bool hasFloat = false, bool hasChanged = true)
        {
            return !hasChanged ||
                   !(!hasFloat || e.KeyChar == '.' || char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar));
        }

        public static float RandomFloat(float min, float max)
        {
            if (min > max)
            {
                var temp = min;
                min = max;
                max = temp;
            }

            return (float)new Random().NextDouble() * (max - min) + min;
        }

        public static decimal RandomDecimal(decimal min, decimal max)
        {
            if (min <= max) return (decimal)new Random().NextDouble() * (max - min) + min;
            var temp = min;

            min = max;
            max = temp;
            return (decimal)new Random().NextDouble() * (max - min) + min;
        }

        public static long Timestamp()
        {
            var temp = DateTime.UtcNow - DateTime.Parse("1970/01/01 00:00:00", CultureInfo.InvariantCulture);
            return (long)temp.TotalMilliseconds;
        }

        public static string ExtractNumberFromString(string str)
        {
            return Regex.Match(str, "[+-]?\\d*\\.?\\d*").Value;
        }

        public static string ExtractNumberFromElement(IWebDriver driver, string path)
        {
            return ExtractNumberFromString(WaitUntilElement(driver, path).GetAttribute("innerHTML"));
        }

        public static IWebElement WaitUntilElement(IWebDriver driver, string path,
            ByElement byType = ByElement.XPath, ElementType elementType = ElementType.Exists,
            int timeout = 60)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromMinutes(timeout));
            By element = null;

            try
            {
                switch (byType)
                {
                    case ByElement.Class:
                        element = By.ClassName(path);
                        break;
                    case ByElement.ID:
                        element = By.Id(path);
                        break;
                    case ByElement.CssSelector:
                        element = By.CssSelector(path);
                        break;
                    default:
                        element = By.XPath(path);
                        break;
                }

                switch (elementType)
                {
                    case ElementType.Clickable:
                        return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element));
                    default:
                        return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(element));
                }
            }
            catch
            {
                Console.WriteLine($"Element {element} chưa tìm thấy");
                return null;
            }
        }
    }
}