﻿using Utility;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Dice_Bot
{
    public partial class CreateKeyForm : Form
    {
        private List<string> encodes = new List<string> { "MD5", "SHA1", "SHA256", "SHA512", "SHA384" };
        private List<string> exps = new List<string> { "3 ngày", "5 ngày", "7 ngày", "14 ngày", "30 ngày", "60 ngày", "Vĩnh viễn" };

        public CreateKeyForm()
        {
            InitializeComponent();
            hsd_edit_cb.Items.AddRange(exps.ToArray());
            hsd_generate_cb.Items.AddRange(exps.ToArray());

            encode_basic_cb.Items.AddRange(encodes.ToArray());
            encode_generate_cb.Items.AddRange(encodes.ToArray());

            hsd_edit_cb.SelectedIndex = 0;
            hsd_generate_cb.SelectedIndex = 0;

            encode_basic_cb.SelectedIndex = 0;
            encode_generate_cb.SelectedIndex = 0;

            DisplayAllKey();
        }

        private async void DisplayAllKey()
        {
            var user = await UtilsAPIHost.APIGetAllData();
            var binding = new BindingSource {DataSource = user.data};
            dataGridView.DataSource = binding;
        }

        private void ClearData()
        {
            keyword_generate_tb.Clear();
            result_generate_tb.Clear();
            note_generate_tb.Clear();
            note_edit_tb.Clear();

            key_tb.Clear();
            id_tb.Clear();

            hsd_generate_cb.SelectedIndex = 0;
            hsd_edit_cb.SelectedIndex = 0;
            encode_generate_cb.SelectedIndex = 0;
            Clipboard.Clear();
        }

        private object CheckExpDay(ListControl expCB, Control expTB)
        {
            var index = expCB.SelectedIndex;
            var text = expTB.Text;

            switch (index)
            {
                case 0 when string.IsNullOrEmpty(text):
                    UtilsDialog.DialogWarning("Bạn cần chọn hoặc thêm số ngày sử dụng key");
                    return null;
                case 0 when !string.IsNullOrEmpty(text):
                    return text;
                default:
                    switch (index)
                    {
                        case 1: return 3;
                        case 2: return 5;
                        case 3: return 7;
                        case 4: return 14;
                        case 5: return 30;
                        case 6: return 60;
                        case 7: return 99999;
                    }

                    return null;
            }
        }

        #region Function Form
        private void create_btn_click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(keyword_basic_tb.Text))
            {
                UtilsDialog.DialogWarning("Bạn cần nhập giá trị keyword để mã hóa");
                return;
            }

            var rs = encode_basic_cb.Items[encode_basic_cb.SelectedIndex];
            switch (rs)
            {
                case "MD5":
                    result_basic_tb.Text = UtilsEncode.RandomHash(UtilsEncode.MD5(keyword_basic_tb.Text));
                    break;
                case "SHA1":
                    result_basic_tb.Text = UtilsEncode.RandomHash(UtilsEncode.SHA1(keyword_basic_tb.Text));
                    break;
                case "SHA256":
                    result_basic_tb.Text = UtilsEncode.RandomHash(UtilsEncode.SHA256(keyword_basic_tb.Text));
                    break;
                case "SHA512":
                    result_basic_tb.Text = UtilsEncode.RandomHash(UtilsEncode.SHA512(keyword_basic_tb.Text));
                    break;
                default:
                    result_basic_tb.Text = UtilsEncode.RandomHash(UtilsEncode.SHA384(keyword_basic_tb.Text));
                    break;
            }
        }

        private void generate_click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(keyword_generate_tb.Text))
            {
                UtilsDialog.DialogWarning("Bạn cần nhập giá trị keyword để mã hóa");
                return;
            }

            if (string.IsNullOrEmpty(result_basic_tb.Text))
            {
                UtilsDialog.DialogWarning("Bạn phải có result của basic");
                return;
            }

            var rs = encode_generate_cb.Items[encode_generate_cb.SelectedIndex];
            var data = keyword_generate_tb.Text + result_basic_tb.Text;

            switch (rs)
            {
                case "MD5":
                    result_generate_tb.Text = UtilsEncode.RandomHash(UtilsEncode.MD5(data));
                    break;
                case "SHA1":
                    result_generate_tb.Text = UtilsEncode.RandomHash(UtilsEncode.SHA1(data));
                    break;
                case "SHA256":
                    result_generate_tb.Text = UtilsEncode.RandomHash(UtilsEncode.SHA256(data));
                    break;
                case "SHA512":
                    result_generate_tb.Text = UtilsEncode.RandomHash(UtilsEncode.SHA512(data));
                    break;
                default:
                    result_generate_tb.Text = UtilsEncode.RandomHash(UtilsEncode.SHA384(data));
                    break;
            }
        }

        private void copy_basic_click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(result_basic_tb.Text))
            {
                UtilsDialog.DialogWarning("Không có dữ liệu");
                return;
            }

            Clipboard.SetText(result_basic_tb.Text);
            UtilsDialog.DialogInformation("Copy thành công");
        }

        private void copy_generate_click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(result_generate_tb.Text))
            {
                UtilsDialog.DialogWarning("Không có dữ liệu");
                return;
            }

            Clipboard.SetText(result_generate_tb.Text);
            UtilsDialog.DialogInformation("Copy thành công");
        }

        private async void add_database_click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(result_generate_tb.Text))
            {
                UtilsDialog.DialogWarning("Bạn chưa có dữ liệu để thêm vào");
                return;
            }

            try
            {
                var rs = CheckExpDay(hsd_generate_cb, hsd_generate_tb);
                if (rs == null) return;
                
                var user = await UtilsAPIHost.APICreateKey(this.result_generate_tb.Text, rs.ToString(), this.thread_generate_tb.Text);
                UtilsDialog.DialogInformation(user.msg);

                DisplayAllKey();
                ClearData();
            }
            catch
            {
                UtilsDialog.DialogError("Server lỗi");
            }
        }

        private async void edit_click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(key_tb.Text))
            {
                UtilsDialog.DialogWarning("Bạn chưa chọn dữ liệu");
                return;
            }

            try
            {
                var rs = CheckExpDay(hsd_edit_cb, hsd_edit_tb);
                if (rs == null) return;
                
                var user = await UtilsAPIHost.APIUpdateKey(key_tb.Text, id_tb.Text ?? "1",
                    rs.ToString(), note_edit_tb.Text, this.thread_edit_tb.Text);
                UtilsDialog.DialogInformation(user.msg);

                DisplayAllKey();
                ClearData();
            }
            catch
            {
                UtilsDialog.DialogError("LỖI chỉnh sửa dữ liệu thất bại");
            }
        }

        private async void delete_click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(key_tb.Text))
            {
                UtilsDialog.DialogWarning("Bạn chưa chọn dữ liệu");
                return;
            }

            var rs = MessageBox.Show("Bạn thật sự muốn xóa nó không ?",
                "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            switch (rs)
            {
                case DialogResult.Yes:
                    try
                    {
                        var user = await UtilsAPIHost.APIDeleteKey(key_tb.Text);
                        UtilsDialog.DialogInformation(user.msg);

                        DisplayAllKey();
                        ClearData();
                    }
                    catch
                    {
                        UtilsDialog.DialogError("LỖI xóa dữ liệu thất bại :((");
                    }

                    break;
            }
        }

        private void datagridview_cell_click(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dataGridView.CurrentRow != null) dataGridView.CurrentRow.Selected = true;
                var rows = dataGridView.Rows[e.RowIndex];

                key_tb.Text = rows.Cells["key_code"].Value.ToString();
                id_tb.Text = rows.Cells["device_id"].Value.ToString();
                note_edit_tb.Text = rows.Cells["note"].Value.ToString();
                created_time_tb.Text = rows.Cells["created_time"].Value.ToString();
                thread_edit_tb.Text = rows.Cells["thread"].Value.ToString();

                var expDay = rows.Cells["exp_day"].Value.ToString();
                var timestamp = DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;

                expired_rb.Checked = timestamp > int.Parse(rows.Cells["timestamp"].Value.ToString());
                hsd_edit_tb.Text = expDay;

                switch (int.Parse(expDay))
                {
                    case 3: hsd_edit_cb.SelectedIndex = 1;break;
                    case 5: hsd_edit_cb.SelectedIndex = 2; break;
                    case 7: hsd_edit_cb.SelectedIndex = 3; break;
                    case 14: hsd_edit_cb.SelectedIndex = 4; break;
                    case 30: hsd_edit_cb.SelectedIndex = 5; break;
                    case 60: hsd_edit_cb.SelectedIndex = 6; break;
                    case 99999: hsd_edit_cb.SelectedIndex = 7; break;
                }
            }
            catch
            {
                UtilsDialog.DialogWarning("Dữ liệu bạn chọn không đúng");
            }
        }

        private void thread_edit_tb_key_press(object sender, KeyPressEventArgs e)
        {
            e.Handled = UtilsValue.KeyPressNumber(e);
        }

        private void hsd_generate_tb_key_press(object sender, KeyPressEventArgs e)
        {
            e.Handled = UtilsValue.KeyPressNumber(e);
        }

        private void hsd_edit_tb_key_press(object sender, KeyPressEventArgs e)
        {
            e.Handled = UtilsValue.KeyPressNumber(e);
        }

        private void thread_generate_tb_text_changed(object sender, EventArgs e)
        {
            this.thread_generate_tb.Text = UtilsValue.TextChanged(this.thread_generate_tb, 1m, 100m);
        }

        private void thread_edit_tb_text_changed(object sender, EventArgs e)
        {
            this.thread_edit_tb.Text = UtilsValue.TextChanged(this.thread_edit_tb, 1m, 100m);
        }

        private void thread_generate_tb_key_press(object sender, KeyPressEventArgs e)
        {
            e.Handled = UtilsValue.KeyPressNumber(e);
        }
        #endregion
    }
}
