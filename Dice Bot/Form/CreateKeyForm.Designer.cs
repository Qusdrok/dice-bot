﻿namespace Dice_Bot
{
    partial class CreateKeyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.encode_basic_cb = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.result_basic_tb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.keyword_basic_tb = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.thread_generate_tb = new System.Windows.Forms.TextBox();
            this.hsd_generate_tb = new System.Windows.Forms.TextBox();
            this.label = new System.Windows.Forms.Label();
            this.note_generate_tb = new System.Windows.Forms.TextBox();
            this.hsd_generate_cb = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.add_btn = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.encode_generate_cb = new System.Windows.Forms.ComboBox();
            this.button4 = new System.Windows.Forms.Button();
            this.keyword_generate_tb = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.result_generate_tb = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.thread_edit_tb = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.created_time_tb = new System.Windows.Forms.TextBox();
            this.expired_rb = new System.Windows.Forms.RadioButton();
            this.hsd_edit_tb = new System.Windows.Forms.TextBox();
            this.hsd_edit_cb = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.note_edit_tb = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.id_tb = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.delete_btn = new System.Windows.Forms.Button();
            this.edit_btn = new System.Windows.Forms.Button();
            this.key_tb = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.dataGridView)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(314, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 15);
            this.label1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.encode_basic_cb);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.result_basic_tb);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.keyword_basic_tb);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(14, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(318, 155);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Basic";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 15);
            this.label6.TabIndex = 7;
            this.label6.Text = "Encode:";
            // 
            // encode_basic_cb
            // 
            this.encode_basic_cb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.encode_basic_cb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.encode_basic_cb.FormattingEnabled = true;
            this.encode_basic_cb.Location = new System.Drawing.Point(73, 45);
            this.encode_basic_cb.Name = "encode_basic_cb";
            this.encode_basic_cb.Size = new System.Drawing.Size(237, 23);
            this.encode_basic_cb.TabIndex = 6;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(73, 121);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(87, 27);
            this.button2.TabIndex = 5;
            this.button2.Text = "Create";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.create_btn_click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(224, 121);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(87, 27);
            this.button1.TabIndex = 4;
            this.button1.Text = "Copy";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.copy_basic_click);
            // 
            // result_basic_tb
            // 
            this.result_basic_tb.Location = new System.Drawing.Point(73, 91);
            this.result_basic_tb.Name = "result_basic_tb";
            this.result_basic_tb.ReadOnly = true;
            this.result_basic_tb.Size = new System.Drawing.Size(237, 23);
            this.result_basic_tb.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(164, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Result:";
            // 
            // keyword_basic_tb
            // 
            this.keyword_basic_tb.Location = new System.Drawing.Point(73, 15);
            this.keyword_basic_tb.Name = "keyword_basic_tb";
            this.keyword_basic_tb.Size = new System.Drawing.Size(237, 23);
            this.keyword_basic_tb.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "Keyword:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.thread_generate_tb);
            this.groupBox2.Controls.Add(this.hsd_generate_tb);
            this.groupBox2.Controls.Add(this.label);
            this.groupBox2.Controls.Add(this.note_generate_tb);
            this.groupBox2.Controls.Add(this.hsd_generate_cb);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.add_btn);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.encode_generate_cb);
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.keyword_generate_tb);
            this.groupBox2.Controls.Add(this.button5);
            this.groupBox2.Controls.Add(this.result_generate_tb);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(14, 175);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(318, 286);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Generate";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(23, 85);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(36, 15);
            this.label15.TabIndex = 21;
            this.label15.Text = "Note:";
            // 
            // thread_generate_tb
            // 
            this.thread_generate_tb.Location = new System.Drawing.Point(73, 52);
            this.thread_generate_tb.Name = "thread_generate_tb";
            this.thread_generate_tb.Size = new System.Drawing.Size(237, 23);
            this.thread_generate_tb.TabIndex = 20;
            this.thread_generate_tb.Text = "1";
            this.thread_generate_tb.TextChanged += new System.EventHandler(this.thread_generate_tb_text_changed);
            this.thread_generate_tb.KeyPress +=
                new System.Windows.Forms.KeyPressEventHandler(this.thread_generate_tb_key_press);
            // 
            // hsd_generate_tb
            // 
            this.hsd_generate_tb.Location = new System.Drawing.Point(224, 113);
            this.hsd_generate_tb.Name = "hsd_generate_tb";
            this.hsd_generate_tb.Size = new System.Drawing.Size(87, 23);
            this.hsd_generate_tb.TabIndex = 19;
            this.hsd_generate_tb.KeyPress +=
                new System.Windows.Forms.KeyPressEventHandler(this.hsd_generate_tb_key_press);
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(10, 55);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(46, 15);
            this.label.TabIndex = 18;
            this.label.Text = "Thread:";
            // 
            // note_generate_tb
            // 
            this.note_generate_tb.Location = new System.Drawing.Point(73, 82);
            this.note_generate_tb.Name = "note_generate_tb";
            this.note_generate_tb.Size = new System.Drawing.Size(237, 23);
            this.note_generate_tb.TabIndex = 17;
            // 
            // hsd_generate_cb
            // 
            this.hsd_generate_cb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.hsd_generate_cb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.hsd_generate_cb.FormattingEnabled = true;
            this.hsd_generate_cb.Location = new System.Drawing.Point(73, 112);
            this.hsd_generate_cb.Name = "hsd_generate_cb";
            this.hsd_generate_cb.Size = new System.Drawing.Size(143, 23);
            this.hsd_generate_cb.TabIndex = 16;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(23, 115);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 15);
            this.label11.TabIndex = 15;
            this.label11.Text = "HSD:";
            // 
            // add_btn
            // 
            this.add_btn.Location = new System.Drawing.Point(73, 253);
            this.add_btn.Name = "add_btn";
            this.add_btn.Size = new System.Drawing.Size(238, 27);
            this.add_btn.TabIndex = 13;
            this.add_btn.Text = "Add Database";
            this.add_btn.UseVisualStyleBackColor = true;
            this.add_btn.Click += new System.EventHandler(this.add_database_click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 147);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 15);
            this.label7.TabIndex = 12;
            this.label7.Text = "Encode:";
            // 
            // encode_generate_cb
            // 
            this.encode_generate_cb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.encode_generate_cb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.encode_generate_cb.FormattingEnabled = true;
            this.encode_generate_cb.Location = new System.Drawing.Point(73, 143);
            this.encode_generate_cb.Name = "encode_generate_cb";
            this.encode_generate_cb.Size = new System.Drawing.Size(237, 23);
            this.encode_generate_cb.TabIndex = 11;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(73, 219);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(87, 27);
            this.button4.TabIndex = 10;
            this.button4.Text = "Generate";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.generate_click);
            // 
            // keyword_generate_tb
            // 
            this.keyword_generate_tb.Location = new System.Drawing.Point(73, 22);
            this.keyword_generate_tb.Name = "keyword_generate_tb";
            this.keyword_generate_tb.Size = new System.Drawing.Size(237, 23);
            this.keyword_generate_tb.TabIndex = 6;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(224, 219);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(87, 27);
            this.button5.TabIndex = 9;
            this.button5.Text = "Copy";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.copy_generate_click);
            // 
            // result_generate_tb
            // 
            this.result_generate_tb.Location = new System.Drawing.Point(73, 189);
            this.result_generate_tb.Name = "result_generate_tb";
            this.result_generate_tb.ReadOnly = true;
            this.result_generate_tb.Size = new System.Drawing.Size(237, 23);
            this.result_generate_tb.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(164, 171);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 15);
            this.label4.TabIndex = 7;
            this.label4.Text = "Result:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 15);
            this.label5.TabIndex = 6;
            this.label5.Text = "Keyword:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dataGridView);
            this.groupBox3.Location = new System.Drawing.Point(665, 14);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(329, 448);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Data";
            // 
            // dataGridView
            // 
            this.dataGridView.ColumnHeadersHeightSizeMode =
                System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Location = new System.Drawing.Point(7, 15);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.Size = new System.Drawing.Size(315, 426);
            this.dataGridView.TabIndex = 0;
            this.dataGridView.CellClick +=
                new System.Windows.Forms.DataGridViewCellEventHandler(this.datagridview_cell_click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.thread_edit_tb);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.created_time_tb);
            this.groupBox4.Controls.Add(this.expired_rb);
            this.groupBox4.Controls.Add(this.hsd_edit_tb);
            this.groupBox4.Controls.Add(this.hsd_edit_cb);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.note_edit_tb);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.id_tb);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.delete_btn);
            this.groupBox4.Controls.Add(this.edit_btn);
            this.groupBox4.Controls.Add(this.key_tb);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Location = new System.Drawing.Point(339, 14);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(318, 262);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Edit";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(15, 112);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(46, 15);
            this.label16.TabIndex = 28;
            this.label16.Text = "Thread:";
            // 
            // thread_edit_tb
            // 
            this.thread_edit_tb.Location = new System.Drawing.Point(73, 108);
            this.thread_edit_tb.Name = "thread_edit_tb";
            this.thread_edit_tb.Size = new System.Drawing.Size(237, 23);
            this.thread_edit_tb.TabIndex = 27;
            this.thread_edit_tb.TextChanged += new System.EventHandler(this.thread_edit_tb_text_changed);
            this.thread_edit_tb.KeyPress +=
                new System.Windows.Forms.KeyPressEventHandler(this.thread_edit_tb_key_press);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 82);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(51, 15);
            this.label14.TabIndex = 26;
            this.label14.Text = "Created:";
            // 
            // created_time_tb
            // 
            this.created_time_tb.Location = new System.Drawing.Point(73, 78);
            this.created_time_tb.Name = "created_time_tb";
            this.created_time_tb.ReadOnly = true;
            this.created_time_tb.Size = new System.Drawing.Size(237, 23);
            this.created_time_tb.TabIndex = 25;
            // 
            // expired_rb
            // 
            this.expired_rb.AutoSize = true;
            this.expired_rb.Location = new System.Drawing.Point(241, 232);
            this.expired_rb.Name = "expired_rb";
            this.expired_rb.Size = new System.Drawing.Size(64, 19);
            this.expired_rb.TabIndex = 24;
            this.expired_rb.TabStop = true;
            this.expired_rb.Text = "Expired";
            this.expired_rb.UseVisualStyleBackColor = true;
            // 
            // hsd_edit_tb
            // 
            this.hsd_edit_tb.Location = new System.Drawing.Point(224, 168);
            this.hsd_edit_tb.Name = "hsd_edit_tb";
            this.hsd_edit_tb.Size = new System.Drawing.Size(87, 23);
            this.hsd_edit_tb.TabIndex = 22;
            this.hsd_edit_tb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.hsd_edit_tb_key_press);
            // 
            // hsd_edit_cb
            // 
            this.hsd_edit_cb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.hsd_edit_cb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.hsd_edit_cb.FormattingEnabled = true;
            this.hsd_edit_cb.Location = new System.Drawing.Point(73, 167);
            this.hsd_edit_cb.Name = "hsd_edit_cb";
            this.hsd_edit_cb.Size = new System.Drawing.Size(143, 23);
            this.hsd_edit_cb.TabIndex = 21;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(23, 142);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(36, 15);
            this.label13.TabIndex = 20;
            this.label13.Text = "Note:";
            // 
            // note_edit_tb
            // 
            this.note_edit_tb.Location = new System.Drawing.Point(73, 138);
            this.note_edit_tb.Name = "note_edit_tb";
            this.note_edit_tb.Size = new System.Drawing.Size(237, 23);
            this.note_edit_tb.TabIndex = 19;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(21, 171);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 15);
            this.label9.TabIndex = 9;
            this.label9.Text = "HSD:";
            // 
            // id_tb
            // 
            this.id_tb.Location = new System.Drawing.Point(73, 48);
            this.id_tb.Name = "id_tb";
            this.id_tb.ReadOnly = true;
            this.id_tb.Size = new System.Drawing.Size(237, 23);
            this.id_tb.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(37, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 15);
            this.label8.TabIndex = 7;
            this.label8.Text = "ID:";
            // 
            // delete_btn
            // 
            this.delete_btn.Location = new System.Drawing.Point(73, 198);
            this.delete_btn.Name = "delete_btn";
            this.delete_btn.Size = new System.Drawing.Size(87, 27);
            this.delete_btn.TabIndex = 5;
            this.delete_btn.Text = "Delete";
            this.delete_btn.UseVisualStyleBackColor = true;
            this.delete_btn.Click += new System.EventHandler(this.delete_click);
            // 
            // edit_btn
            // 
            this.edit_btn.Location = new System.Drawing.Point(224, 198);
            this.edit_btn.Name = "edit_btn";
            this.edit_btn.Size = new System.Drawing.Size(87, 27);
            this.edit_btn.TabIndex = 4;
            this.edit_btn.Text = "Edit";
            this.edit_btn.UseVisualStyleBackColor = true;
            this.edit_btn.Click += new System.EventHandler(this.edit_click);
            // 
            // key_tb
            // 
            this.key_tb.Location = new System.Drawing.Point(73, 15);
            this.key_tb.Name = "key_tb";
            this.key_tb.ReadOnly = true;
            this.key_tb.Size = new System.Drawing.Size(237, 23);
            this.key_tb.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(29, 18);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 15);
            this.label10.TabIndex = 0;
            this.label10.Text = "Key:";
            // 
            // CreateKeyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1006, 475);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "CreateKeyForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Create Key";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.dataGridView)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox encode_basic_cb;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox result_basic_tb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox keyword_basic_tb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox keyword_generate_tb;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox result_generate_tb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox encode_generate_cb;
        private System.Windows.Forms.Button add_btn;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox id_tb;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button delete_btn;
        private System.Windows.Forms.Button edit_btn;
        private System.Windows.Forms.TextBox key_tb;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.TextBox note_generate_tb;
        private System.Windows.Forms.ComboBox hsd_generate_cb;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox note_edit_tb;
        private System.Windows.Forms.TextBox hsd_edit_tb;
        private System.Windows.Forms.ComboBox hsd_edit_cb;
        private System.Windows.Forms.TextBox hsd_generate_tb;
        private System.Windows.Forms.RadioButton expired_rb;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox created_time_tb;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox thread_generate_tb;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox thread_edit_tb;
    }
}