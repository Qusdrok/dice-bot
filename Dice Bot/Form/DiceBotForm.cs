﻿using Dice_Bot.Event;
using Dice_Bot.Model;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Utility;

namespace Dice_Bot
{
    public partial class DiceBotForm : Form
    {
        private Dictionary<string, string> accounts = new Dictionary<string, string>();
        private List<DataGridView> dataGridViews = new List<DataGridView>();
        private List<string> columns = new List<string>();
        private List<string> proxys = new List<string>();
        private List<Chart> charts = new List<Chart>();
        private List<GBGame> gbGames = new List<GBGame>();
        public List<ChromeDriver> chromes = new List<ChromeDriver>();
        public List<FirefoxDriver> firefoxs = new List<FirefoxDriver>();
        public List<DiceBot> dbs = new List<DiceBot>();
        private Random rnd = new Random();

        private WebsiteInfo wbInfo;
        private Website wbSelected;
        private Rectangle DGVBounds;
        private Rectangle ChartBounds;
        private Rectangle GBBounds;
        private Rectangle BetTBMaxBounds;
        private Rectangle BetTBMinBounds;
        private Rectangle CashoutTBMaxBounds;
        private Rectangle CashoutTBMinBounds;
        private Rectangle GameCBBounds;
        private Rectangle CoinCBBounds;

        private bool isCheckedProgrammer;
        private int totalWin;
        private int totalLose;
        private int totalBet;

        public DiceBotForm(int maxThread)
        {
            InitializeComponent();
            InitForm(maxThread);
        }

        #region Main Function
        private void InitForm(int maxThread)
        {
            DGVBounds = this.data_grid_view.Bounds;
            ChartBounds = this.chart.Bounds;
            GBBounds = this.gb_game.Bounds;
            BetTBMaxBounds = this.gb_bet_max_tb.Bounds;
            BetTBMinBounds = this.gb_bet_min_tb.Bounds;
            CashoutTBMaxBounds = this.gb_cashout_max_tb.Bounds;
            CashoutTBMinBounds = this.gb_cashout_min_tb.Bounds;
            GameCBBounds = this.gb_game_cb.Bounds;
            CoinCBBounds = this.gb_coin_cb.Bounds;

            gbGames.Add(new GBGame(this.gb_bet_max_tb, this.gb_bet_max_tb, this.gb_cashout_max_tb,
                this.gb_cashout_min_tb, this.gb_coin_cb, this.gb_game_cb));
            dataGridViews.Add(this.data_grid_view);
            charts.Add(this.chart);

            browser_cb.Items.AddRange(Enum.GetValues(typeof(Browser)).Cast<object>().ToArray());
            website_cb.Items.AddRange(Enum.GetValues(typeof(Website)).Cast<object>().ToArray());

            browser_cb.SelectedIndex = 1;
            website_cb.SelectedIndex = 0;

            InitComboBox();
            InitFieldBet();
            InitProgrammer();

            this.thread_nud.Maximum = maxThread;
            for (var i = 0; i < columns.Count; i++)
                this.data_grid_view.Columns.Add("Column " + i, columns[i]);
        }

        private void InitComboBox()
        {
            this.cashout_win_cb.SelectedIndex = 3;
            this.bet_win_cb.SelectedIndex = 3;

            this.cashout_lose_cb.SelectedIndex = 3;
            this.bet_lose_cb.SelectedIndex = 3;

            this.cashout_profit_win_lose_cb.SelectedIndex = 3;
            this.bet_profit_win_lose_cb.SelectedIndex = 3;

            this.wbSelected = (Website)website_cb.SelectedItem;
            this.wbInfo = Event.Event.WebsiteInfos.SingleOrDefault(x => x.website == this.wbSelected);

            var coins = this.wbInfo.coinInfos.Select(x => x.coin).Cast<object>().ToArray();
            var games = this.wbInfo.games.Cast<object>().ToArray();

            deposit_cb.Items.Clear();
            deposit_cb.Items.AddRange(coins);

            foreach (var grpGame in gbGames)
            {
                grpGame.coin.Items.Clear();
                grpGame.game.Items.Clear();

                grpGame.coin.Items.AddRange(coins);
                grpGame.game.Items.AddRange(games);

                grpGame.coin.SelectedIndex = 0;
                grpGame.game.SelectedIndex = 0;
            }
        }

        private void InitFieldBet()
        {
            var fields = new Bet(false, 0, 0, DateTime.Now, null)
                .GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (var field in fields)
            {
                this.columns.Add(field.Name);
            }
        }

        private void InitProgrammer()
        {
            this.programmer_fctb.Text = @"-- Example code
-- Sets your first winchance, and cashout; the usual recipe would be winchance = 99 / cashout.
cashout   = 1.1
winchance = 90

-- Sets your first bet, min bet is 0.0000001.
nextbet   = 0.0000001

-- Bet high/low when true/false.
bethigh   = true

function dobet()
    -- If you win/lose, the result is true/false
    if result then
    else
    end
end";
            this.variables_tb.Text = "-- Method --\n\n" +
                "* print(newContent, color); (newContent, color is string)\n" +
                "- Example: print('qusdrok', 'red')\n\n" +
                "* withdraw(amount, address); (amount is decimal, address is string)\n" +
                "- Example: withdraw(1000m, 'bc1q5w0gt8du285v9d4l54lwu5e7r4ac88ysrah5lg')\n\n" +
                "* resetseed(); (depends on the website)\n" +
                "- Example: resetseed()\n\n" +
                "* stopprogram();\n" +
                "- Example: stopprogram()\n\n" +
                "-- Variables --\n\n" +
                "* nextbet: amount of coin to bet\n" +
                "* cashout: amount of cashout to bet\n" +
                "* winchance: % won calculate from cashout\n" +
                "* bethigh: bet high/low in game dice\n" +
                "* balance: amount of current balance after bet\n" +
                "* rootBalance: amount of root balance\n" +
                "* roll: result of the latest game\n" +
                "* wins: total win from start bet\n" +
                "* loses: total lose from start bet\n" +
                "* totalprofit: total profit from start bet\n" +
                "* profitwin: total profit win from start bet\n" +
                "* profitlose: total profit lose from start bet\n" +
                "* result: result of current game if you have bet";
        }

        private void InitSettings()
        {
            #region TextBox, Label
            Settings.LBTotalWin = this.total_win_lb;
            Settings.LBTotalLose = this.total_lose_lb;
            Settings.LBTotalBet = this.total_bet_lb;

            Settings.TBWithdrawAddress = this.withdraw_address_tb.Text;
            Settings.TBWithdrawPassword = this.withdraw_password_tb.Text;

            Settings.TBWithdrawAmount = decimal.Parse(this.withdraw_amount_tb.Text);

            Settings.TBStopEachGameMin = int.Parse(this.stop_each_game_min_tb.Text);
            Settings.TBStopEachGameMax = int.Parse(this.stop_each_game_max_tb.Text);

            Settings.TBStopGameWinMin = int.Parse(this.stop_game_win_min_tb.Text);
            Settings.TBStopGameWinMax = int.Parse(this.stop_game_win_max_tb.Text);

            Settings.TBStopGameLoseMin = int.Parse(this.stop_game_lose_min_tb.Text);
            Settings.TBStopGameLoseMax = int.Parse(this.stop_game_lose_max_tb.Text);

            Settings.TBAfterWinStopGameMin = int.Parse(this.after_win_stop_game_min_tb.Text);
            Settings.TBAfterWinStopGameMax = int.Parse(this.after_win_stop_game_max_tb.Text);

            Settings.TBStopGameAfterWinMin = int.Parse(this.stop_game_after_win_min_tb.Text);
            Settings.TBStopGameAfterWinMax = int.Parse(this.stop_game_after_win_max_tb.Text);

            Settings.TBAfterLoseStopGameMin = int.Parse(this.after_lose_stop_game_min_tb.Text);
            Settings.TBAfterLoseStopGameMax = int.Parse(this.after_lose_stop_game_max_tb.Text);

            Settings.TBStopGameAfterLoseMin = int.Parse(this.stop_game_after_lose_min_tb.Text);
            Settings.TBStopGameAfterLoseMax = int.Parse(this.stop_game_after_lose_max_tb.Text);

            Settings.TBAfterWinStopThreadMin = int.Parse(this.after_win_stop_thread_min_tb.Text);
            Settings.TBAfterWinStopThreadMax = int.Parse(this.after_win_stop_thread_max_tb.Text);

            Settings.TBAfterLoseStopThreadMin = int.Parse(this.after_lose_stop_thread_min_tb.Text);
            Settings.TBAfterLoseStopThreadMax = int.Parse(this.after_lose_stop_thread_max_tb.Text);

            Settings.TBTargetWin = decimal.Parse(this.target_win_tb.Text);
            Settings.TBTargetLose = decimal.Parse(this.target_lose_tb.Text);

            Settings.TBCashoutWinMax = decimal.Parse(this.cashout_win_max_tb.Text);
            Settings.TBCashoutWinMin = decimal.Parse(this.cashout_win_min_tb.Text);

            Settings.TBBetWinMax = decimal.Parse(this.bet_win_max_tb.Text);
            Settings.TBBetWinMin = decimal.Parse(this.bet_win_min_tb.Text);

            Settings.TBCashoutLoseMax = decimal.Parse(this.cashout_lose_max_tb.Text);
            Settings.TBCashoutLoseMin = decimal.Parse(this.cashout_lose_min_tb.Text);

            Settings.TBBetLoseMax = decimal.Parse(this.bet_lose_max_tb.Text);
            Settings.TBBetLoseMin = decimal.Parse(this.bet_lose_min_tb.Text);

            Settings.TBCashoutProfitWinLoseMax = decimal.Parse(this.cashout_profit_win_lose_max_tb.Text);
            Settings.TBCashoutProfitWinLoseMin = decimal.Parse(this.cashout_profit_win_lose_min_tb.Text);

            Settings.TBBetProfitWinLoseMax = decimal.Parse(this.bet_profit_win_lose_max_tb.Text);
            Settings.TBBetProfitWinLoseMin = decimal.Parse(this.bet_profit_win_lose_min_tb.Text);
            #endregion

            #region ComboBox
            Settings.CBStopEachGame = this.stop_each_game_checkbox.Checked;

            Settings.CBCashoutWin = this.cashout_win_cb.SelectedIndex;
            Settings.CBBetWin = this.bet_win_cb.SelectedIndex;

            Settings.CBCashoutLose = this.cashout_lose_cb.SelectedIndex;
            Settings.CBBetLose = this.bet_lose_cb.SelectedIndex;

            Settings.CBCashoutProfitWinLose = this.cashout_profit_win_lose_cb.SelectedIndex;
            Settings.CBBetProfitWinLose = this.bet_profit_win_lose_cb.SelectedIndex;
            #endregion

            #region CheckBox, RadioButton
            Settings.RBFloorAccumulationProfitWinLose = this.floor_accumulation_profit_win_lose_rb.Checked;
            Settings.RBNormal = this.normal_rb.Checked;

            Settings.RBResetWin = this.reset_win_rb.Checked;
            Settings.RBMultiplyWin = this.multiply_win_rb.Checked;

            Settings.RBResetLose = this.reset_lose_rb.Checked;
            Settings.RBMultiplyLose = this.multiply_lose_rb.Checked;
            #endregion
        }

        public long AddDataToChart(Chart chart, Bet bet, long chartBets)
        {
            if (InvokeRequired) return (long) Invoke(new EventAddDataToChart(this.AddDataToChart),chart, bet, chartBets);
            var liveBets = 1000;
            var chartPoints = chart.Series[0].Points;
            chartBets++;

            while (chartPoints.Count > liveBets - 1) chartPoints.RemoveAt(0);
            var chartArea = chart.ChartAreas[0];
            var axisX = chartArea.AxisX;
            var axisY = chartArea.AxisY;

            axisX.Maximum = chartBets < liveBets ? chartBets + 1 : liveBets;
            axisX.Minimum = 1;

            if (chartBets == 1)
            {
                chartPoints.Add(new DataPoint(0, 0)
                {
                    Color = Color.Green,
                    BorderColor = Color.Green,
                    MarkerColor = Color.Green,

                    MarkerSize = 3,
                    BorderWidth = 1,

                    MarkerStyle = MarkerStyle.Circle,
                    BorderDashStyle = ChartDashStyle.Solid
                });
            }

            var rsColor = bet.basic.result ? Color.Green : Color.Red;
            chartPoints.Add(new DataPoint(chartBets, (double)bet.totalProfit)
            {
                Color = rsColor,
                BorderColor = rsColor,
                MarkerColor = rsColor,

                MarkerSize = bet.basic.result ? 3 : 2,
                BorderWidth = 1,

                MarkerStyle = MarkerStyle.Circle,
                BorderDashStyle = ChartDashStyle.Solid
            });

            try
            {
                if (chartPoints.Count <= liveBets - 1 && chartPoints.Count % 10 != 0) return chartBets;
                var maxY = (decimal)chartPoints.Max(x => x.YValues[0]);
                var minY = (decimal)chartPoints.Min(x => x.YValues[0]);
                var span = maxY - minY;

                if (maxY <= (decimal)axisY.Maximum && minY >= (decimal)axisY.Minimum &&
                    !((double)maxY < axisY.Maximum - (double)(span / 2.0m)) &&
                    !((double)minY > axisY.Minimum + (double)(span / 2.0m))) return chartBets;
                if (minY <= 0.0000001m && minY >= -0.0000001m || maxY <= 0.0000001m && maxY >= -0.0000001m)
                    return chartBets;
                var largeSpan = (span * 100000000).ToString("0");
                var firstDigit = decimal.Parse(largeSpan.Substring(0, 1)) + 1;

                decimal zeros = (span * 100000000).ToString("0").Length - 1;
                var newSpan = firstDigit * (decimal)Math.Pow(10, (double)zeros);

                newSpan /= 100000000m;
                var interval = newSpan / 5.0m;
                var tmp = maxY / interval;

                var ceiling = Math.Ceiling(tmp);
                var floor = Math.Floor(minY / interval);

                var newMax = ceiling * interval + interval;
                var newMin = floor * interval - interval;

                if (newMax <= newMin || newMax <= 0.0000001m && newMax >= -0.0000001m ||
                    newMin <= 0.0000001m && newMin >= -0.0000001m) return chartBets;

                axisY.Maximum = (double)newMax;
                axisY.Minimum = (double)newMin;
                return chartBets;
            }
            catch
            {
                return chartBets;
            }
        }

        public void AddDataToDGV(DataGridView dataGridView, Bet bet)
        {
            if (InvokeRequired)
            {
                Invoke(new EventAddDataToDGV(this.AddDataToDGV), dataGridView, bet);
            }
            else
            {
                dataGridView.DataBindings.Clear();
                dataGridView.Rows.Insert(0, bet.betID, bet.roll, bet.high,
                    bet.bet, bet.cashout, bet.winChance, bet.profitWin, bet.profitLose,
                    bet.totalProfit, bet.balance, bet.rootBalance, bet.win, bet.lose, bet.time);

                var rows = dataGridView.Rows;
                dataGridView.Rows[0].DefaultCellStyle.BackColor = bet.basic.result ? Color.LightGreen : Color.Pink;
                while (rows.Count > 100) rows.RemoveAt(rows.Count - 1);
            }
        }

        public void UpdateHistory(string newContent, Color color, int threadIndex = -1)
        {
            if (InvokeRequired)
            {
                Invoke(new EventUpdateHistory(this.UpdateHistory), newContent, color, threadIndex);
                return;
            }

            var now = DateTime.Now.ToString();
            if (this.history_tb.Lines.Length > 8888)
            {
                this.history_tb.Clear();
                this.history_tb.AppendText(now + " Đã xoá lịch sử đặt cược");
            }

            this.history_tb.SelectionColor = color;
            this.tool_strip_status_lb.Text = "Status: " + newContent;
            this.history_tb.AppendText(now + " Thread " + (threadIndex > -1 ? "" + threadIndex : "Main")
                + ": " + newContent + "\n" + Environment.NewLine);
            this.history_tb.ScrollToCaret();
        }

        public void UpdateTotalBet(bool result)
        {
            if (InvokeRequired)
            {
                Invoke(new EventUpdateTotalBet(this.UpdateTotalBet), result);
                return;
            }

            if (result) this.total_win_lb.Text = totalWin++.ToString();
            else this.total_lose_lb.Text = totalLose++.ToString();
            this.total_bet_lb.Text = totalBet++.ToString();
        }

        public void EnableAllControl(bool enable, bool isProgrammer = false)
        {
            if (InvokeRequired)
            {
                Invoke(new EventEnableAllControl(this.EnableAllControl), enable, isProgrammer);
            }
            else
            {
                ((Control)this.advanced_tabpage).Enabled = enable;
                ((Control)this.game_tabpage).Enabled = true;
                this.programmer_fctb.Enabled = !enable;
                
                if (!isProgrammer)
                {
                    this.gb_setting.Enabled = enable;
                    this.gb_proxy.Enabled = enable;

                    this.account_tb.Enabled = enable;
                    this.start_btn.Enabled = enable;
                    this.stop_btn.Enabled = !enable;
                }
                else
                {
                    foreach (var gbGame in gbGames)
                    {
                        gbGame.betMax.Enabled = false;
                        gbGame.betMin.Enabled = false;

                        gbGame.cashoutMax.Enabled = false;
                        gbGame.cashoutMin.Enabled = false;
                    }
                }
            }
        }

        private decimal GetAccount()
        {
            foreach (var line in this.account_tb.Lines)
            {
                var tmp = line.Split(';');
                this.accounts.Add(tmp[0], tmp[1]);
            }

            return this.accounts.Count;
        }

        private bool StartGame()
        {
            this.accounts.Clear();
            var accounts = GetAccount();

            if (string.IsNullOrEmpty(this.account_tb.Text))
            {
                UtilsDialog.DialogError("Bạn chưa nhập tài khoản :((");
                return false;
            }

            if (accounts != Settings.NUDThread)
            {
                UtilsDialog.DialogWarning("Số tài khoản và số luồng phải bằng nhau");
                return false;
            }

            if (proxy_rb.Checked && proxys.Count != Settings.NUDThread)
            {
                UtilsDialog.DialogWarning("Số lượng proxy và số luồng phải bằng nhau");
                return false;
            }

            var tasks = new List<Task>();
            var browserIndex = this.browser_cb.SelectedIndex;
            var isProgrammer = this.programmer_rb.Checked;
            var programmer = this.programmer_fctb.Text;
            var betHigh = this.bet_high_rb.Checked;
            var iTask = 0;
            var i = 0;

            UtilsAsync.RunAsyncVoid(async delegate
            {
                while (i < Settings.NUDThread)
                {
                    var flag = iTask < Settings.NUDThread;
                    if (flag)
                    {
                        #region Select Game
                        Interlocked.Increment(ref iTask);
                        DiceBotInfo dbInfo = null;
                        Bet bet = null;
                        var game = Game.CrashClassic;
                        Coin coin;
                        var index = i;

                        this.Invoke(new MethodInvoker(() =>
                        {
                            var account = this.accounts.ElementAt(index);
                            var gbGame = this.gbGames[index];

                            var cashoutMax = decimal.Parse(gbGame.cashoutMax.Text);
                            var cashoutMin = decimal.Parse(gbGame.cashoutMin.Text);
                            var betMax = decimal.Parse(gbGame.betMax.Text);
                            var betMin = decimal.Parse(gbGame.betMin.Text);

                            coin = (Coin)gbGame.coin.SelectedItem;
                            game = (Game)gbGame.game.SelectedItem;

                            var basic = new Basic(account.Key, account.Value, betMax,
                                betMin, cashoutMax, cashoutMin, coin, game);

                            var dgv = this.dataGridViews[index];
                            var chart = this.charts[index];
                            var proxy = string.Empty;
                            if (this.proxys.Count > 0) proxy = this.proxys[index];

                            dgv.Rows.Clear();
                            chart.Series.Clear();
                            chart.ChartAreas.Clear();

                            chart.Series.Add("Series" + index + 1);
                            chart.ChartAreas.Add("ChartArea" + index + 1);
                            chart.Series[0].ChartType = SeriesChartType.Spline;

                            bet = new Bet(betHigh, basic.GetBet(), basic.GetCashout(), DateTime.Now, basic);
                            dbInfo = new DiceBotInfo(this, dgv, chart, index, browserIndex, wbInfo.divideChance,
                                programmer, proxy, isProgrammer: isProgrammer);
                        }));

                        tasks.Add(Task.Run(() =>
                        {
                            switch (this.wbSelected)
                            {
                                case Website.BCGame:
                                    switch (game)
                                    {
                                        case Game.CrashClassic:
                                            new BCGame.BCGameCrashClassic(dbInfo, this.wbInfo, bet);
                                            break;
                                        case Game.CrashTrenballRed:
                                            new BCGame.BCGameCrashTrenball(dbInfo, this.wbInfo, bet, 1);
                                            break;
                                        case Game.CrashTrenballGreen:
                                            new BCGame.BCGameCrashTrenball(dbInfo, this.wbInfo, bet, 2);
                                            break;
                                        case Game.CrashTrenballMoon:
                                            new BCGame.BCGameCrashTrenball(dbInfo, this.wbInfo, bet, 3);
                                            break;
                                        case Game.Dice:
                                            new BCGame.BCGameDice(dbInfo, this.wbInfo, bet);
                                            break;
                                    }
                                    break;
                                case Website.Dice999:
                                    new Dice999.Dice999(dbInfo, this.wbInfo, bet);
                                    break;
                                case Website.EtherCrash:
                                    new EtherCrash.EtherCrash(dbInfo, this.wbInfo, bet);
                                    break;
                                case Website.PrimeDice:
                                    new PrimeDice.PrimeDice(dbInfo, this.wbInfo, bet);
                                    break;
                            }
                            Interlocked.Increment(ref iTask);
                        }));
                        #endregion
                        i++;
                        await Task.Delay(rnd.Next(150, 200));
                    }
                    else
                    {
                        await Task.Delay(rnd.Next(150, 200));
                    }
                }
                await Task.WhenAll(tasks.ToArray());
            });
            return true;
        }

        private void DestroyDriver()
        {
            switch ((Browser)browser_cb.SelectedItem)
            {
                case Browser.Chrome:
                    foreach (var chrome in chromes)
                    {
                        chrome.Quit();
                    }
                    break;
                case Browser.FireFox:
                    foreach (var firefox in firefoxs)
                    {
                        firefox.Quit();
                    }
                    break;
            }
        }
        #endregion

        #region Function Form
        private void thread_numeric_value_changed(object sender, EventArgs e)
        {
            Settings.NUDThread = (int)this.thread_nud.Value;
            var dgvLength = dataGridViews.Count;

            var games = this.gb_game_cb.Items.Cast<object>().ToArray();
            var coins = this.gb_coin_cb.Items.Cast<object>().ToArray();

            for (var i = data_grid_view_panel.Controls.Count - 1; i >= Settings.NUDThread; i--)
            {
                data_grid_view_panel.Controls.RemoveAt(i);
                dataGridViews.RemoveAt(i);

                chart_panel.Controls.RemoveAt(i);
                charts.RemoveAt(i);

                gb_game_panel.Controls.RemoveAt(i);
                gbGames.RemoveAt(i);
            }

            for (var i = dgvLength; i < Settings.NUDThread; i++)
            {
                var cloneChart = new Chart();
                var cloneDGV = new DataGridView();
                var cloneBetTBMax = new TextBox();
                var cloneBetTBMin = new TextBox();
                var cloneCashoutTBMax = new TextBox();
                var cloneCashoutTBMin = new TextBox();
                var cloneGB = new GroupBox();
                var cloneGameCB = new ComboBox();
                var cloneCoinCB = new ComboBox();

                for (var j = 0; j < columns.Count; j++) cloneDGV.Columns.Add("Column " + j, columns[j]);
                cloneDGV.Size = this.data_grid_view.Size;
                cloneDGV.SetBounds(DGVBounds.X + 890 * i, DGVBounds.Y, DGVBounds.Width, DGVBounds.Height);
                cloneDGV.AllowUserToAddRows = false;
                cloneDGV.ReadOnly = true;
                dataGridViews.Add(cloneDGV);

                cloneChart.Size = this.chart.Size;
                cloneChart.SetBounds(ChartBounds.X + 890 * i, ChartBounds.Y, ChartBounds.Width, ChartBounds.Height);
                charts.Add(cloneChart);

                cloneBetTBMax.Size = this.gb_bet_max_tb.Size;
                cloneBetTBMin.Size = this.gb_bet_min_tb.Size;
                cloneBetTBMax.Text = this.gb_bet_max_tb.Text;
                cloneBetTBMin.Text = this.gb_bet_min_tb.Text;

                cloneBetTBMax.KeyPress += this.gb_bet_max_tb_key_press;
                cloneBetTBMax.TextChanged += this.gb_bet_max_tb_text_changed;
                cloneBetTBMin.KeyPress += this.gb_bet_min_tb_key_press;
                cloneBetTBMin.TextChanged += this.gb_bet_min_tb_text_changed;

                cloneBetTBMax.SetBounds(BetTBMaxBounds.X, BetTBMaxBounds.Y, BetTBMaxBounds.Width, BetTBMaxBounds.Height);
                cloneBetTBMin.SetBounds(BetTBMinBounds.X, BetTBMinBounds.Y, BetTBMinBounds.Width, BetTBMinBounds.Height);

                cloneCashoutTBMax.Size = this.gb_cashout_max_tb.Size;
                cloneCashoutTBMin.Size = this.gb_cashout_min_tb.Size;
                cloneCashoutTBMax.Text = this.gb_cashout_max_tb.Text;
                cloneCashoutTBMin.Text = this.gb_cashout_min_tb.Text;
                
                cloneCashoutTBMax.KeyPress += this.gb_cashout_max_tb_key_press;
                cloneCashoutTBMax.TextChanged += this.gb_cashout_max_tb_text_changed;
                cloneCashoutTBMin.KeyPress += this.gb_cashout_min_tb_key_press;
                cloneCashoutTBMin.TextChanged += this.gb_cashout_min_tb_text_changed;

                cloneCashoutTBMax.SetBounds(CashoutTBMaxBounds.X, CashoutTBMaxBounds.Y,
                    CashoutTBMaxBounds.Width, CashoutTBMaxBounds.Height);
                cloneCashoutTBMin.SetBounds(CashoutTBMinBounds.X, CashoutTBMinBounds.Y,
                    CashoutTBMinBounds.Width, CashoutTBMinBounds.Height);

                cloneGameCB.Size = this.gb_game_cb.Size;
                cloneGameCB.Items.AddRange(games);
                cloneGameCB.SelectedIndex = 0;
                cloneGameCB.KeyPress += gb_game_cb_key_press;
                cloneGameCB.SetBounds(GameCBBounds.X, GameCBBounds.Y, GameCBBounds.Width, GameCBBounds.Height);

                cloneCoinCB.Size = this.gb_coin_cb.Size;
                cloneCoinCB.Items.AddRange(coins);
                cloneCoinCB.SelectedIndex = 0;
                cloneCoinCB.KeyPress += gb_coin_cb_key_press;
                cloneCoinCB.SetBounds(CoinCBBounds.X, CoinCBBounds.Y, CoinCBBounds.Width, CoinCBBounds.Height);

                cloneGB.Text = "Bet, Cashout, Coin, Game " + (1 + i);
                cloneGB.Size = this.gb_game.Size;
                cloneGB.SetBounds(GBBounds.X + 162 * (i % 3), GBBounds.Y + 124 * (i / 3),
                    GBBounds.Width, GBBounds.Height);
                cloneGB.Controls.Add(cloneBetTBMax);
                cloneGB.Controls.Add(cloneBetTBMin);
                cloneGB.Controls.Add(cloneCashoutTBMax);
                cloneGB.Controls.Add(cloneCashoutTBMin);
                cloneGB.Controls.Add(cloneCoinCB);
                cloneGB.Controls.Add(cloneGameCB);
                gbGames.Add(new GBGame(cloneBetTBMax, cloneBetTBMin, cloneCashoutTBMax,
                    cloneCashoutTBMin, cloneCoinCB, cloneGameCB));

                gb_game_panel.Controls.Add(cloneGB);
                data_grid_view_panel.Controls.Add(cloneDGV);
                chart_panel.Controls.Add(cloneChart);
            }
        }

        private void thread_nud_key_press(object sender, KeyPressEventArgs e)
        {
            e.Handled = UtilsValue.KeyPressNumber(e);
        }

        private void total_target_key_press(object sender, KeyPressEventArgs e)
        {
            e.Handled = UtilsValue.KeyPressNumber(e, true);
        }

        private void start_btn_click(object sender, EventArgs e)
        {
            this.InitSettings();
            if (this.StartGame()) this.EnableAllControl(false);
        }

        private void stop_btn_click(object sender, EventArgs e)
        {
            DestroyDriver();
            foreach (var diceBot in dbs)
            {
                diceBot.StopProgram();
            }

            this.start_btn.Enabled = true;
            this.EnableAllControl(true);
        }

        private void deposit_selected_index_changed(object sender, EventArgs e)
        {
            var coin = (Coin)deposit_cb.SelectedItem;
            try
            {
                Clipboard.SetText(Event.Event.DepositCoin[coin]);
                this.UpdateHistory("Bạn đã copy ví " + coin + " của DEV !!!", Color.Black);
            }
            catch
            {
                this.UpdateHistory("DEV không có coin " + coin + " !!!", Color.Black);
            }
        }

        private void website_cb_selected_index_changed(object sender, EventArgs e)
        {
            this.wbSelected = (Website)website_cb.SelectedItem;
            this.InitComboBox();

            string message;
            switch (this.wbSelected)
            {
                case Website.BCGame:
                case Website.Dice999:
                    message = "'Username;Password'";
                    break;
                case Website.EtherCrash:
                    message = "'Username;Session-ID', bạn có thể lấy nó ở <user> -> Security -> SessionID";
                    break;
                case Website.PrimeDice:
                    message = "'Username;API-Key, bạn có thể lấy nó ở <user> -> Security -> API'";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            this.UpdateHistory("Bạn đã chọn website " + this.wbSelected + ", " +
                "tài khoản nhập theo định dạng " + message, Color.Black);
        }

        private void withdraw_amount_tb_key_press(object sender, KeyPressEventArgs e)
        {
            e.Handled = UtilsValue.KeyPressNumber(e, true);
        }

        private void default_proxy_rb_checked_changed(object sender, EventArgs e)
        {
            proxy_tb.Enabled = false;
            add_proxy_file_btn.Enabled = false;
        }

        private void add_account_file_click(object sender, EventArgs e)
        {
            var openFileDialog = new OpenFileDialog {Filter = "TXT files|*.txt"};
            var result = openFileDialog.ShowDialog();

            if (result != DialogResult.OK) return;
            var lines = File.ReadAllLines(openFileDialog.FileName);
            this.account_tb.Clear();

            foreach (var line in lines)
            {
                this.account_tb.AppendText(line + Environment.NewLine);
            }
        }

        private void add_proxy_file_click(object sender, EventArgs e)
        {
            var openFileDialog = new OpenFileDialog {Filter = "TXT files|*.txt"};
            var result = openFileDialog.ShowDialog();

            if (result != DialogResult.OK) return;
            var lines = File.ReadAllLines(openFileDialog.FileName);
            this.proxys.Clear();
            this.proxy_tb.Clear();

            foreach (var line in lines)
            {
                this.proxys.Add(line);
                this.proxy_tb.AppendText(line + Environment.NewLine);
            }
        }

        private void proxy_rb_checked_changed(object sender, EventArgs e)
        {
            proxy_tb.Enabled = true;
            add_proxy_file_btn.Enabled = true;
        }

        private void max_row_tb_key_press(object sender, KeyPressEventArgs e)
        {
            e.Handled = UtilsValue.KeyPressNumber(e);
        }

        private void gb_game_cb_key_press(object sender, KeyPressEventArgs e)
        {
            e.Handled = UtilsValue.KeyPressNumber(e, hasChanged: false);
        }

        private void deposit_cb_key_press(object sender, KeyPressEventArgs e)
        {
            e.Handled = UtilsValue.KeyPressNumber(e, hasChanged: false);
        }

        private void website_cb_key_press(object sender, KeyPressEventArgs e)
        {
            e.Handled = UtilsValue.KeyPressNumber(e, hasChanged: false);
        }

        private void stop_each_game_checkbox_checked_changed(object sender, EventArgs e)
        {
            var enabled = this.stop_each_game_checkbox.Checked;
            this.grp_stop_game.Enabled = !enabled;
        }

        private void normal_rb_checked_changed(object sender, EventArgs e)
        {
            this.gb_on_lose.Enabled = true;
            this.gb_on_win.Enabled = true;
            this.gb_profit_win_lose.Enabled = false;
        }

        private void profit_win_lose_rb_checked_changed(object sender, EventArgs e)
        {
            this.gb_on_lose.Enabled = false;
            this.gb_on_win.Enabled = false;
            this.gb_profit_win_lose.Enabled = true;
        }

        private void reset_win_rb_checked_changed(object sender, EventArgs e)
        {
            this.gb_cashout_win.Enabled = false;
            this.gb_bet_win.Enabled = false;
        }

        private void multiply_win_rb_checked_changed(object sender, EventArgs e)
        {
            this.gb_cashout_win.Enabled = true;
            this.gb_bet_win.Enabled = true;
        }

        private void reset_lose_rb_checked_changed(object sender, EventArgs e)
        {
            this.gb_cashout_lose.Enabled = false;
            this.gb_bet_lose.Enabled = false;
        }

        private void multiply_lose_rb_checked_changed(object sender, EventArgs e)
        {
            this.gb_cashout_lose.Enabled = true;
            this.gb_bet_lose.Enabled = true;
        }

        private void gb_coin_cb_key_press(object sender, KeyPressEventArgs e)
        {
            e.Handled = UtilsValue.KeyPressNumber(e, hasChanged: false);
        }

        private void browser_cb_key_press(object sender, KeyPressEventArgs e)
        {
            e.Handled = UtilsValue.KeyPressNumber(e, hasChanged: false);
        }

        private void gb_bet_min_tb_text_changed(object sender, EventArgs e)
        {
            this.gb_bet_min_tb.Text = UtilsValue.TextChanged(this.gb_bet_min_tb, 0.0000001m, 100m);
        }

        private void gb_bet_max_tb_text_changed(object sender, EventArgs e)
        {
            this.gb_bet_max_tb.Text = UtilsValue.TextChanged(this.gb_bet_max_tb, 0.0000001m, 100m);
        }

        private void gb_cashout_min_tb_text_changed(object sender, EventArgs e)
        {
            this.gb_cashout_min_tb.Text = UtilsValue.TextChanged(this.gb_cashout_min_tb, 1.01m, 100m);
        }

        private void gb_cashout_max_tb_text_changed(object sender, EventArgs e)
        {
            this.gb_cashout_max_tb.Text = UtilsValue.TextChanged(this.gb_cashout_max_tb, 1.01m, 100m);
        }

        private void gb_bet_min_tb_key_press(object sender, KeyPressEventArgs e)
        {
            e.Handled = UtilsValue.KeyPressNumber(e, true);
        }

        private void gb_bet_max_tb_key_press(object sender, KeyPressEventArgs e)
        {
            e.Handled = UtilsValue.KeyPressNumber(e, true);
        }

        private void gb_cashout_min_tb_key_press(object sender, KeyPressEventArgs e)
        {
            e.Handled = UtilsValue.KeyPressNumber(e, true);
        }

        private void gb_cashout_max_tb_key_press(object sender, KeyPressEventArgs e)
        {
            e.Handled = UtilsValue.KeyPressNumber(e, true);
        }

        private void dice_bot_form_closed(object sender, FormClosedEventArgs e)
        {
            DestroyDriver();
        }

        private void gb_coin_cb_seleted_index_changed(object sender, EventArgs e)
        {
            var comboboxs = gbGames.Select(x => x.coin.SelectedValue).ToList();
        }

        private void gb_coin_cb_enter(object sender, EventArgs e)
        {
            
        }

        private void programmer_rb_click(object sender, EventArgs e)
        {
            this.programmer_rb.Checked = !isCheckedProgrammer;
            this.EnableAllControl(isCheckedProgrammer, !isCheckedProgrammer);
            isCheckedProgrammer = !isCheckedProgrammer;
        }
        #endregion
    }
}