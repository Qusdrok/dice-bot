﻿using DeviceId;
using System;
using System.Windows.Forms;
using Utility;

namespace Dice_Bot
{
    public partial class LoginForm : Form
    {
        private double timestamp;
        private string key;
        private string deviceID;

        public LoginForm()
        {
            InitializeComponent();

            this.timestamp = DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
            this.key = Properties.Settings.Default.KEY;
            this.deviceID = GetDeviceID();

            if (string.IsNullOrEmpty(key)) return;
            key_tb.Text = key;
            key_tb.ReadOnly = true;
            key_tb.PasswordChar = '*';
        }

        private async void Login(string access = null)
        {
            if (string.IsNullOrEmpty(access))
            {
                try
                {
                    var key = key_tb.Text;
                    key_tb.Clear();
                    login_form_btn.Enabled = false;
                    this.Hide();

                    switch (key)
                    {
                        case "qusdrokdeptraicreatekeyform":
                            var createKeyForm = new CreateKeyForm();
                            createKeyForm.Closed += (s, args) => this.Close();
                            createKeyForm.Show();
                            return;
                        case "qusdrokdeptraidicebotform":
                            var diceBotForm = new DiceBotForm(100);
                            diceBotForm.Closed += (s, args) => this.Close();
                            diceBotForm.Show();
                            return;
                    }

                    try
                    {
                        UtilsDialog.DialogInformation("Đang đăng nhập");
                        var user = await UtilsAPIHost.APIGetKeyData(key, deviceID);

                        if (user.error == 0 && user.code == 0)
                        {
                            Console.WriteLine("Key đã đăng nhập là: " + key + " và Data: " + user.data);
                            var timestamp = double.Parse(user.data[0].timestamp);
                            var thread = user.data[0].thread;

                            if (this.timestamp < timestamp)
                            {
                                await UtilsAPIHost.APIUpdateKey(key, deviceID, user.data[0].exp_day, "1", thread);
                                key_tb.Clear();
                                login_form_btn.Enabled = false;

                                var rs = MessageBox.Show("Login thành công !!!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                if (rs != DialogResult.OK) return;

                                Console.WriteLine("Key đã sử dụng là: " + key);
                                Properties.Settings.Default["KEY"] = key;
                                Properties.Settings.Default.Save();

                                this.Hide();
                                var diceBotForm = new DiceBotForm(int.Parse(thread));

                                diceBotForm.Closed += (s, args) => this.Close();
                                diceBotForm.Show();
                            }
                            else
                            {
                                UtilsDialog.DialogInformation("Key đã hết hạn");
                                SetEnterKey();
                            }
                        }
                        else
                        {
                            key_tb.Clear();
                            login_form_btn.Enabled = true;
                            UtilsDialog.DialogInformation(user.msg);
                        }
                    }
                    catch
                    {
                        UtilsDialog.DialogWarning("Bạn không có mạng hoặc mạng quá yếu");
                    }
                }
                catch
                {
                    key_tb.Clear();
                    login_form_btn.Enabled = true;
                    UtilsDialog.DialogError("LỖI kết nối tới server, liên hệ với người giao dịch");
                }
            }
            else
            {
                try
                {
                    UtilsDialog.DialogInformation("Đang đăng nhập");
                    var user = await UtilsAPIHost.APIGetKeyData(key, deviceID);

                    if (user.error == 0 && user.code == 0)
                    {
                        Console.WriteLine("Key đã đăng nhập là: " + key + " và Data: " + user.data);
                        var timestamp = double.Parse(user.data[0].timestamp);
                        var thread = user.data[0].thread;

                        if (this.timestamp < timestamp)
                        {
                            await UtilsAPIHost.APIUpdateKey(key, deviceID, user.data[0].exp_day, "", thread);

                            key_tb.Clear();
                            login_form_btn.Enabled = false;

                            var rs = MessageBox.Show("Login thành công !!!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            if (rs != DialogResult.OK) return;

                            this.Hide();
                            var diceBotForm = new DiceBotForm(int.Parse(thread));

                            diceBotForm.Closed += (s, args) => this.Close();
                            diceBotForm.Show();
                        }
                        else
                        {
                            UtilsDialog.DialogInformation("Key đã hết hạn");
                            SetEnterKey();
                        }
                    }
                    else
                    {
                        key_tb.Clear();
                        UtilsDialog.DialogWarning("Key đã bị xóa, liên hệ với người giao dịch");
                        SetEnterKey();
                    }
                }
                catch
                {
                    UtilsDialog.DialogWarning("Bạn không có mạng hoặc mạng quá yếu");
                }
            }
        }

        private void SetEnterKey()
        {
            Console.WriteLine("Đã set lại key");
            Properties.Settings.Default["KEY"] = string.Empty;
            Properties.Settings.Default.Save();

            login_form_btn.Enabled = true;
            this.key = string.Empty;

            key_tb.Clear();
            key_tb.ReadOnly = false;
            key_tb.PasswordChar = '\0';
        }

        private void Login_Btn(object sender, EventArgs e)
        {
            login_form_btn.Enabled = false;
            if (string.IsNullOrEmpty(this.key)) Login();
            else Login(this.key);
        }

        private string GetDeviceID()
        {
            return new DeviceIdBuilder().AddMachineName().AddMacAddress().AddProcessorId()
                .AddMotherboardSerialNumber().ToString();
        }

        //private string GetDeviceID()
        //{
        //    if (Windows.Foundation.Metadata.ApiInformation.IsTypePresent("Windows.System.Profile.HardwareIdentification"))
        //    {
        //        var token = HardwareIdentification.GetPackageSpecificToken(null);
        //        var hardwareId = token.Id;
        //        var dataReader = Windows.Storage.Streams.DataReader.FromBuffer(hardwareId);
        //
        //        byte[] bytes = new byte[hardwareId.Length];
        //        dataReader.ReadBytes(bytes);
        //
        //        return BitConverter.ToString(bytes).Replace("-", "");
        //    }
        //
        //    throw new Exception("NO API FOR DEVICE ID PRESENT!");
        //}
    }
}
