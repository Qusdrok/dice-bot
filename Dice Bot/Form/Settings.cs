﻿using Utility;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Dice_Bot.Event
{
    public static class Settings
    {
        #region TextBox, Label
        public static Label LBTotalWin { get; set; }
        public static Label LBTotalLose { get; set; }
        public static Label LBTotalBet { get; set; }

        public static string TBWithdrawAddress { get; set; }
        public static string TBWithdrawPassword { get; set; }

        public static decimal TBWithdrawAmount { get; set; }

        public static int NUDThread { get; set; }

        public static decimal TBTotalTarget { get; set; }

        public static int TBStopEachGameMin { get; set; }
        public static int TBStopEachGameMax { get; set; }

        public static int TBStopGameWinMin { get; set; }
        public static int TBStopGameWinMax { get; set; }

        public static int TBStopGameLoseMin { get; set; }
        public static int TBStopGameLoseMax { get; set; }

        public static int TBAfterWinStopGameMin { get; set; }
        public static int TBAfterWinStopGameMax { get; set; }

        public static int TBStopGameAfterWinMin { get; set; }
        public static int TBStopGameAfterWinMax { get; set; }

        public static int TBAfterLoseStopGameMin { get; set; }
        public static int TBAfterLoseStopGameMax { get; set; }

        public static int TBStopGameAfterLoseMin { get; set; }
        public static int TBStopGameAfterLoseMax { get; set; }

        public static int TBAfterWinStopThreadMin { get; set; }
        public static int TBAfterWinStopThreadMax { get; set; }

        public static int TBAfterLoseStopThreadMin { get; set; }
        public static int TBAfterLoseStopThreadMax { get; set; }

        public static decimal TBTargetWin { get; set; }
        public static decimal TBTargetLose { get; set; }

        public static decimal TBCashoutWinMax { get; set; }
        public static decimal TBCashoutWinMin { get; set; }

        public static decimal TBBetWinMax { get; set; }
        public static decimal TBBetWinMin { get; set; }

        public static decimal TBCashoutLoseMax { get; set; }
        public static decimal TBCashoutLoseMin { get; set; }

        public static decimal TBBetLoseMax { get; set; }
        public static decimal TBBetLoseMin { get; set; }

        public static decimal TBCashoutProfitWinLoseMax { get; set; }
        public static decimal TBCashoutProfitWinLoseMin { get; set; }

        public static decimal TBBetProfitWinLoseMax { get; set; }
        public static decimal TBBetProfitWinLoseMin { get; set; }
        #endregion
        #region Show Information Bet
        public static RichTextBox HistoryTB { get; set; }
        public static ToolStripStatusLabel StatusLB { get; set; }
        #endregion
        #region CheckBox, RadioButton
        public static bool CBStopEachGame { get; set; }
        public static bool RBNormal { get; set; }

        public static bool RBResetWin { get; set; }
        public static bool RBMultiplyWin { get; set; }

        public static bool RBResetLose { get; set; }
        public static bool RBMultiplyLose { get; set; }

        public static bool RBFloorAccumulationProfitWinLose { get; set; }
        public static decimal TBFloorAccumulationCashoutProfitWinLose { get; set; }
        public static decimal TBFloorAccumulationBetProfitWinLose { get; set; }
        #endregion
        #region ComboBox
        public static int CBCashoutWin { get; set; }
        public static int CBBetWin { get; set; }

        public static int CBCashoutLose { get; set; }
        public static int CBBetLose { get; set; }

        public static int CBCashoutProfitWinLose { get; set; }
        public static int CBBetProfitWinLose { get; set; }
        #endregion

        private static Random rnd = new Random();

        public static object OnStop(Bet bet, decimal betMin, decimal cashoutMin)
        {
            var win = rnd.Next(TBAfterWinStopThreadMin, TBAfterWinStopThreadMax);
            if (TBAfterWinStopThreadMax > 0 && bet.win > win) return $"Bạn đã thắng được {win} lần, chương trình đã dừng lại :33";

            var lose = rnd.Next(TBAfterLoseStopThreadMin, TBAfterLoseStopThreadMax);
            if (TBAfterLoseStopThreadMax > 0 && bet.lose > lose) return $"Bạn đã thua {lose} lần, chương trình đã dừng lại :((";

            var targetWin = bet.rootBalance * (TBTargetWin / 100);
            if (targetWin > 0 && bet.balance >= bet.rootBalance + targetWin)
                return $"Bạn đã thắng được {targetWin} coins, chương trình đã dừng lại :33";

            var targetLose = bet.rootBalance * (TBTargetLose / 100);
            if (targetLose > 0 && bet.balance >= bet.rootBalance + targetLose)
                return $"Bạn đã thua {targetLose} coins, chương trình đã dừng lại :((";

            if (CBStopEachGame && TBStopEachGameMax > 0)
            {
                bet = DoBet(bet, betMin, cashoutMin);
                return new KeyValuePair<int, Bet>(rnd.Next(TBStopEachGameMin, TBStopEachGameMax), bet);
            }

            if (TBAfterWinStopGameMax > 0 && rnd.Next(TBAfterWinStopGameMin, TBAfterWinStopGameMax) > bet.win)
            {
                bet = DoBet(bet, betMin, cashoutMin);
                return new KeyValuePair<int, Bet>(rnd.Next(TBStopGameAfterWinMin, TBStopGameAfterWinMax), bet);
            }

            if (TBAfterLoseStopGameMax > 0 && rnd.Next(TBAfterLoseStopGameMin, TBAfterLoseStopGameMax) > bet.lose)
            {
                bet = DoBet(bet, betMin, cashoutMin);
                return new KeyValuePair<int, Bet>(rnd.Next(TBStopGameAfterLoseMin, TBStopGameAfterLoseMax), bet);
            }

            return DoBet(bet, betMin, cashoutMin);
        }

        private static Bet DoBet(Bet bet, decimal betMin, decimal cashoutMin)
        {
            if (RBNormal)
            {
                if (bet.basic.result)
                {
                    if (RBResetWin)
                    {
                        bet.bet = betMin;
                        bet.cashout = cashoutMin;
                    }
                    else
                    {
                        var cashout = UtilsValue.RandomDecimal(TBCashoutWinMin, TBCashoutWinMax);
                        var betAmount = UtilsValue.RandomDecimal(TBBetWinMin, TBBetWinMax);

                        switch (CBCashoutWin)
                        {
                            case 0: // Multiply
                                bet.cashout *= cashout;
                                break;
                            case 1: // Add
                                bet.cashout += cashout;
                                break;
                            case 2: // Minus
                                bet.cashout -= cashout;
                                break;
                        }

                        switch (CBBetWin)
                        {
                            case 0: // Multiply
                                bet.bet *= betAmount;
                                break;
                            case 1: // Add
                                bet.bet += betAmount;
                                break;
                            case 2: // Minus
                                bet.bet -= betAmount;
                                break;
                        }
                    }
                }
                else
                {
                    if (RBResetLose)
                    {
                        bet.bet = betMin;
                        bet.cashout = cashoutMin;
                    }
                    else
                    {
                        var cashout = UtilsValue.RandomDecimal(TBCashoutLoseMin, TBCashoutLoseMax);
                        var betAmount = UtilsValue.RandomDecimal(TBBetLoseMin, TBBetLoseMax);

                        switch (CBCashoutLose)
                        {
                            case 0: // Multiply
                                bet.cashout *= cashout;
                                break;
                            case 1: // Add
                                bet.cashout += cashout;
                                break;
                            case 2: // Minus
                                bet.cashout -= cashout;
                                break;
                            default:
                                bet.cashout = cashoutMin;
                                break;
                        }

                        switch (CBBetLose)
                        {
                            case 0: // Multiply
                                bet.bet *= betAmount;
                                break;
                            case 1: // Add
                                bet.bet += betAmount;
                                break;
                            case 2: // Minus
                                bet.bet -= betAmount;
                                break;
                            default:
                                bet.bet = betMin;
                                break;
                        }
                    }
                }
            }
            else
            {
                if (bet.profitWin < bet.profitLose)
                {
                    var cashout = UtilsValue.RandomDecimal(TBCashoutProfitWinLoseMin, TBCashoutProfitWinLoseMax);
                    var betAmount = UtilsValue.RandomDecimal(TBBetProfitWinLoseMin, TBBetProfitWinLoseMax);

                    switch (CBCashoutProfitWinLose)
                    {
                        case 0: // Multiply
                            if (RBFloorAccumulationProfitWinLose)
                            {
                                TBFloorAccumulationCashoutProfitWinLose += cashout;
                                bet.cashout *= TBFloorAccumulationCashoutProfitWinLose;
                            }
                            else
                            {
                                bet.cashout *= cashout;
                            }
                            break;
                        case 1: // Add
                            if (RBFloorAccumulationProfitWinLose)
                            {
                                TBFloorAccumulationCashoutProfitWinLose += cashout;
                                bet.cashout += TBFloorAccumulationCashoutProfitWinLose;
                            }
                            else
                            {
                                bet.cashout += cashout;
                            }
                            break;
                        case 2: // Minus
                            if (RBFloorAccumulationProfitWinLose)
                            {
                                TBFloorAccumulationCashoutProfitWinLose += cashout;
                                bet.cashout -= TBFloorAccumulationCashoutProfitWinLose;
                            }
                            else
                            {
                                bet.cashout -= cashout;
                            }
                            break;
                        default:
                            bet.cashout = cashoutMin;
                            break;
                    }

                    switch (CBBetProfitWinLose)
                    {
                        case 0: // Multiply
                            if (RBFloorAccumulationProfitWinLose)
                            {
                                TBFloorAccumulationBetProfitWinLose += betAmount;
                                bet.bet *= TBFloorAccumulationBetProfitWinLose;
                            }
                            else
                            {
                                bet.bet *= betAmount;
                            }
                            break;
                        case 1: // Add
                            if (RBFloorAccumulationProfitWinLose)
                            {
                                TBFloorAccumulationBetProfitWinLose += betAmount;
                                bet.bet += TBFloorAccumulationBetProfitWinLose;
                            }
                            else
                            {
                                bet.bet += betAmount;
                            }
                            break;
                        case 2: // Minus
                            if (RBFloorAccumulationProfitWinLose)
                            {
                                TBFloorAccumulationBetProfitWinLose += betAmount;
                                bet.bet -= TBFloorAccumulationBetProfitWinLose;
                            }
                            else
                            {
                                bet.bet -= betAmount;
                            }
                            break;
                        default:
                            bet.bet = betMin;
                            break;
                    }
                }
                else
                {
                    TBFloorAccumulationCashoutProfitWinLose = 0m;
                    TBFloorAccumulationBetProfitWinLose = 0m;
                }
            }

            return bet;
        }
    }
}
