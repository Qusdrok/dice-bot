﻿namespace Dice_Bot
{
    partial class DiceBotForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 =
                new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 =
                new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 =
                new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources =
                new System.ComponentModel.ComponentResourceManager(typeof(DiceBotForm));
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.total_win_lb = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.total_lose_lb = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.total_bet_lb = new System.Windows.Forms.Label();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage = new System.Windows.Forms.TabPage();
            this.chart_panel = new System.Windows.Forms.Panel();
            this.chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.data_grid_view_panel = new System.Windows.Forms.Panel();
            this.data_grid_view = new System.Windows.Forms.DataGridView();
            this.setting_tabcontrol = new System.Windows.Forms.TabControl();
            this.advanced_tabpage = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.bet_low_rb = new System.Windows.Forms.RadioButton();
            this.bet_high_rb = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.profit_win_lose_rb = new System.Windows.Forms.RadioButton();
            this.normal_rb = new System.Windows.Forms.RadioButton();
            this.gb_on_win = new System.Windows.Forms.GroupBox();
            this.multiply_win_rb = new System.Windows.Forms.RadioButton();
            this.gb_bet_win = new System.Windows.Forms.GroupBox();
            this.bet_win_cb = new System.Windows.Forms.ComboBox();
            this.bet_win_min_tb = new System.Windows.Forms.TextBox();
            this.bet_win_max_tb = new System.Windows.Forms.TextBox();
            this.gb_cashout_win = new System.Windows.Forms.GroupBox();
            this.cashout_win_min_tb = new System.Windows.Forms.TextBox();
            this.cashout_win_max_tb = new System.Windows.Forms.TextBox();
            this.cashout_win_cb = new System.Windows.Forms.ComboBox();
            this.reset_win_rb = new System.Windows.Forms.RadioButton();
            this.gb_profit_win_lose = new System.Windows.Forms.GroupBox();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.bet_profit_win_lose_cb = new System.Windows.Forms.ComboBox();
            this.bet_profit_win_lose_min_tb = new System.Windows.Forms.TextBox();
            this.bet_profit_win_lose_max_tb = new System.Windows.Forms.TextBox();
            this.floor_accumulation_profit_win_lose_rb = new System.Windows.Forms.RadioButton();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.cashout_profit_win_lose_cb = new System.Windows.Forms.ComboBox();
            this.cashout_profit_win_lose_min_tb = new System.Windows.Forms.TextBox();
            this.cashout_profit_win_lose_max_tb = new System.Windows.Forms.TextBox();
            this.gb_on_lose = new System.Windows.Forms.GroupBox();
            this.multiply_lose_rb = new System.Windows.Forms.RadioButton();
            this.gb_bet_lose = new System.Windows.Forms.GroupBox();
            this.bet_lose_cb = new System.Windows.Forms.ComboBox();
            this.bet_lose_min_tb = new System.Windows.Forms.TextBox();
            this.bet_lose_max_tb = new System.Windows.Forms.TextBox();
            this.gb_cashout_lose = new System.Windows.Forms.GroupBox();
            this.cashout_lose_min_tb = new System.Windows.Forms.TextBox();
            this.cashout_lose_max_tb = new System.Windows.Forms.TextBox();
            this.cashout_lose_cb = new System.Windows.Forms.ComboBox();
            this.reset_lose_rb = new System.Windows.Forms.RadioButton();
            this.grp_stop_game = new System.Windows.Forms.GroupBox();
            this.stop_game_win_min_tb = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.stop_game_win_max_tb = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.stop_game_lose_min_tb = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.stop_game_lose_max_tb = new System.Windows.Forms.TextBox();
            this.stop_each_game_checkbox = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.target_lose_tb = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.target_win_tb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.after_lose_stop_thread_max_tb = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.after_lose_stop_thread_min_tb = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.after_win_stop_thread_max_tb = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.after_win_stop_thread_min_tb = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.stop_game_after_lose_max_tb = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.stop_game_after_lose_min_tb = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.after_lose_stop_game_max_tb = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.after_lose_stop_game_min_tb = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.stop_game_after_win_max_tb = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.stop_game_after_win_min_tb = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.after_win_stop_game_max_tb = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.after_win_stop_game_min_tb = new System.Windows.Forms.TextBox();
            this.stop_each_game_max_tb = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.stop_each_game_min_tb = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.game_tabpage = new System.Windows.Forms.TabPage();
            this.gb_game_panel = new System.Windows.Forms.Panel();
            this.gb_game = new System.Windows.Forms.GroupBox();
            this.gb_cashout_max_tb = new System.Windows.Forms.TextBox();
            this.gb_cashout_min_tb = new System.Windows.Forms.TextBox();
            this.gb_bet_max_tb = new System.Windows.Forms.TextBox();
            this.gb_bet_min_tb = new System.Windows.Forms.TextBox();
            this.gb_coin_cb = new System.Windows.Forms.ComboBox();
            this.gb_game_cb = new System.Windows.Forms.ComboBox();
            this.programmer_tabpage = new System.Windows.Forms.TabPage();
            this.programmer_fctb = new FastColoredTextBoxNS.FastColoredTextBox();
            this.programmer_rb = new System.Windows.Forms.RadioButton();
            this.history_tabpage = new System.Windows.Forms.TabPage();
            this.history_tb = new System.Windows.Forms.RichTextBox();
            this.variables_tabpage = new System.Windows.Forms.TabPage();
            this.variables_tb = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.gb_proxy = new System.Windows.Forms.GroupBox();
            this.default_proxy_rb = new System.Windows.Forms.RadioButton();
            this.label33 = new System.Windows.Forms.Label();
            this.proxy_rb = new System.Windows.Forms.RadioButton();
            this.add_proxy_file_btn = new System.Windows.Forms.Button();
            this.proxy_tb = new System.Windows.Forms.TextBox();
            this.gp_basic_stat = new System.Windows.Forms.GroupBox();
            this.withdraw_amount_tb = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.withdraw_password_tb = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.withdraw_btn = new System.Windows.Forms.Button();
            this.withdraw_address_tb = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.gb_setting = new System.Windows.Forms.GroupBox();
            this.browser_cb = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.thread_nud = new System.Windows.Forms.NumericUpDown();
            this.website_cb = new System.Windows.Forms.ComboBox();
            this.deposit_cb = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.gb_login = new System.Windows.Forms.GroupBox();
            this.stop_btn = new System.Windows.Forms.Button();
            this.start_btn = new System.Windows.Forms.Button();
            this.account_tb = new System.Windows.Forms.RichTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.tool_strip_status_lb = new System.Windows.Forms.ToolStripStatusLabel();
            this.tableLayoutPanel3.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPage.SuspendLayout();
            this.chart_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.chart)).BeginInit();
            this.data_grid_view_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.data_grid_view)).BeginInit();
            this.setting_tabcontrol.SuspendLayout();
            this.advanced_tabpage.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.gb_on_win.SuspendLayout();
            this.gb_bet_win.SuspendLayout();
            this.gb_cashout_win.SuspendLayout();
            this.gb_profit_win_lose.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.gb_on_lose.SuspendLayout();
            this.gb_bet_lose.SuspendLayout();
            this.gb_cashout_lose.SuspendLayout();
            this.grp_stop_game.SuspendLayout();
            this.game_tabpage.SuspendLayout();
            this.gb_game_panel.SuspendLayout();
            this.gb_game.SuspendLayout();
            this.programmer_tabpage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.programmer_fctb)).BeginInit();
            this.history_tabpage.SuspendLayout();
            this.variables_tabpage.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.gb_proxy.SuspendLayout();
            this.gp_basic_stat.SuspendLayout();
            this.gb_setting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.thread_nud)).BeginInit();
            this.gb_login.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(
                new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(
                new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.total_win_lb, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label8, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.total_lose_lb, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label10, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.total_bet_lb, 1, 2);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(7, 22);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel3.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel3.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(325, 103);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label7.ForeColor = System.Drawing.Color.Green;
            this.label7.Location = new System.Drawing.Point(3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 20);
            this.label7.TabIndex = 3;
            this.label7.Text = "Win:";
            // 
            // total_win_lb
            // 
            this.total_win_lb.AutoSize = true;
            this.total_win_lb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.total_win_lb.ForeColor = System.Drawing.Color.Green;
            this.total_win_lb.Location = new System.Drawing.Point(165, 0);
            this.total_win_lb.Name = "total_win_lb";
            this.total_win_lb.Size = new System.Drawing.Size(19, 20);
            this.total_win_lb.TabIndex = 2;
            this.total_win_lb.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(3, 35);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 20);
            this.label8.TabIndex = 4;
            this.label8.Text = "Lose:";
            // 
            // total_lose_lb
            // 
            this.total_lose_lb.AutoSize = true;
            this.total_lose_lb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F,
                System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.total_lose_lb.ForeColor = System.Drawing.Color.Red;
            this.total_lose_lb.Location = new System.Drawing.Point(165, 35);
            this.total_lose_lb.Name = "total_lose_lb";
            this.total_lose_lb.Size = new System.Drawing.Size(19, 20);
            this.total_lose_lb.TabIndex = 5;
            this.total_lose_lb.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label10.Location = new System.Drawing.Point(3, 70);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(96, 20);
            this.label10.TabIndex = 6;
            this.label10.Text = "Total Bets:";
            // 
            // total_bet_lb
            // 
            this.total_bet_lb.AutoSize = true;
            this.total_bet_lb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.total_bet_lb.Location = new System.Drawing.Point(165, 70);
            this.total_bet_lb.Name = "total_bet_lb";
            this.total_bet_lb.Size = new System.Drawing.Size(19, 20);
            this.total_bet_lb.TabIndex = 7;
            this.total_bet_lb.Text = "0";
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage);
            this.tabControl.Location = new System.Drawing.Point(14, 14);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1554, 798);
            this.tabControl.TabIndex = 26;
            // 
            // tabPage
            // 
            this.tabPage.Controls.Add(this.chart_panel);
            this.tabPage.Controls.Add(this.data_grid_view_panel);
            this.tabPage.Controls.Add(this.setting_tabcontrol);
            this.tabPage.Controls.Add(this.tableLayoutPanel1);
            this.tabPage.Location = new System.Drawing.Point(4, 24);
            this.tabPage.Name = "tabPage";
            this.tabPage.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage.Size = new System.Drawing.Size(1546, 770);
            this.tabPage.TabIndex = 1;
            this.tabPage.Text = "Game";
            this.tabPage.UseVisualStyleBackColor = true;
            // 
            // chart_panel
            // 
            this.chart_panel.AutoScroll = true;
            this.chart_panel.Controls.Add(this.chart);
            this.chart_panel.Location = new System.Drawing.Point(3, 507);
            this.chart_panel.Name = "chart_panel";
            this.chart_panel.Size = new System.Drawing.Size(1046, 257);
            this.chart_panel.TabIndex = 30;
            // 
            // chart
            // 
            this.chart.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            chartArea1.Name = "ChartArea1";
            this.chart.ChartAreas.Add(chartArea1);
            this.chart.Enabled = false;
            this.chart.IsSoftShadows = false;
            legend1.BorderWidth = 2;
            legend1.Name = "Legend1";
            this.chart.Legends.Add(legend1);
            this.chart.Location = new System.Drawing.Point(3, 3);
            this.chart.Name = "chart";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series1.IsVisibleInLegend = false;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart.Series.Add(series1);
            this.chart.Size = new System.Drawing.Size(1038, 247);
            this.chart.TabIndex = 0;
            this.chart.Text = "chart1";
            // 
            // data_grid_view_panel
            // 
            this.data_grid_view_panel.AutoScroll = true;
            this.data_grid_view_panel.Controls.Add(this.data_grid_view);
            this.data_grid_view_panel.Location = new System.Drawing.Point(3, 268);
            this.data_grid_view_panel.Name = "data_grid_view_panel";
            this.data_grid_view_panel.Size = new System.Drawing.Size(1046, 235);
            this.data_grid_view_panel.TabIndex = 29;
            // 
            // data_grid_view
            // 
            this.data_grid_view.AllowUserToAddRows = false;
            this.data_grid_view.ColumnHeadersHeightSizeMode =
                System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.data_grid_view.Enabled = false;
            this.data_grid_view.Location = new System.Drawing.Point(5, 5);
            this.data_grid_view.Name = "data_grid_view";
            this.data_grid_view.ReadOnly = true;
            this.data_grid_view.Size = new System.Drawing.Size(1037, 227);
            this.data_grid_view.TabIndex = 0;
            // 
            // setting_tabcontrol
            // 
            this.setting_tabcontrol.Controls.Add(this.advanced_tabpage);
            this.setting_tabcontrol.Controls.Add(this.game_tabpage);
            this.setting_tabcontrol.Controls.Add(this.programmer_tabpage);
            this.setting_tabcontrol.Controls.Add(this.history_tabpage);
            this.setting_tabcontrol.Controls.Add(this.variables_tabpage);
            this.setting_tabcontrol.Location = new System.Drawing.Point(1052, 3);
            this.setting_tabcontrol.Multiline = true;
            this.setting_tabcontrol.Name = "setting_tabcontrol";
            this.setting_tabcontrol.SelectedIndex = 0;
            this.setting_tabcontrol.Size = new System.Drawing.Size(492, 765);
            this.setting_tabcontrol.TabIndex = 28;
            // 
            // advanced_tabpage
            // 
            this.advanced_tabpage.AutoScroll = true;
            this.advanced_tabpage.Controls.Add(this.groupBox3);
            this.advanced_tabpage.Controls.Add(this.groupBox1);
            this.advanced_tabpage.Location = new System.Drawing.Point(4, 24);
            this.advanced_tabpage.Name = "advanced_tabpage";
            this.advanced_tabpage.Padding = new System.Windows.Forms.Padding(3);
            this.advanced_tabpage.Size = new System.Drawing.Size(484, 737);
            this.advanced_tabpage.TabIndex = 2;
            this.advanced_tabpage.Text = "Advanced";
            this.advanced_tabpage.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.bet_low_rb);
            this.groupBox3.Controls.Add(this.bet_high_rb);
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(457, 58);
            this.groupBox3.TabIndex = 29;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Choose bet type";
            // 
            // bet_low_rb
            // 
            this.bet_low_rb.AutoSize = true;
            this.bet_low_rb.Location = new System.Drawing.Point(275, 25);
            this.bet_low_rb.Name = "bet_low_rb";
            this.bet_low_rb.Size = new System.Drawing.Size(67, 19);
            this.bet_low_rb.TabIndex = 1;
            this.bet_low_rb.TabStop = true;
            this.bet_low_rb.Text = "Bet Low";
            this.bet_low_rb.UseVisualStyleBackColor = true;
            // 
            // bet_high_rb
            // 
            this.bet_high_rb.AutoSize = true;
            this.bet_high_rb.Checked = true;
            this.bet_high_rb.Location = new System.Drawing.Point(125, 25);
            this.bet_high_rb.Name = "bet_high_rb";
            this.bet_high_rb.Size = new System.Drawing.Size(71, 19);
            this.bet_high_rb.TabIndex = 0;
            this.bet_high_rb.TabStop = true;
            this.bet_high_rb.Text = "Bet High";
            this.bet_high_rb.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.grp_stop_game);
            this.groupBox1.Controls.Add(this.stop_each_game_checkbox);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.target_lose_tb);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.target_win_tb);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label48);
            this.groupBox1.Controls.Add(this.after_lose_stop_thread_max_tb);
            this.groupBox1.Controls.Add(this.label49);
            this.groupBox1.Controls.Add(this.after_lose_stop_thread_min_tb);
            this.groupBox1.Controls.Add(this.label50);
            this.groupBox1.Controls.Add(this.label47);
            this.groupBox1.Controls.Add(this.after_win_stop_thread_max_tb);
            this.groupBox1.Controls.Add(this.label45);
            this.groupBox1.Controls.Add(this.after_win_stop_thread_min_tb);
            this.groupBox1.Controls.Add(this.label46);
            this.groupBox1.Controls.Add(this.stop_game_after_lose_max_tb);
            this.groupBox1.Controls.Add(this.label41);
            this.groupBox1.Controls.Add(this.stop_game_after_lose_min_tb);
            this.groupBox1.Controls.Add(this.label42);
            this.groupBox1.Controls.Add(this.after_lose_stop_game_max_tb);
            this.groupBox1.Controls.Add(this.label43);
            this.groupBox1.Controls.Add(this.after_lose_stop_game_min_tb);
            this.groupBox1.Controls.Add(this.label44);
            this.groupBox1.Controls.Add(this.stop_game_after_win_max_tb);
            this.groupBox1.Controls.Add(this.label40);
            this.groupBox1.Controls.Add(this.stop_game_after_win_min_tb);
            this.groupBox1.Controls.Add(this.label39);
            this.groupBox1.Controls.Add(this.after_win_stop_game_max_tb);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.after_win_stop_game_min_tb);
            this.groupBox1.Controls.Add(this.stop_each_game_max_tb);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.stop_each_game_min_tb);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Location = new System.Drawing.Point(3, 61);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(457, 879);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Condition Stop and Other";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.profit_win_lose_rb);
            this.groupBox2.Controls.Add(this.normal_rb);
            this.groupBox2.Controls.Add(this.gb_on_win);
            this.groupBox2.Controls.Add(this.gb_profit_win_lose);
            this.groupBox2.Controls.Add(this.gb_on_lose);
            this.groupBox2.Location = new System.Drawing.Point(7, 320);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(443, 466);
            this.groupBox2.TabIndex = 183;
            this.groupBox2.TabStop = false;
            // 
            // profit_win_lose_rb
            // 
            this.profit_win_lose_rb.AutoSize = true;
            this.profit_win_lose_rb.Location = new System.Drawing.Point(7, 286);
            this.profit_win_lose_rb.Name = "profit_win_lose_rb";
            this.profit_win_lose_rb.Size = new System.Drawing.Size(143, 19);
            this.profit_win_lose_rb.TabIndex = 183;
            this.profit_win_lose_rb.TabStop = true;
            this.profit_win_lose_rb.Text = "Profit win < profit lose";
            this.profit_win_lose_rb.UseVisualStyleBackColor = true;
            this.profit_win_lose_rb.CheckedChanged += new System.EventHandler(this.profit_win_lose_rb_checked_changed);
            // 
            // normal_rb
            // 
            this.normal_rb.AutoSize = true;
            this.normal_rb.Checked = true;
            this.normal_rb.Location = new System.Drawing.Point(7, 17);
            this.normal_rb.Name = "normal_rb";
            this.normal_rb.Size = new System.Drawing.Size(65, 19);
            this.normal_rb.TabIndex = 182;
            this.normal_rb.TabStop = true;
            this.normal_rb.Text = "Normal";
            this.normal_rb.UseVisualStyleBackColor = true;
            this.normal_rb.CheckedChanged += new System.EventHandler(this.normal_rb_checked_changed);
            // 
            // gb_on_win
            // 
            this.gb_on_win.Controls.Add(this.multiply_win_rb);
            this.gb_on_win.Controls.Add(this.gb_bet_win);
            this.gb_on_win.Controls.Add(this.gb_cashout_win);
            this.gb_on_win.Controls.Add(this.reset_win_rb);
            this.gb_on_win.Location = new System.Drawing.Point(6, 44);
            this.gb_on_win.Name = "gb_on_win";
            this.gb_on_win.Size = new System.Drawing.Size(203, 235);
            this.gb_on_win.TabIndex = 180;
            this.gb_on_win.TabStop = false;
            this.gb_on_win.Text = "On Win";
            // 
            // multiply_win_rb
            // 
            this.multiply_win_rb.AutoSize = true;
            this.multiply_win_rb.Location = new System.Drawing.Point(76, 22);
            this.multiply_win_rb.Name = "multiply_win_rb";
            this.multiply_win_rb.Size = new System.Drawing.Size(69, 19);
            this.multiply_win_rb.TabIndex = 181;
            this.multiply_win_rb.TabStop = true;
            this.multiply_win_rb.Text = "Multiply";
            this.multiply_win_rb.UseVisualStyleBackColor = true;
            this.multiply_win_rb.CheckedChanged += new System.EventHandler(this.multiply_win_rb_checked_changed);
            // 
            // gb_bet_win
            // 
            this.gb_bet_win.Controls.Add(this.bet_win_cb);
            this.gb_bet_win.Controls.Add(this.bet_win_min_tb);
            this.gb_bet_win.Controls.Add(this.bet_win_max_tb);
            this.gb_bet_win.Enabled = false;
            this.gb_bet_win.Location = new System.Drawing.Point(7, 141);
            this.gb_bet_win.Name = "gb_bet_win";
            this.gb_bet_win.Size = new System.Drawing.Size(188, 85);
            this.gb_bet_win.TabIndex = 180;
            this.gb_bet_win.TabStop = false;
            this.gb_bet_win.Text = "Bet";
            // 
            // bet_win_cb
            // 
            this.bet_win_cb.FormattingEnabled = true;
            this.bet_win_cb.Items.AddRange(new object[] {"Multiply Bet", "Add Bet", "Minus Bet", "None"});
            this.bet_win_cb.Location = new System.Drawing.Point(7, 22);
            this.bet_win_cb.Name = "bet_win_cb";
            this.bet_win_cb.Size = new System.Drawing.Size(168, 23);
            this.bet_win_cb.TabIndex = 179;
            // 
            // bet_win_min_tb
            // 
            this.bet_win_min_tb.Location = new System.Drawing.Point(7, 53);
            this.bet_win_min_tb.Name = "bet_win_min_tb";
            this.bet_win_min_tb.Size = new System.Drawing.Size(76, 23);
            this.bet_win_min_tb.TabIndex = 177;
            this.bet_win_min_tb.Text = "0.0000001";
            // 
            // bet_win_max_tb
            // 
            this.bet_win_max_tb.Location = new System.Drawing.Point(104, 53);
            this.bet_win_max_tb.Name = "bet_win_max_tb";
            this.bet_win_max_tb.Size = new System.Drawing.Size(73, 23);
            this.bet_win_max_tb.TabIndex = 178;
            this.bet_win_max_tb.Text = "0.0000001";
            // 
            // gb_cashout_win
            // 
            this.gb_cashout_win.Controls.Add(this.cashout_win_min_tb);
            this.gb_cashout_win.Controls.Add(this.cashout_win_max_tb);
            this.gb_cashout_win.Controls.Add(this.cashout_win_cb);
            this.gb_cashout_win.Enabled = false;
            this.gb_cashout_win.Location = new System.Drawing.Point(7, 48);
            this.gb_cashout_win.Name = "gb_cashout_win";
            this.gb_cashout_win.Size = new System.Drawing.Size(187, 85);
            this.gb_cashout_win.TabIndex = 180;
            this.gb_cashout_win.TabStop = false;
            this.gb_cashout_win.Text = "Cashout";
            // 
            // cashout_win_min_tb
            // 
            this.cashout_win_min_tb.Location = new System.Drawing.Point(7, 53);
            this.cashout_win_min_tb.Name = "cashout_win_min_tb";
            this.cashout_win_min_tb.Size = new System.Drawing.Size(76, 23);
            this.cashout_win_min_tb.TabIndex = 182;
            this.cashout_win_min_tb.Text = "1.1";
            // 
            // cashout_win_max_tb
            // 
            this.cashout_win_max_tb.Location = new System.Drawing.Point(101, 53);
            this.cashout_win_max_tb.Name = "cashout_win_max_tb";
            this.cashout_win_max_tb.Size = new System.Drawing.Size(73, 23);
            this.cashout_win_max_tb.TabIndex = 183;
            this.cashout_win_max_tb.Text = "1.1";
            // 
            // cashout_win_cb
            // 
            this.cashout_win_cb.FormattingEnabled = true;
            this.cashout_win_cb.Items.AddRange(
                new object[] {"Multiply Cashout", "Add Cashout", "Minus Cashout", "None"});
            this.cashout_win_cb.Location = new System.Drawing.Point(7, 22);
            this.cashout_win_cb.Name = "cashout_win_cb";
            this.cashout_win_cb.Size = new System.Drawing.Size(168, 23);
            this.cashout_win_cb.TabIndex = 181;
            // 
            // reset_win_rb
            // 
            this.reset_win_rb.AutoSize = true;
            this.reset_win_rb.Checked = true;
            this.reset_win_rb.Location = new System.Drawing.Point(7, 22);
            this.reset_win_rb.Name = "reset_win_rb";
            this.reset_win_rb.Size = new System.Drawing.Size(53, 19);
            this.reset_win_rb.TabIndex = 170;
            this.reset_win_rb.TabStop = true;
            this.reset_win_rb.Text = "Reset";
            this.reset_win_rb.UseVisualStyleBackColor = true;
            this.reset_win_rb.CheckedChanged += new System.EventHandler(this.reset_win_rb_checked_changed);
            // 
            // gb_profit_win_lose
            // 
            this.gb_profit_win_lose.Controls.Add(this.groupBox20);
            this.gb_profit_win_lose.Controls.Add(this.floor_accumulation_profit_win_lose_rb);
            this.gb_profit_win_lose.Controls.Add(this.groupBox21);
            this.gb_profit_win_lose.Enabled = false;
            this.gb_profit_win_lose.Location = new System.Drawing.Point(7, 313);
            this.gb_profit_win_lose.Name = "gb_profit_win_lose";
            this.gb_profit_win_lose.Size = new System.Drawing.Size(394, 143);
            this.gb_profit_win_lose.TabIndex = 182;
            this.gb_profit_win_lose.TabStop = false;
            this.gb_profit_win_lose.Text = "Profit win < profit lose";
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.bet_profit_win_lose_cb);
            this.groupBox20.Controls.Add(this.bet_profit_win_lose_min_tb);
            this.groupBox20.Controls.Add(this.bet_profit_win_lose_max_tb);
            this.groupBox20.Location = new System.Drawing.Point(201, 48);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(185, 87);
            this.groupBox20.TabIndex = 159;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Bet";
            // 
            // bet_profit_win_lose_cb
            // 
            this.bet_profit_win_lose_cb.FormattingEnabled = true;
            this.bet_profit_win_lose_cb.Items.AddRange(new object[] {"Multiply Bet", "Add Bet", "Minus Bet", "None"});
            this.bet_profit_win_lose_cb.Location = new System.Drawing.Point(7, 22);
            this.bet_profit_win_lose_cb.Name = "bet_profit_win_lose_cb";
            this.bet_profit_win_lose_cb.Size = new System.Drawing.Size(168, 23);
            this.bet_profit_win_lose_cb.TabIndex = 156;
            // 
            // bet_profit_win_lose_min_tb
            // 
            this.bet_profit_win_lose_min_tb.Location = new System.Drawing.Point(7, 53);
            this.bet_profit_win_lose_min_tb.Name = "bet_profit_win_lose_min_tb";
            this.bet_profit_win_lose_min_tb.Size = new System.Drawing.Size(76, 23);
            this.bet_profit_win_lose_min_tb.TabIndex = 154;
            this.bet_profit_win_lose_min_tb.Text = "0.0000001";
            // 
            // bet_profit_win_lose_max_tb
            // 
            this.bet_profit_win_lose_max_tb.Location = new System.Drawing.Point(103, 53);
            this.bet_profit_win_lose_max_tb.Name = "bet_profit_win_lose_max_tb";
            this.bet_profit_win_lose_max_tb.Size = new System.Drawing.Size(73, 23);
            this.bet_profit_win_lose_max_tb.TabIndex = 155;
            this.bet_profit_win_lose_max_tb.Text = "0.0000001";
            // 
            // floor_accumulation_profit_win_lose_rb
            // 
            this.floor_accumulation_profit_win_lose_rb.AutoSize = true;
            this.floor_accumulation_profit_win_lose_rb.Location = new System.Drawing.Point(7, 22);
            this.floor_accumulation_profit_win_lose_rb.Name = "floor_accumulation_profit_win_lose_rb";
            this.floor_accumulation_profit_win_lose_rb.Size = new System.Drawing.Size(130, 19);
            this.floor_accumulation_profit_win_lose_rb.TabIndex = 0;
            this.floor_accumulation_profit_win_lose_rb.TabStop = true;
            this.floor_accumulation_profit_win_lose_rb.Text = "Floor Accumulation";
            this.toolTip.SetToolTip(this.floor_accumulation_profit_win_lose_rb,
                "VD: Các game tiếp theo, tăng bet 0.1 sau mỗi game, game 1 là base bet + 0.1, game" +
                " 2 là base bet + 0.1 * 2, game 3 là base bet + 0.1 *3, ....");
            this.floor_accumulation_profit_win_lose_rb.UseVisualStyleBackColor = true;
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.cashout_profit_win_lose_cb);
            this.groupBox21.Controls.Add(this.cashout_profit_win_lose_min_tb);
            this.groupBox21.Controls.Add(this.cashout_profit_win_lose_max_tb);
            this.groupBox21.Location = new System.Drawing.Point(7, 48);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(187, 87);
            this.groupBox21.TabIndex = 158;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Cashout";
            // 
            // cashout_profit_win_lose_cb
            // 
            this.cashout_profit_win_lose_cb.FormattingEnabled = true;
            this.cashout_profit_win_lose_cb.Items.AddRange(new object[]
                {"Multiply Cashout", "Add Cashout", "Minus Cashout", "None"});
            this.cashout_profit_win_lose_cb.Location = new System.Drawing.Point(8, 21);
            this.cashout_profit_win_lose_cb.Name = "cashout_profit_win_lose_cb";
            this.cashout_profit_win_lose_cb.Size = new System.Drawing.Size(168, 23);
            this.cashout_profit_win_lose_cb.TabIndex = 153;
            // 
            // cashout_profit_win_lose_min_tb
            // 
            this.cashout_profit_win_lose_min_tb.Location = new System.Drawing.Point(8, 52);
            this.cashout_profit_win_lose_min_tb.Name = "cashout_profit_win_lose_min_tb";
            this.cashout_profit_win_lose_min_tb.Size = new System.Drawing.Size(76, 23);
            this.cashout_profit_win_lose_min_tb.TabIndex = 146;
            this.cashout_profit_win_lose_min_tb.Text = "1.1";
            // 
            // cashout_profit_win_lose_max_tb
            // 
            this.cashout_profit_win_lose_max_tb.Location = new System.Drawing.Point(104, 52);
            this.cashout_profit_win_lose_max_tb.Name = "cashout_profit_win_lose_max_tb";
            this.cashout_profit_win_lose_max_tb.Size = new System.Drawing.Size(73, 23);
            this.cashout_profit_win_lose_max_tb.TabIndex = 149;
            this.cashout_profit_win_lose_max_tb.Text = "1.1";
            // 
            // gb_on_lose
            // 
            this.gb_on_lose.Controls.Add(this.multiply_lose_rb);
            this.gb_on_lose.Controls.Add(this.gb_bet_lose);
            this.gb_on_lose.Controls.Add(this.gb_cashout_lose);
            this.gb_on_lose.Controls.Add(this.reset_lose_rb);
            this.gb_on_lose.Location = new System.Drawing.Point(216, 44);
            this.gb_on_lose.Name = "gb_on_lose";
            this.gb_on_lose.Size = new System.Drawing.Size(205, 237);
            this.gb_on_lose.TabIndex = 181;
            this.gb_on_lose.TabStop = false;
            this.gb_on_lose.Text = "On Lose";
            // 
            // multiply_lose_rb
            // 
            this.multiply_lose_rb.AutoSize = true;
            this.multiply_lose_rb.Location = new System.Drawing.Point(76, 22);
            this.multiply_lose_rb.Name = "multiply_lose_rb";
            this.multiply_lose_rb.Size = new System.Drawing.Size(69, 19);
            this.multiply_lose_rb.TabIndex = 182;
            this.multiply_lose_rb.TabStop = true;
            this.multiply_lose_rb.Text = "Multiply";
            this.multiply_lose_rb.UseVisualStyleBackColor = true;
            this.multiply_lose_rb.CheckedChanged += new System.EventHandler(this.multiply_lose_rb_checked_changed);
            // 
            // gb_bet_lose
            // 
            this.gb_bet_lose.Controls.Add(this.bet_lose_cb);
            this.gb_bet_lose.Controls.Add(this.bet_lose_min_tb);
            this.gb_bet_lose.Controls.Add(this.bet_lose_max_tb);
            this.gb_bet_lose.Enabled = false;
            this.gb_bet_lose.Location = new System.Drawing.Point(6, 142);
            this.gb_bet_lose.Name = "gb_bet_lose";
            this.gb_bet_lose.Size = new System.Drawing.Size(192, 87);
            this.gb_bet_lose.TabIndex = 180;
            this.gb_bet_lose.TabStop = false;
            this.gb_bet_lose.Text = "Bet";
            // 
            // bet_lose_cb
            // 
            this.bet_lose_cb.FormattingEnabled = true;
            this.bet_lose_cb.Items.AddRange(new object[] {"Multiply Bet", "Add Bet", "Minus Bet", "None"});
            this.bet_lose_cb.Location = new System.Drawing.Point(7, 22);
            this.bet_lose_cb.Name = "bet_lose_cb";
            this.bet_lose_cb.Size = new System.Drawing.Size(168, 23);
            this.bet_lose_cb.TabIndex = 176;
            // 
            // bet_lose_min_tb
            // 
            this.bet_lose_min_tb.Location = new System.Drawing.Point(7, 53);
            this.bet_lose_min_tb.Name = "bet_lose_min_tb";
            this.bet_lose_min_tb.Size = new System.Drawing.Size(76, 23);
            this.bet_lose_min_tb.TabIndex = 174;
            this.bet_lose_min_tb.Text = "0.0000001";
            // 
            // bet_lose_max_tb
            // 
            this.bet_lose_max_tb.Location = new System.Drawing.Point(104, 53);
            this.bet_lose_max_tb.Name = "bet_lose_max_tb";
            this.bet_lose_max_tb.Size = new System.Drawing.Size(73, 23);
            this.bet_lose_max_tb.TabIndex = 175;
            this.bet_lose_max_tb.Text = "0.0000001";
            // 
            // gb_cashout_lose
            // 
            this.gb_cashout_lose.Controls.Add(this.cashout_lose_min_tb);
            this.gb_cashout_lose.Controls.Add(this.cashout_lose_max_tb);
            this.gb_cashout_lose.Controls.Add(this.cashout_lose_cb);
            this.gb_cashout_lose.Enabled = false;
            this.gb_cashout_lose.Location = new System.Drawing.Point(7, 48);
            this.gb_cashout_lose.Name = "gb_cashout_lose";
            this.gb_cashout_lose.Size = new System.Drawing.Size(185, 87);
            this.gb_cashout_lose.TabIndex = 180;
            this.gb_cashout_lose.TabStop = false;
            this.gb_cashout_lose.Text = "Cashout";
            // 
            // cashout_lose_min_tb
            // 
            this.cashout_lose_min_tb.Location = new System.Drawing.Point(7, 53);
            this.cashout_lose_min_tb.Name = "cashout_lose_min_tb";
            this.cashout_lose_min_tb.Size = new System.Drawing.Size(76, 23);
            this.cashout_lose_min_tb.TabIndex = 179;
            this.cashout_lose_min_tb.Text = "1.1";
            // 
            // cashout_lose_max_tb
            // 
            this.cashout_lose_max_tb.Location = new System.Drawing.Point(101, 53);
            this.cashout_lose_max_tb.Name = "cashout_lose_max_tb";
            this.cashout_lose_max_tb.Size = new System.Drawing.Size(73, 23);
            this.cashout_lose_max_tb.TabIndex = 180;
            this.cashout_lose_max_tb.Text = "1.1";
            // 
            // cashout_lose_cb
            // 
            this.cashout_lose_cb.FormattingEnabled = true;
            this.cashout_lose_cb.Items.AddRange(new object[]
                {"Multiply Cashout", "Add Cashout", "Minus Cashout", "None"});
            this.cashout_lose_cb.Location = new System.Drawing.Point(7, 22);
            this.cashout_lose_cb.Name = "cashout_lose_cb";
            this.cashout_lose_cb.Size = new System.Drawing.Size(168, 23);
            this.cashout_lose_cb.TabIndex = 178;
            // 
            // reset_lose_rb
            // 
            this.reset_lose_rb.AutoSize = true;
            this.reset_lose_rb.Checked = true;
            this.reset_lose_rb.Location = new System.Drawing.Point(7, 22);
            this.reset_lose_rb.Name = "reset_lose_rb";
            this.reset_lose_rb.Size = new System.Drawing.Size(53, 19);
            this.reset_lose_rb.TabIndex = 170;
            this.reset_lose_rb.TabStop = true;
            this.reset_lose_rb.Text = "Reset";
            this.reset_lose_rb.UseVisualStyleBackColor = true;
            this.reset_lose_rb.CheckedChanged += new System.EventHandler(this.reset_lose_rb_checked_changed);
            // 
            // grp_stop_game
            // 
            this.grp_stop_game.Controls.Add(this.stop_game_win_min_tb);
            this.grp_stop_game.Controls.Add(this.label35);
            this.grp_stop_game.Controls.Add(this.label36);
            this.grp_stop_game.Controls.Add(this.stop_game_win_max_tb);
            this.grp_stop_game.Controls.Add(this.label38);
            this.grp_stop_game.Controls.Add(this.stop_game_lose_min_tb);
            this.grp_stop_game.Controls.Add(this.label37);
            this.grp_stop_game.Controls.Add(this.stop_game_lose_max_tb);
            this.grp_stop_game.Enabled = false;
            this.grp_stop_game.Location = new System.Drawing.Point(9, 47);
            this.grp_stop_game.Name = "grp_stop_game";
            this.grp_stop_game.Size = new System.Drawing.Size(318, 85);
            this.grp_stop_game.TabIndex = 162;
            this.grp_stop_game.TabStop = false;
            this.grp_stop_game.Text = "Stop game";
            // 
            // stop_game_win_min_tb
            // 
            this.stop_game_win_min_tb.Location = new System.Drawing.Point(192, 20);
            this.stop_game_win_min_tb.Name = "stop_game_win_min_tb";
            this.stop_game_win_min_tb.Size = new System.Drawing.Size(38, 23);
            this.stop_game_win_min_tb.TabIndex = 5;
            this.stop_game_win_min_tb.Text = "0";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(9, 23);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(170, 15);
            this.label35.TabIndex = 4;
            this.label35.Text = "Stop game after each win from";
            this.toolTip.SetToolTip(this.label35, "Dừng game sau mỗi lần thắng");
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(239, 23);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(18, 15);
            this.label36.TabIndex = 6;
            this.label36.Text = "to";
            // 
            // stop_game_win_max_tb
            // 
            this.stop_game_win_max_tb.Location = new System.Drawing.Point(266, 20);
            this.stop_game_win_max_tb.Name = "stop_game_win_max_tb";
            this.stop_game_win_max_tb.Size = new System.Drawing.Size(38, 23);
            this.stop_game_win_max_tb.TabIndex = 7;
            this.stop_game_win_max_tb.Text = "0";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(6, 53);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(172, 15);
            this.label38.TabIndex = 8;
            this.label38.Text = "Stop game after each lose from";
            this.toolTip.SetToolTip(this.label38, "Dừng game sau mỗi lần thua");
            // 
            // stop_game_lose_min_tb
            // 
            this.stop_game_lose_min_tb.Location = new System.Drawing.Point(192, 50);
            this.stop_game_lose_min_tb.Name = "stop_game_lose_min_tb";
            this.stop_game_lose_min_tb.Size = new System.Drawing.Size(38, 23);
            this.stop_game_lose_min_tb.TabIndex = 9;
            this.stop_game_lose_min_tb.Text = "0";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(239, 53);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(18, 15);
            this.label37.TabIndex = 10;
            this.label37.Text = "to";
            // 
            // stop_game_lose_max_tb
            // 
            this.stop_game_lose_max_tb.Location = new System.Drawing.Point(266, 50);
            this.stop_game_lose_max_tb.Name = "stop_game_lose_max_tb";
            this.stop_game_lose_max_tb.Size = new System.Drawing.Size(38, 23);
            this.stop_game_lose_max_tb.TabIndex = 11;
            this.stop_game_lose_max_tb.Text = "0";
            // 
            // stop_each_game_checkbox
            // 
            this.stop_each_game_checkbox.AutoSize = true;
            this.stop_each_game_checkbox.Checked = true;
            this.stop_each_game_checkbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.stop_each_game_checkbox.Location = new System.Drawing.Point(321, 24);
            this.stop_each_game_checkbox.Name = "stop_each_game_checkbox";
            this.stop_each_game_checkbox.Size = new System.Drawing.Size(15, 14);
            this.stop_each_game_checkbox.TabIndex = 68;
            this.stop_each_game_checkbox.UseVisualStyleBackColor = true;
            this.stop_each_game_checkbox.CheckedChanged +=
                new System.EventHandler(this.stop_each_game_checkbox_checked_changed);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(215, 293);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(129, 15);
            this.label13.TabIndex = 44;
            this.label13.Text = "will stop current thread";
            // 
            // target_lose_tb
            // 
            this.target_lose_tb.Location = new System.Drawing.Point(169, 290);
            this.target_lose_tb.Name = "target_lose_tb";
            this.target_lose_tb.Size = new System.Drawing.Size(38, 23);
            this.target_lose_tb.TabIndex = 43;
            this.target_lose_tb.Text = "0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 293);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(153, 15);
            this.label14.TabIndex = 42;
            this.label14.Text = "Target lose (% root balance)";
            this.toolTip.SetToolTip(this.label14, "Tổng coin thua thì dừng đặt cược");
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(215, 263);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(129, 15);
            this.label6.TabIndex = 41;
            this.label6.Text = "will stop current thread";
            // 
            // target_win_tb
            // 
            this.target_win_tb.Location = new System.Drawing.Point(169, 260);
            this.target_win_tb.Name = "target_win_tb";
            this.target_win_tb.Size = new System.Drawing.Size(38, 23);
            this.target_win_tb.TabIndex = 40;
            this.target_win_tb.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 263);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(151, 15);
            this.label3.TabIndex = 39;
            this.label3.Text = "Target win (% root balance)";
            this.toolTip.SetToolTip(this.label3, "Tổng coin thắng thì dừng đặt cược");
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(213, 233);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(129, 15);
            this.label48.TabIndex = 38;
            this.label48.Text = "will stop current thread";
            // 
            // after_lose_stop_thread_max_tb
            // 
            this.after_lose_stop_thread_max_tb.Location = new System.Drawing.Point(169, 230);
            this.after_lose_stop_thread_max_tb.Name = "after_lose_stop_thread_max_tb";
            this.after_lose_stop_thread_max_tb.Size = new System.Drawing.Size(38, 23);
            this.after_lose_stop_thread_max_tb.TabIndex = 37;
            this.after_lose_stop_thread_max_tb.Text = "0";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(142, 231);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(18, 15);
            this.label49.TabIndex = 36;
            this.label49.Text = "to";
            // 
            // after_lose_stop_thread_min_tb
            // 
            this.after_lose_stop_thread_min_tb.Location = new System.Drawing.Point(96, 230);
            this.after_lose_stop_thread_min_tb.Name = "after_lose_stop_thread_min_tb";
            this.after_lose_stop_thread_min_tb.Size = new System.Drawing.Size(38, 23);
            this.after_lose_stop_thread_min_tb.TabIndex = 35;
            this.after_lose_stop_thread_min_tb.Text = "0";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(2, 233);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(86, 15);
            this.label50.TabIndex = 34;
            this.label50.Text = "After lose from";
            this.toolTip.SetToolTip(this.label50,
                "Chức năng này sẽ ưu tiên hơn chức năng dừng sau mỗi lần thua/khoảng thua");
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(213, 203);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(129, 15);
            this.label47.TabIndex = 33;
            this.label47.Text = "will stop current thread";
            // 
            // after_win_stop_thread_max_tb
            // 
            this.after_win_stop_thread_max_tb.Location = new System.Drawing.Point(168, 200);
            this.after_win_stop_thread_max_tb.Name = "after_win_stop_thread_max_tb";
            this.after_win_stop_thread_max_tb.Size = new System.Drawing.Size(38, 23);
            this.after_win_stop_thread_max_tb.TabIndex = 32;
            this.after_win_stop_thread_max_tb.Text = "0";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(142, 201);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(18, 15);
            this.label45.TabIndex = 31;
            this.label45.Text = "to";
            // 
            // after_win_stop_thread_min_tb
            // 
            this.after_win_stop_thread_min_tb.Location = new System.Drawing.Point(96, 200);
            this.after_win_stop_thread_min_tb.Name = "after_win_stop_thread_min_tb";
            this.after_win_stop_thread_min_tb.Size = new System.Drawing.Size(38, 23);
            this.after_win_stop_thread_min_tb.TabIndex = 30;
            this.after_win_stop_thread_min_tb.Text = "0";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(6, 203);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(84, 15);
            this.label46.TabIndex = 29;
            this.label46.Text = "After win from";
            this.toolTip.SetToolTip(this.label46,
                "Chức năng này sẽ ưu tiên hơn chức năng dừng sau mỗi lần thắng/khoảng thắng");
            // 
            // stop_game_after_lose_max_tb
            // 
            this.stop_game_after_lose_max_tb.Location = new System.Drawing.Point(406, 167);
            this.stop_game_after_lose_max_tb.Name = "stop_game_after_lose_max_tb";
            this.stop_game_after_lose_max_tb.Size = new System.Drawing.Size(38, 23);
            this.stop_game_after_lose_max_tb.TabIndex = 28;
            this.stop_game_after_lose_max_tb.Text = "0";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(379, 171);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(18, 15);
            this.label41.TabIndex = 27;
            this.label41.Text = "to";
            // 
            // stop_game_after_lose_min_tb
            // 
            this.stop_game_after_lose_min_tb.Location = new System.Drawing.Point(334, 167);
            this.stop_game_after_lose_min_tb.Name = "stop_game_after_lose_min_tb";
            this.stop_game_after_lose_min_tb.Size = new System.Drawing.Size(38, 23);
            this.stop_game_after_lose_min_tb.TabIndex = 26;
            this.stop_game_after_lose_min_tb.Text = "0";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(215, 171);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(113, 15);
            this.label42.TabIndex = 25;
            this.label42.Text = "will stop game from";
            // 
            // after_lose_stop_game_max_tb
            // 
            this.after_lose_stop_game_max_tb.Location = new System.Drawing.Point(169, 167);
            this.after_lose_stop_game_max_tb.Name = "after_lose_stop_game_max_tb";
            this.after_lose_stop_game_max_tb.Size = new System.Drawing.Size(38, 23);
            this.after_lose_stop_game_max_tb.TabIndex = 24;
            this.after_lose_stop_game_max_tb.Text = "0";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(142, 171);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(18, 15);
            this.label43.TabIndex = 23;
            this.label43.Text = "to";
            // 
            // after_lose_stop_game_min_tb
            // 
            this.after_lose_stop_game_min_tb.Location = new System.Drawing.Point(96, 170);
            this.after_lose_stop_game_min_tb.Name = "after_lose_stop_game_min_tb";
            this.after_lose_stop_game_min_tb.Size = new System.Drawing.Size(38, 23);
            this.after_lose_stop_game_min_tb.TabIndex = 22;
            this.after_lose_stop_game_min_tb.Text = "0";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(2, 173);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(86, 15);
            this.label44.TabIndex = 21;
            this.label44.Text = "After lose from";
            // 
            // stop_game_after_win_max_tb
            // 
            this.stop_game_after_win_max_tb.Location = new System.Drawing.Point(406, 137);
            this.stop_game_after_win_max_tb.Name = "stop_game_after_win_max_tb";
            this.stop_game_after_win_max_tb.Size = new System.Drawing.Size(38, 23);
            this.stop_game_after_win_max_tb.TabIndex = 20;
            this.stop_game_after_win_max_tb.Text = "0";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(379, 141);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(18, 15);
            this.label40.TabIndex = 19;
            this.label40.Text = "to";
            // 
            // stop_game_after_win_min_tb
            // 
            this.stop_game_after_win_min_tb.Location = new System.Drawing.Point(334, 137);
            this.stop_game_after_win_min_tb.Name = "stop_game_after_win_min_tb";
            this.stop_game_after_win_min_tb.Size = new System.Drawing.Size(38, 23);
            this.stop_game_after_win_min_tb.TabIndex = 18;
            this.stop_game_after_win_min_tb.Text = "0";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(215, 141);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(113, 15);
            this.label39.TabIndex = 17;
            this.label39.Text = "will stop game from";
            // 
            // after_win_stop_game_max_tb
            // 
            this.after_win_stop_game_max_tb.Location = new System.Drawing.Point(169, 137);
            this.after_win_stop_game_max_tb.Name = "after_win_stop_game_max_tb";
            this.after_win_stop_game_max_tb.Size = new System.Drawing.Size(38, 23);
            this.after_win_stop_game_max_tb.TabIndex = 16;
            this.after_win_stop_game_max_tb.Text = "0";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(142, 141);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(18, 15);
            this.label25.TabIndex = 15;
            this.label25.Text = "to";
            // 
            // after_win_stop_game_min_tb
            // 
            this.after_win_stop_game_min_tb.Location = new System.Drawing.Point(96, 140);
            this.after_win_stop_game_min_tb.Name = "after_win_stop_game_min_tb";
            this.after_win_stop_game_min_tb.Size = new System.Drawing.Size(38, 23);
            this.after_win_stop_game_min_tb.TabIndex = 14;
            this.after_win_stop_game_min_tb.Text = "0";
            // 
            // stop_each_game_max_tb
            // 
            this.stop_each_game_max_tb.Location = new System.Drawing.Point(275, 21);
            this.stop_each_game_max_tb.Name = "stop_each_game_max_tb";
            this.stop_each_game_max_tb.Size = new System.Drawing.Size(38, 23);
            this.stop_each_game_max_tb.TabIndex = 13;
            this.stop_each_game_max_tb.Text = "0";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 143);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(84, 15);
            this.label18.TabIndex = 12;
            this.label18.Text = "After win from";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(248, 24);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(18, 15);
            this.label24.TabIndex = 2;
            this.label24.Text = "to";
            // 
            // stop_each_game_min_tb
            // 
            this.stop_each_game_min_tb.Location = new System.Drawing.Point(202, 21);
            this.stop_each_game_min_tb.Name = "stop_each_game_min_tb";
            this.stop_each_game_min_tb.Size = new System.Drawing.Size(38, 23);
            this.stop_each_game_min_tb.TabIndex = 1;
            this.stop_each_game_min_tb.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(41, 25);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(148, 15);
            this.label17.TabIndex = 0;
            this.label17.Text = "Stop after each game from";
            this.toolTip.SetToolTip(this.label17, "Dừng game sau mỗi lần đặt cược");
            // 
            // game_tabpage
            // 
            this.game_tabpage.Controls.Add(this.gb_game_panel);
            this.game_tabpage.Location = new System.Drawing.Point(4, 22);
            this.game_tabpage.Name = "game_tabpage";
            this.game_tabpage.Padding = new System.Windows.Forms.Padding(3);
            this.game_tabpage.Size = new System.Drawing.Size(484, 739);
            this.game_tabpage.TabIndex = 4;
            this.game_tabpage.Text = "Game";
            this.game_tabpage.UseVisualStyleBackColor = true;
            // 
            // gb_game_panel
            // 
            this.gb_game_panel.AutoScroll = true;
            this.gb_game_panel.Controls.Add(this.gb_game);
            this.gb_game_panel.Location = new System.Drawing.Point(0, 0);
            this.gb_game_panel.Name = "gb_game_panel";
            this.gb_game_panel.Size = new System.Drawing.Size(483, 735);
            this.gb_game_panel.TabIndex = 0;
            // 
            // gb_game
            // 
            this.gb_game.Controls.Add(this.gb_cashout_max_tb);
            this.gb_game.Controls.Add(this.gb_cashout_min_tb);
            this.gb_game.Controls.Add(this.gb_bet_max_tb);
            this.gb_game.Controls.Add(this.gb_bet_min_tb);
            this.gb_game.Controls.Add(this.gb_coin_cb);
            this.gb_game.Controls.Add(this.gb_game_cb);
            this.gb_game.Location = new System.Drawing.Point(3, 3);
            this.gb_game.Name = "gb_game";
            this.gb_game.Size = new System.Drawing.Size(189, 143);
            this.gb_game.TabIndex = 0;
            this.gb_game.TabStop = false;
            this.gb_game.Text = "Bet, Cashout, Coin, Game 1";
            // 
            // gb_cashout_max_tb
            // 
            this.gb_cashout_max_tb.Location = new System.Drawing.Point(100, 51);
            this.gb_cashout_max_tb.Name = "gb_cashout_max_tb";
            this.gb_cashout_max_tb.Size = new System.Drawing.Size(81, 23);
            this.gb_cashout_max_tb.TabIndex = 18;
            this.gb_cashout_max_tb.Text = "1.1";
            this.gb_cashout_max_tb.TextChanged += new System.EventHandler(this.gb_cashout_max_tb_text_changed);
            this.gb_cashout_max_tb.KeyPress +=
                new System.Windows.Forms.KeyPressEventHandler(this.gb_cashout_max_tb_key_press);
            // 
            // gb_cashout_min_tb
            // 
            this.gb_cashout_min_tb.Location = new System.Drawing.Point(7, 51);
            this.gb_cashout_min_tb.Name = "gb_cashout_min_tb";
            this.gb_cashout_min_tb.Size = new System.Drawing.Size(81, 23);
            this.gb_cashout_min_tb.TabIndex = 17;
            this.gb_cashout_min_tb.Text = "1.1";
            this.gb_cashout_min_tb.TextChanged += new System.EventHandler(this.gb_cashout_min_tb_text_changed);
            this.gb_cashout_min_tb.KeyPress +=
                new System.Windows.Forms.KeyPressEventHandler(this.gb_cashout_min_tb_key_press);
            // 
            // gb_bet_max_tb
            // 
            this.gb_bet_max_tb.Location = new System.Drawing.Point(100, 22);
            this.gb_bet_max_tb.Name = "gb_bet_max_tb";
            this.gb_bet_max_tb.Size = new System.Drawing.Size(81, 23);
            this.gb_bet_max_tb.TabIndex = 16;
            this.gb_bet_max_tb.Text = "0.0000001";
            this.gb_bet_max_tb.TextChanged += new System.EventHandler(this.gb_bet_max_tb_text_changed);
            this.gb_bet_max_tb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gb_bet_max_tb_key_press);
            // 
            // gb_bet_min_tb
            // 
            this.gb_bet_min_tb.Location = new System.Drawing.Point(7, 21);
            this.gb_bet_min_tb.Name = "gb_bet_min_tb";
            this.gb_bet_min_tb.Size = new System.Drawing.Size(81, 23);
            this.gb_bet_min_tb.TabIndex = 15;
            this.gb_bet_min_tb.Text = "0.0000001";
            this.gb_bet_min_tb.TextChanged += new System.EventHandler(this.gb_bet_min_tb_text_changed);
            this.gb_bet_min_tb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gb_bet_min_tb_key_press);
            // 
            // gb_coin_cb
            // 
            this.gb_coin_cb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.gb_coin_cb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.gb_coin_cb.FormattingEnabled = true;
            this.gb_coin_cb.Location = new System.Drawing.Point(7, 82);
            this.gb_coin_cb.Name = "gb_coin_cb";
            this.gb_coin_cb.Size = new System.Drawing.Size(174, 23);
            this.gb_coin_cb.TabIndex = 14;
            this.gb_coin_cb.SelectedIndexChanged += new System.EventHandler(this.gb_coin_cb_seleted_index_changed);
            this.gb_coin_cb.Enter += new System.EventHandler(this.gb_coin_cb_enter);
            this.gb_coin_cb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gb_coin_cb_key_press);
            // 
            // gb_game_cb
            // 
            this.gb_game_cb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.gb_game_cb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.gb_game_cb.FormattingEnabled = true;
            this.gb_game_cb.Location = new System.Drawing.Point(7, 113);
            this.gb_game_cb.Name = "gb_game_cb";
            this.gb_game_cb.Size = new System.Drawing.Size(174, 23);
            this.gb_game_cb.TabIndex = 13;
            this.gb_game_cb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gb_game_cb_key_press);
            // 
            // programmer_tabpage
            // 
            this.programmer_tabpage.Controls.Add(this.programmer_fctb);
            this.programmer_tabpage.Controls.Add(this.programmer_rb);
            this.programmer_tabpage.Location = new System.Drawing.Point(4, 22);
            this.programmer_tabpage.Name = "programmer_tabpage";
            this.programmer_tabpage.Padding = new System.Windows.Forms.Padding(3);
            this.programmer_tabpage.Size = new System.Drawing.Size(484, 739);
            this.programmer_tabpage.TabIndex = 1;
            this.programmer_tabpage.Text = "Programmer";
            this.programmer_tabpage.UseVisualStyleBackColor = true;
            // 
            // programmer_fctb
            // 
            this.programmer_fctb.AutoCompleteBracketsList =
                new char[] {'(', ')', '{', '}', '[', ']', '\"', '\"', '\'', '\''};
            this.programmer_fctb.AutoIndentCharsPatterns =
                "\r\n^\\s*[\\w\\.]+(\\s\\w+)?\\s*(?<range>=)\\s*(?<range>.+)\r\n";
            this.programmer_fctb.AutoScrollMinSize = new System.Drawing.Size(2, 14);
            this.programmer_fctb.BackBrush = null;
            this.programmer_fctb.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.programmer_fctb.BracketsHighlightStrategy = FastColoredTextBoxNS.BracketsHighlightStrategy.Strategy2;
            this.programmer_fctb.CharHeight = 14;
            this.programmer_fctb.CharWidth = 8;
            this.programmer_fctb.CommentPrefix = "--";
            this.programmer_fctb.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.programmer_fctb.DisabledColor = System.Drawing.Color.FromArgb(((int) (((byte) (100)))),
                ((int) (((byte) (180)))), ((int) (((byte) (180)))), ((int) (((byte) (180)))));
            this.programmer_fctb.Enabled = false;
            this.programmer_fctb.Font = new System.Drawing.Font("Courier New", 9.75F);
            this.programmer_fctb.IsReplaceMode = false;
            this.programmer_fctb.Language = FastColoredTextBoxNS.Language.Lua;
            this.programmer_fctb.LeftBracket = '(';
            this.programmer_fctb.LeftBracket2 = '{';
            this.programmer_fctb.Location = new System.Drawing.Point(2, 25);
            this.programmer_fctb.Name = "programmer_fctb";
            this.programmer_fctb.Paddings = new System.Windows.Forms.Padding(0);
            this.programmer_fctb.RightBracket = ')';
            this.programmer_fctb.RightBracket2 = '}';
            this.programmer_fctb.SelectionColor = System.Drawing.Color.FromArgb(((int) (((byte) (60)))),
                ((int) (((byte) (0)))), ((int) (((byte) (0)))), ((int) (((byte) (255)))));
            this.programmer_fctb.ServiceColors =
                ((FastColoredTextBoxNS.ServiceColors) (resources.GetObject("programmer_fctb.ServiceColors")));
            this.programmer_fctb.Size = new System.Drawing.Size(478, 702);
            this.programmer_fctb.TabIndex = 12;
            this.programmer_fctb.Zoom = 100;
            // 
            // programmer_rb
            // 
            this.programmer_rb.AutoSize = true;
            this.programmer_rb.Location = new System.Drawing.Point(7, 3);
            this.programmer_rb.Name = "programmer_rb";
            this.programmer_rb.Size = new System.Drawing.Size(92, 19);
            this.programmer_rb.TabIndex = 1;
            this.programmer_rb.TabStop = true;
            this.programmer_rb.Text = "Programmer";
            this.programmer_rb.UseVisualStyleBackColor = true;
            this.programmer_rb.Click += new System.EventHandler(this.programmer_rb_click);
            // 
            // history_tabpage
            // 
            this.history_tabpage.Controls.Add(this.history_tb);
            this.history_tabpage.Location = new System.Drawing.Point(4, 22);
            this.history_tabpage.Name = "history_tabpage";
            this.history_tabpage.Padding = new System.Windows.Forms.Padding(3);
            this.history_tabpage.Size = new System.Drawing.Size(484, 739);
            this.history_tabpage.TabIndex = 0;
            this.history_tabpage.Text = "History";
            this.history_tabpage.UseVisualStyleBackColor = true;
            // 
            // history_tb
            // 
            this.history_tb.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F,
                System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.history_tb.Location = new System.Drawing.Point(0, 0);
            this.history_tb.Name = "history_tb";
            this.history_tb.ReadOnly = true;
            this.history_tb.Size = new System.Drawing.Size(479, 731);
            this.history_tb.TabIndex = 0;
            this.history_tb.Text = "";
            // 
            // variables_tabpage
            // 
            this.variables_tabpage.Controls.Add(this.variables_tb);
            this.variables_tabpage.Location = new System.Drawing.Point(4, 22);
            this.variables_tabpage.Name = "variables_tabpage";
            this.variables_tabpage.Padding = new System.Windows.Forms.Padding(3);
            this.variables_tabpage.Size = new System.Drawing.Size(484, 739);
            this.variables_tabpage.TabIndex = 5;
            this.variables_tabpage.Text = "Variables";
            this.variables_tabpage.UseVisualStyleBackColor = true;
            // 
            // variables_tb
            // 
            this.variables_tb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.variables_tb.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F,
                System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.variables_tb.Location = new System.Drawing.Point(3, 3);
            this.variables_tb.Name = "variables_tb";
            this.variables_tb.ReadOnly = true;
            this.variables_tb.Size = new System.Drawing.Size(478, 733);
            this.variables_tb.TabIndex = 0;
            this.variables_tb.Text = "";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoScroll = true;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.gb_proxy, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.gp_basic_stat, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.gb_setting, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.gb_login, 2, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1046, 261);
            this.tableLayoutPanel1.TabIndex = 27;
            // 
            // gb_proxy
            // 
            this.gb_proxy.Controls.Add(this.default_proxy_rb);
            this.gb_proxy.Controls.Add(this.label33);
            this.gb_proxy.Controls.Add(this.proxy_rb);
            this.gb_proxy.Controls.Add(this.add_proxy_file_btn);
            this.gb_proxy.Controls.Add(this.proxy_tb);
            this.gb_proxy.Location = new System.Drawing.Point(1046, 3);
            this.gb_proxy.Name = "gb_proxy";
            this.gb_proxy.Size = new System.Drawing.Size(349, 235);
            this.gb_proxy.TabIndex = 3;
            this.gb_proxy.TabStop = false;
            this.gb_proxy.Text = "Proxy";
            // 
            // default_proxy_rb
            // 
            this.default_proxy_rb.AutoSize = true;
            this.default_proxy_rb.Checked = true;
            this.default_proxy_rb.Location = new System.Drawing.Point(240, 203);
            this.default_proxy_rb.Name = "default_proxy_rb";
            this.default_proxy_rb.Size = new System.Drawing.Size(96, 19);
            this.default_proxy_rb.TabIndex = 8;
            this.default_proxy_rb.TabStop = true;
            this.default_proxy_rb.Text = "Default proxy";
            this.default_proxy_rb.UseVisualStyleBackColor = true;
            this.default_proxy_rb.CheckedChanged += new System.EventHandler(this.default_proxy_rb_checked_changed);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label33.Location = new System.Drawing.Point(7, 16);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(139, 20);
            this.label33.TabIndex = 11;
            this.label33.Text = "Các proxy của bạn";
            this.toolTip.SetToolTip(this.label33, "Proxy nhập vào theo định dạng \"ip:port\"");
            // 
            // proxy_rb
            // 
            this.proxy_rb.AutoSize = true;
            this.proxy_rb.Location = new System.Drawing.Point(7, 203);
            this.proxy_rb.Name = "proxy_rb";
            this.proxy_rb.Size = new System.Drawing.Size(55, 19);
            this.proxy_rb.TabIndex = 1;
            this.proxy_rb.TabStop = true;
            this.proxy_rb.Text = "Proxy";
            this.proxy_rb.UseVisualStyleBackColor = true;
            this.proxy_rb.CheckedChanged += new System.EventHandler(this.proxy_rb_checked_changed);
            // 
            // add_proxy_file_btn
            // 
            this.add_proxy_file_btn.Enabled = false;
            this.add_proxy_file_btn.Location = new System.Drawing.Point(240, 14);
            this.add_proxy_file_btn.Name = "add_proxy_file_btn";
            this.add_proxy_file_btn.Size = new System.Drawing.Size(101, 27);
            this.add_proxy_file_btn.TabIndex = 10;
            this.add_proxy_file_btn.Text = "Thêm File";
            this.add_proxy_file_btn.UseVisualStyleBackColor = true;
            this.add_proxy_file_btn.Click += new System.EventHandler(this.add_proxy_file_click);
            // 
            // proxy_tb
            // 
            this.proxy_tb.Enabled = false;
            this.proxy_tb.Location = new System.Drawing.Point(7, 46);
            this.proxy_tb.Multiline = true;
            this.proxy_tb.Name = "proxy_tb";
            this.proxy_tb.Size = new System.Drawing.Size(334, 141);
            this.proxy_tb.TabIndex = 0;
            // 
            // gp_basic_stat
            // 
            this.gp_basic_stat.Controls.Add(this.withdraw_amount_tb);
            this.gp_basic_stat.Controls.Add(this.label32);
            this.gp_basic_stat.Controls.Add(this.withdraw_password_tb);
            this.gp_basic_stat.Controls.Add(this.label22);
            this.gp_basic_stat.Controls.Add(this.withdraw_btn);
            this.gp_basic_stat.Controls.Add(this.withdraw_address_tb);
            this.gp_basic_stat.Controls.Add(this.label21);
            this.gp_basic_stat.Controls.Add(this.tableLayoutPanel3);
            this.gp_basic_stat.Location = new System.Drawing.Point(3, 3);
            this.gp_basic_stat.Name = "gp_basic_stat";
            this.gp_basic_stat.Size = new System.Drawing.Size(341, 235);
            this.gp_basic_stat.TabIndex = 0;
            this.gp_basic_stat.TabStop = false;
            this.gp_basic_stat.Text = "Basic Stats";
            // 
            // withdraw_amount_tb
            // 
            this.withdraw_amount_tb.Location = new System.Drawing.Point(110, 203);
            this.withdraw_amount_tb.Name = "withdraw_amount_tb";
            this.withdraw_amount_tb.Size = new System.Drawing.Size(128, 23);
            this.withdraw_amount_tb.TabIndex = 7;
            this.withdraw_amount_tb.Text = "1000";
            this.toolTip.SetToolTip(this.withdraw_amount_tb, "Amount withdraw default is 1000 coin");
            this.withdraw_amount_tb.KeyPress +=
                new System.Windows.Forms.KeyPressEventHandler(this.withdraw_amount_tb_key_press);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label32.Location = new System.Drawing.Point(22, 204);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(69, 20);
            this.label32.TabIndex = 6;
            this.label32.Text = "Amount:";
            this.toolTip.SetToolTip(this.label32, "Số lượng coin bạn muốn rút");
            // 
            // withdraw_password_tb
            // 
            this.withdraw_password_tb.Location = new System.Drawing.Point(110, 170);
            this.withdraw_password_tb.Name = "withdraw_password_tb";
            this.withdraw_password_tb.Size = new System.Drawing.Size(222, 23);
            this.withdraw_password_tb.TabIndex = 5;
            this.toolTip.SetToolTip(this.withdraw_password_tb, "Mật khẩu để rút coin về ví");
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label22.Location = new System.Drawing.Point(7, 170);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(82, 20);
            this.label22.TabIndex = 4;
            this.label22.Text = "Password:";
            // 
            // withdraw_btn
            // 
            this.withdraw_btn.Enabled = false;
            this.withdraw_btn.Location = new System.Drawing.Point(245, 201);
            this.withdraw_btn.Name = "withdraw_btn";
            this.withdraw_btn.Size = new System.Drawing.Size(87, 27);
            this.withdraw_btn.TabIndex = 3;
            this.withdraw_btn.Text = "Withdraw";
            this.withdraw_btn.UseVisualStyleBackColor = true;
            // 
            // withdraw_address_tb
            // 
            this.withdraw_address_tb.Location = new System.Drawing.Point(110, 134);
            this.withdraw_address_tb.Name = "withdraw_address_tb";
            this.withdraw_address_tb.Size = new System.Drawing.Size(222, 23);
            this.withdraw_address_tb.TabIndex = 2;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label21.Location = new System.Drawing.Point(19, 132);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(72, 20);
            this.label21.TabIndex = 1;
            this.label21.Text = "Address:";
            this.toolTip.SetToolTip(this.label21, "Địa chỉ ví bạn muốn rút");
            // 
            // gb_setting
            // 
            this.gb_setting.Controls.Add(this.browser_cb);
            this.gb_setting.Controls.Add(this.label31);
            this.gb_setting.Controls.Add(this.label19);
            this.gb_setting.Controls.Add(this.thread_nud);
            this.gb_setting.Controls.Add(this.website_cb);
            this.gb_setting.Controls.Add(this.deposit_cb);
            this.gb_setting.Controls.Add(this.label23);
            this.gb_setting.Controls.Add(this.label20);
            this.gb_setting.Location = new System.Drawing.Point(350, 3);
            this.gb_setting.Name = "gb_setting";
            this.gb_setting.Size = new System.Drawing.Size(335, 235);
            this.gb_setting.TabIndex = 1;
            this.gb_setting.TabStop = false;
            this.gb_setting.Text = "Settings";
            // 
            // browser_cb
            // 
            this.browser_cb.FormattingEnabled = true;
            this.browser_cb.Location = new System.Drawing.Point(100, 15);
            this.browser_cb.Name = "browser_cb";
            this.browser_cb.Size = new System.Drawing.Size(227, 23);
            this.browser_cb.TabIndex = 9;
            this.browser_cb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.browser_cb_key_press);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label31.Location = new System.Drawing.Point(9, 16);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(71, 20);
            this.label31.TabIndex = 8;
            this.label31.Text = "Browser:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label19.Location = new System.Drawing.Point(19, 78);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(63, 20);
            this.label19.TabIndex = 0;
            this.label19.Text = "Thread:";
            // 
            // thread_nud
            // 
            this.thread_nud.Location = new System.Drawing.Point(100, 77);
            this.thread_nud.Name = "thread_nud";
            this.thread_nud.Size = new System.Drawing.Size(227, 23);
            this.thread_nud.TabIndex = 1;
            this.thread_nud.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.thread_nud.ValueChanged += new System.EventHandler(this.thread_numeric_value_changed);
            this.thread_nud.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.thread_nud_key_press);
            // 
            // website_cb
            // 
            this.website_cb.FormattingEnabled = true;
            this.website_cb.Location = new System.Drawing.Point(100, 46);
            this.website_cb.Name = "website_cb";
            this.website_cb.Size = new System.Drawing.Size(227, 23);
            this.website_cb.TabIndex = 3;
            this.website_cb.SelectedIndexChanged += new System.EventHandler(this.website_cb_selected_index_changed);
            this.website_cb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.website_cb_key_press);
            // 
            // deposit_cb
            // 
            this.deposit_cb.FormattingEnabled = true;
            this.deposit_cb.Location = new System.Drawing.Point(100, 107);
            this.deposit_cb.Name = "deposit_cb";
            this.deposit_cb.Size = new System.Drawing.Size(227, 23);
            this.deposit_cb.TabIndex = 3;
            this.deposit_cb.SelectedIndexChanged += new System.EventHandler(this.deposit_selected_index_changed);
            this.deposit_cb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.deposit_cb_key_press);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label23.Location = new System.Drawing.Point(9, 47);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(71, 20);
            this.label23.TabIndex = 2;
            this.label23.Text = "Website:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label20.Location = new System.Drawing.Point(12, 108);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(68, 20);
            this.label20.TabIndex = 2;
            this.label20.Text = "Deposit:";
            // 
            // gb_login
            // 
            this.gb_login.Controls.Add(this.stop_btn);
            this.gb_login.Controls.Add(this.start_btn);
            this.gb_login.Controls.Add(this.account_tb);
            this.gb_login.Controls.Add(this.label16);
            this.gb_login.Controls.Add(this.button1);
            this.gb_login.Location = new System.Drawing.Point(691, 3);
            this.gb_login.Name = "gb_login";
            this.gb_login.Size = new System.Drawing.Size(349, 235);
            this.gb_login.TabIndex = 2;
            this.gb_login.TabStop = false;
            this.gb_login.Text = "Login";
            // 
            // stop_btn
            // 
            this.stop_btn.Enabled = false;
            this.stop_btn.Location = new System.Drawing.Point(257, 200);
            this.stop_btn.Name = "stop_btn";
            this.stop_btn.Size = new System.Drawing.Size(85, 27);
            this.stop_btn.TabIndex = 9;
            this.stop_btn.Text = "Stop";
            this.stop_btn.UseVisualStyleBackColor = true;
            this.stop_btn.Click += new System.EventHandler(this.stop_btn_click);
            // 
            // start_btn
            // 
            this.start_btn.Location = new System.Drawing.Point(7, 200);
            this.start_btn.Name = "start_btn";
            this.start_btn.Size = new System.Drawing.Size(85, 27);
            this.start_btn.TabIndex = 8;
            this.start_btn.Text = "Start";
            this.start_btn.UseVisualStyleBackColor = true;
            this.start_btn.Click += new System.EventHandler(this.start_btn_click);
            // 
            // account_tb
            // 
            this.account_tb.Location = new System.Drawing.Point(7, 46);
            this.account_tb.Name = "account_tb";
            this.account_tb.Size = new System.Drawing.Size(334, 146);
            this.account_tb.TabIndex = 7;
            this.account_tb.Text = "";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label16.Location = new System.Drawing.Point(2, 17);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(167, 20);
            this.label16.TabIndex = 6;
            this.label16.Text = "Các tài khoản của bạn";
            this.toolTip.SetToolTip(this.label16,
                "Tài khoản nhập vào theo định dạng \"username;password\" hoặc \"username;session id\" " +
                "hoặc \"username;api key\" tuỳ vào loại trang web");
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(259, 14);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(83, 27);
            this.button1.TabIndex = 5;
            this.button1.Text = "Thêm File";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.add_account_file_click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {this.tool_strip_status_lb});
            this.statusStrip.Location = new System.Drawing.Point(0, 730);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            this.statusStrip.Size = new System.Drawing.Size(1376, 25);
            this.statusStrip.TabIndex = 27;
            // 
            // tool_strip_status_lb
            // 
            this.tool_strip_status_lb.AutoSize = false;
            this.tool_strip_status_lb.Name = "tool_strip_status_lb";
            this.tool_strip_status_lb.Size = new System.Drawing.Size(500, 20);
            this.tool_strip_status_lb.Text = "Status: OK";
            this.tool_strip_status_lb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DiceBotForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1376, 755);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "DiceBotForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dice Bot";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.dice_bot_form_closed);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tabPage.ResumeLayout(false);
            this.chart_panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.chart)).EndInit();
            this.data_grid_view_panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.data_grid_view)).EndInit();
            this.setting_tabcontrol.ResumeLayout(false);
            this.advanced_tabpage.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.gb_on_win.ResumeLayout(false);
            this.gb_on_win.PerformLayout();
            this.gb_bet_win.ResumeLayout(false);
            this.gb_bet_win.PerformLayout();
            this.gb_cashout_win.ResumeLayout(false);
            this.gb_cashout_win.PerformLayout();
            this.gb_profit_win_lose.ResumeLayout(false);
            this.gb_profit_win_lose.PerformLayout();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.gb_on_lose.ResumeLayout(false);
            this.gb_on_lose.PerformLayout();
            this.gb_bet_lose.ResumeLayout(false);
            this.gb_bet_lose.PerformLayout();
            this.gb_cashout_lose.ResumeLayout(false);
            this.gb_cashout_lose.PerformLayout();
            this.grp_stop_game.ResumeLayout(false);
            this.grp_stop_game.PerformLayout();
            this.game_tabpage.ResumeLayout(false);
            this.gb_game_panel.ResumeLayout(false);
            this.gb_game.ResumeLayout(false);
            this.gb_game.PerformLayout();
            this.programmer_tabpage.ResumeLayout(false);
            this.programmer_tabpage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.programmer_fctb)).EndInit();
            this.history_tabpage.ResumeLayout(false);
            this.variables_tabpage.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.gb_proxy.ResumeLayout(false);
            this.gb_proxy.PerformLayout();
            this.gp_basic_stat.ResumeLayout(false);
            this.gp_basic_stat.PerformLayout();
            this.gb_setting.ResumeLayout(false);
            this.gb_setting.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.thread_nud)).EndInit();
            this.gb_login.ResumeLayout(false);
            this.gb_login.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label total_win_lb;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label total_lose_lb;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label total_bet_lb;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TabControl setting_tabcontrol;
        private System.Windows.Forms.TabPage history_tabpage;
        private System.Windows.Forms.TabPage programmer_tabpage;
        private System.Windows.Forms.TabPage tabPage;
        private System.Windows.Forms.NumericUpDown thread_nud;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TabPage advanced_tabpage;
        private System.Windows.Forms.ComboBox deposit_cb;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.GroupBox gp_basic_stat;
        private System.Windows.Forms.Button withdraw_btn;
        private System.Windows.Forms.TextBox withdraw_address_tb;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox withdraw_password_tb;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.GroupBox gb_setting;
        private System.Windows.Forms.ComboBox website_cb;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.RichTextBox history_tb;
        private System.Windows.Forms.GroupBox gb_login;
        private System.Windows.Forms.RichTextBox account_tb;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel chart_panel;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart;
        private System.Windows.Forms.Panel data_grid_view_panel;
        private System.Windows.Forms.DataGridView data_grid_view;
        private System.Windows.Forms.Button stop_btn;
        private System.Windows.Forms.Button start_btn;
        private System.Windows.Forms.ComboBox browser_cb;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel tool_strip_status_lb;
        private System.Windows.Forms.TextBox withdraw_amount_tb;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.RadioButton proxy_rb;
        private System.Windows.Forms.TextBox proxy_tb;
        private System.Windows.Forms.RadioButton default_proxy_rb;
        private System.Windows.Forms.Button add_proxy_file_btn;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TabPage game_tabpage;
        private System.Windows.Forms.Panel gb_game_panel;
        private System.Windows.Forms.GroupBox gb_game;
        private System.Windows.Forms.ComboBox gb_game_cb;
        private System.Windows.Forms.ComboBox gb_coin_cb;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox stop_each_game_checkbox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox target_lose_tb;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox target_win_tb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox after_lose_stop_thread_max_tb;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox after_lose_stop_thread_min_tb;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox after_win_stop_thread_max_tb;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox after_win_stop_thread_min_tb;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox stop_game_after_lose_max_tb;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox stop_game_after_lose_min_tb;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox after_lose_stop_game_max_tb;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox after_lose_stop_game_min_tb;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox stop_game_after_win_max_tb;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox stop_game_after_win_min_tb;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox after_win_stop_game_max_tb;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox after_win_stop_game_min_tb;
        private System.Windows.Forms.TextBox stop_each_game_max_tb;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox stop_game_lose_max_tb;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox stop_game_lose_min_tb;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox stop_game_win_max_tb;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox stop_game_win_min_tb;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox stop_each_game_min_tb;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.RadioButton reset_win_rb;
        private System.Windows.Forms.GroupBox grp_stop_game;
        private System.Windows.Forms.GroupBox gb_on_win;
        private System.Windows.Forms.GroupBox gb_on_lose;
        private System.Windows.Forms.GroupBox gb_bet_lose;
        private System.Windows.Forms.GroupBox gb_cashout_lose;
        private System.Windows.Forms.RadioButton reset_lose_rb;
        private System.Windows.Forms.GroupBox gb_bet_win;
        private System.Windows.Forms.GroupBox gb_cashout_win;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.TextBox cashout_profit_win_lose_min_tb;
        private System.Windows.Forms.TextBox cashout_profit_win_lose_max_tb;
        private System.Windows.Forms.GroupBox gb_profit_win_lose;
        private System.Windows.Forms.RadioButton floor_accumulation_profit_win_lose_rb;
        private System.Windows.Forms.RadioButton multiply_lose_rb;
        private System.Windows.Forms.RadioButton multiply_win_rb;
        private System.Windows.Forms.TextBox gb_bet_min_tb;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton profit_win_lose_rb;
        private System.Windows.Forms.RadioButton normal_rb;
        private System.Windows.Forms.ComboBox cashout_profit_win_lose_cb;
        private System.Windows.Forms.ComboBox bet_win_cb;
        private System.Windows.Forms.TextBox bet_win_min_tb;
        private System.Windows.Forms.TextBox bet_win_max_tb;
        private System.Windows.Forms.TextBox cashout_win_min_tb;
        private System.Windows.Forms.TextBox cashout_win_max_tb;
        private System.Windows.Forms.ComboBox cashout_win_cb;
        private System.Windows.Forms.ComboBox bet_profit_win_lose_cb;
        private System.Windows.Forms.TextBox bet_profit_win_lose_min_tb;
        private System.Windows.Forms.TextBox bet_profit_win_lose_max_tb;
        private System.Windows.Forms.ComboBox bet_lose_cb;
        private System.Windows.Forms.TextBox bet_lose_min_tb;
        private System.Windows.Forms.TextBox bet_lose_max_tb;
        private System.Windows.Forms.TextBox cashout_lose_min_tb;
        private System.Windows.Forms.TextBox cashout_lose_max_tb;
        private System.Windows.Forms.ComboBox cashout_lose_cb;
        private System.Windows.Forms.GroupBox gb_proxy;
        private System.Windows.Forms.RadioButton programmer_rb;
        private System.Windows.Forms.TextBox gb_cashout_max_tb;
        private System.Windows.Forms.TextBox gb_cashout_min_tb;
        private System.Windows.Forms.TextBox gb_bet_max_tb;
        private FastColoredTextBoxNS.FastColoredTextBox programmer_fctb;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton bet_low_rb;
        private System.Windows.Forms.RadioButton bet_high_rb;
        private System.Windows.Forms.TabPage variables_tabpage;
        private System.Windows.Forms.RichTextBox variables_tb;
    }
}